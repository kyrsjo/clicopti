% load library
CLICopti;

% beam parameters
n_bunches = 40; % number of bunches per train
n_particles = 1.2e9; % number of particles per bunch
train_length = 200/1e9; % s, train length

% structure parameters
frequency = 11.9942; % GHz
G = 66; % MV/m
a1 = 0.1; % a/lambda
a2 = 0.1;
d1 = 0.11; % thickness/cell length
d2 = 0.11;
n_cell = 60; % number of cells

% here we go
base = CellBase_compat ("../cellBase/TD_12GHz_v2.dat", frequency, false, 3);
as = AccelStructure_paramSet2_noPsi (base, n_cell, 0.5*(a1+a2), a1-a2, 0.5*(d1+d2), d1-d2);
as.calc_g_integrals (200);

fill_time = as.getTfill () + as.getTrise () % s

peak_beam_current = n_bunches * n_particles / train_length / 6.241509343260179e+18; % A, 1 e / 1 s = (1 / 6.241509343260179e+18) A

power_unloaded = as.getPowerUnloaded (G*1e6*as.getL())/1e6 % MW
power_loaded   = as.getPowerLoaded   (G*1e6*as.getL(), peak_beam_current)/1e6 % MW

efficiency = 100 * as.getTotalEfficiency (power_unloaded*1e6, peak_beam_current, train_length) % percent
t_beam = as.getMaxAllowableBeamTime_detailed (power_loaded*1e6*1.44, 0, 0).time % s