# CLICopti

CLICopti is a C++ library for quickly estimating the parameters of an RF structure from its length, apertures, tapering, and cell design.
It can be ran directly or from a Octave- or Python wrapper.
Typical estimated parameters are the input power required to reach a certain voltage with a given beam current, the maximum safe pulse length for a given input power and the minimum bunch spacing in RF cycles allowed by a given long-range wake limit.

Current maintainer: Andrea Latina (CERN)

Authors:
 * Kyrre Sjobak,   University of Oslo and CERN (2013-2014)
 * Daniel Schulte, CERN                        (2013-)
 * Alexej Grudiev, CERN                        (2013-2014)
 * Andrea Latina,  CERN                        (2014-)
 * Jim Ögren,      Uppsala University and CERN (2017-2019)

We have invested a lot of time and effort in creating and maintaining the CLICopti library, please cite it when using it.
For information for how to cite CLICopti, please see the [CITATION](CITATION) file.

For further reading, the main references for CLICopti are listed in the [REFERENCES](REFERENCES) file.

CLICopti is published under the GNU Lesser General Public License Version 3, please respect it.
For details see the [LICENSE](LICENSE) file.

To understand how to use CLICopti, a good place to start is the top-level report the [CITATION](CITATION) file,
along with reading through [structure.h](h/structure.h).
Furthermore, the examples shown in the [src](src) folder should hopefully be instructive.
