/* CLICopti.i */

%{
#include "cellParams.h"
#include "cellBase.h"
#include "constants.h"
#include "structure.h"

extern void init_structure_calculator();
%}

%include "cellParams.h"
%include "cellBase.h"
%include "constants.h"
%include "structure.h"

%ignore init_structure_calculator;

%init %{
  init_structure_calculator();
  %}
