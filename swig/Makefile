PYTHON_BIN = python
PYTHON_CONFIG = python-config
NUMPY_INCLUDES =  -I`$(PYTHON_BIN) -c "import numpy ; print(numpy.get_include())"`
PYTHON_CFLAGS = $(filter-out -Wstrict-prototypes,  $(shell $(PYTHON_CONFIG) --cflags)) $(NUMPY_INCLUDES)
PYTHON_LDFLAGS = $(shell $(PYTHON_CONFIG) --ldflags)
MKOCTFILE = mkoctfile

INCLUDE_DIR = -I../h

OBJS = CLICopti.o ../src/cellParams.o ../src/cellBase.o ../src/structure.o

CXXFLAGS = -std=c++11 -Ofast -fPIC -Wall -Wno-unused-function -Wno-overloaded-virtual $(INCLUDE_DIR)

all: octave python

octave: CLICopti.oct
python: CLICopti.py

install: all
	cp CLICopti.py _CLICopti.so ../Python
	cp CLICopti.oct ../Octave

# PYTHON

CLICopti.py: _CLICopti.so

_CLICopti.so: CLICopti_python_wrap.o $(OBJS)
	@echo "Creating Python module ..."
	@$(CXX) -shared CLICopti_python_wrap.o $(OBJS) $(PYTHON_LDFLAGS) $(LDFLAGS) -o _CLICopti.so

CLICopti_python_wrap.o: CLICopti_python_wrap.cc
	@$(CXX) $(PYTHON_CFLAGS) $(CXXFLAGS) -c CLICopti_python_wrap.cc


# OCTAVE

CLICopti.oct: CLICopti_octave_wrap.o $(OBJS)
	@echo "Creating Octave module ..."
	$(MKOCTFILE) $(OBJS) CLICopti_octave_wrap.o $(LDFLAGS) -Wno-unused-function -v -o CLICopti.oct

CLICopti_octave_wrap.o: CLICopti_octave_wrap.cc
	$(MKOCTFILE) CLICopti_octave_wrap.cc -Wno-unused-function $(CXXFLAGS) -c


# SWIG-PYTHON

CLICopti_python_wrap.cc: CLICopti.i
	@echo "Running SWIG-Python ..."
	@swig -c++ -python -module CLICopti -o CLICopti_python_wrap.cc $(INCLUDE_DIR) CLICopti.i


# SWIG-OCTAVE

CLICopti_octave_wrap.cc: CLICopti.i
	@echo "Running SWIG-Octave ..."
	@swig -c++ -octave -globals . -module CLICopti -o CLICopti_octave_wrap.cc $(INCLUDE_DIR) CLICopti.i

CLICopti.o: CLICopti.cc
	@$(CXX) $(CXXFLAGS) -c CLICopti.cc


# CLEAN

clean:
	@rm -f $(OBJS)\
 CLICopti_python_wrap.o\
 CLICopti_octave_wrap.o\
 CLICopti.oct\
 _CLICopti.so

cleanall: clean
	@rm -f CLICopti_octave_wrap.cc CLICopti_python_wrap.cc CLICopti.py
