.PHONY: all lib octave python clean install

all: lib octave python

install: all
	make -C swig install

lib:
	make -C src

octave:
	make -C swig octave

python:
	make -C swig python

clean:
	make -C src clean
	make -C swig clean
