import numpy as np
import matplotlib.pyplot as plt

L = float(1.5) #just some number

#base data
X = np.asarray([0,L/2.0,L])
FabsMax = 2.5
F = np.random.random(3)*FabsMax*2.0-FabsMax #[-FabsMax,FabsMax]

#interpolation coeffs
a0 = F[0]
a1 = (-3*F[0]+4*F[1]-F[2])/L
a2 = (2*F[0]-4*F[1]+2*F[2])/(L**2)

#generate data
x = np.linspace(0,L)
y = a0 + a1*x + a2*x**2

#plot to test
plt.plot(X,F, '+')
plt.plot(x,y)
plt.xlim(0.0-L*0.05,L*1.05);
plt.ylim(-FabsMax*1.05,FabsMax*1.05)
plt.show()

