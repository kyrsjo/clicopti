#!/usr/bin/env python

# This script plots the geometry parameters for visualization purposes

import sys
assert len(sys.argv)==2 or len(sys.argv)==3, "USAGE: plotGeomParams.py G|502 (finalOut)"
if sys.argv[1] == "G":
    from GeomParams_CLICG import *
    geom = GeomParams_CLICG()
elif sys.argv[1] == "502":
    from GeomParams_502 import *
    geom = GeomParams_502()
else:
    print "Invalid structure name"
    exit()

finalOut=False
golden_mean = (np.sqrt(5)-1.0)/2.0      # Aesthetic ratio
fig_width = 0.4*45*0.393700787 #138.2795/72.27  # width in inches
fig_height = fig_width*golden_mean*1.5      # height in inches
fig_size =  [fig_width,fig_height]
if len(sys.argv)==3:
    assert sys.argv[2]=="finalOut"
    finalOut=True
    params = {'backend': 'pdf',
              'axes.labelsize': 20,
              'text.fontsize': 20,
              'legend.fontsize': 20,
              'xtick.labelsize': 16,
              'ytick.labelsize': 16,
              'text.usetex': True,
              'figure.figsize': fig_size,
              'font.family' : 'serif',
              'font.serif' : ['Times']}
    import matplotlib
    matplotlib.rcParams.update(params)


import matplotlib.pyplot as plt
import numpy as np

## Geometry parameters

#plt.plot(geom.irisRadii[1:-1], '*-')
#plt.plot(geom.irisThick, '*-')
#plt.plot(geom.cellRadii[1:-1], '*-')
#plt.plot(geom.cellFlat[1:-1], '*-')
#plt.plot(geom.cellEow[1:-1], '*-')
#plt.plot(geom.cellLength, '*-')
#plt.plot(geom.cellLength+geom.irisThick[:-1])

#h = []
#for i in xrange(len(geom.cellLength)):
#    h.append(geom.cellLength[i] + 0.5*(geom.irisThick[i]+geom.irisThick[i+1]))
#plt.plot(h)

## RF parameters
#z = np.linspace(0.0, geom.N*geom.period, 500)

#plt.plot(z, geom.vg_interp(z))
#plt.plot(geom.Ziris, geom.vg*3e8/100.0, 'r+')

#plt.plot(z,geom.Q_interp(z))
#plt.plot(geom.Zcell, geom.Q, 'r+')

#plt.plot(z,geom.RoQ_interp(z))
#plt.plot(geom.Zcell, geom.RoQ, 'r+')

###

geom.solveIntegrals()
(xmin,xmax) = (0.0, geom.zValues.max())

################
## 2x2 LAYOUT ##
################
(fig1, fig1ax) = plt.subplots(2,2,sharex=True,sharey=False,squeeze=True)
##  GRADIENT ##
#fig1ax[0] = plt.subplot(5,1,1,sharex=fig1ax[-1])
fig1ax[0][0].plot(geom.zValues*1e3, geom.Gz_unloaded/1e6, 'k--')
fig1ax[0][0].plot(geom.zValues*1e3, geom.Gz_loaded/1e6, 'k-')
fig1ax[0][0].set_ylabel("$E_\mathrm{acc}$ [MV/m]")
fig1ax[0][0].tick_params(right="off")
fig1ax[0][0].set_yticks(np.arange(60, 100.1, 20))
print "Unloaded G:   Input =", geom.Gz_unloaded[0]/1e6, "Peak =", geom.Gz_unloaded.max()/1e6, "[MV/m]"
print "Loaded G:     Input =", geom.Gz_loaded[0]/1e6, "Peak =", geom.Gz_loaded.max()/1e6, "[MV/m]"
## E-field ##
E_unloaded = geom.Gz_unloaded*geom.Emax_interp(geom.zValues)/1e6 #MV/m
E_loaded   = geom.Gz_loaded*geom.Emax_interp(geom.zValues)/1e6
fig1ax[0][1].plot(geom.zValues*1e3, E_unloaded,'b--')
fig1ax[0][1].plot(geom.zValues*1e3, E_loaded,'b-')
fig1ax[0][1].set_ylabel("$|E|^\mathrm{surf}$ [MV/m]")
fig1ax[0][1].tick_params(left="off")
for t in fig1ax[0][1].yaxis.get_major_ticks():
    t.label1On = False
    t.label2On = True
fig1ax[0][1].yaxis.set_label_coords(1.34,0.5)
fig1ax[0][1].set_ylim(E_loaded.min()*0.9, E_unloaded.max()*1.1)
fig1ax[0][1].set_yticks(np.arange(130, 250.1, 40))
print "Unloaded |E|: Input =", E_unloaded[0], "Peak =", E_unloaded.max(), "[MV/m]"
print "Loaded   |E|: Input =", E_loaded[0], "Peak =", E_loaded.max(), "[MV/m]"
## H-field (dont plot just print) ##
#fig1ax[2] = plt.subplot(5,1,3, sharex=fig1ax[-1])
H_unloaded = geom.Gz_unloaded*geom.Hmax_interp(geom.zValues)/1e6 #[kA/m]
H_loaded   = geom.Gz_loaded*geom.Hmax_interp(geom.zValues)/1e6
#fig1ax[2].plot(geom.zValues*1e3, H_unloaded/H_unloaded[0], 'r--')
#fig1ax[2].plot(geom.zValues*1e3, H_loaded/H_loaded[0], 'r-')
#fig1ax[2].set_ylabel("Hmax [kA/m]")
print "Unloaded |H|: Input =", H_unloaded[0], "Peak =", H_unloaded.max(), "[kA/m]"
print "Loaded   |H|: Input =", H_loaded[0], "Peak =", H_loaded.max(), "[kA/m]"
## S_C ##
SC_unloaded = (geom.Gz_unloaded**2)*geom.SCmax_interp(geom.zValues)/1e15 #[MW/um^2]
SC_loaded   = (geom.Gz_loaded**2)*geom.SCmax_interp(geom.zValues)/1e15
fig1ax[1][0].plot(geom.zValues*1e3, SC_unloaded, 'g--')
fig1ax[1][0].plot(geom.zValues*1e3, SC_loaded, 'g-')
fig1ax[1][0].set_ylabel("$S_c$ [MW/$\mu$m$^2$]")
fig1ax[1][0].set_ylim(SC_loaded.min()*0.9, SC_unloaded.max()*1.1)
fig1ax[1][0].tick_params(right="off")
fig1ax[1][0].set_yticks(np.arange(1.5, 4.51, 1.0))
print "Unloaded Sc:  Input =", SC_unloaded[0], "Peak =", SC_unloaded.max(), "[MW/um^2]"
print "Loaded   Sc:  Input =", SC_loaded[0], "Peak =", SC_loaded.max(), "[MW/um^2]"
## DELTA T ##
#fig1ax[4] = plt.subplot(5,1,5)
fig1ax[1][1].plot(geom.zValues*1e3, geom.DTunloaded, 'm--')
fig1ax[1][1].plot(geom.zValues*1e3, geom.DTloaded, 'm-')
fig1ax[1][1].set_ylabel("$\Delta T$ [K]")
fig1ax[1][1].tick_params(left="off")
for t in fig1ax[1][1].yaxis.get_major_ticks():
    t.label1On = False
    t.label2On = True
fig1ax[1][1].yaxis.set_label_coords(1.34,0.5)
fig1ax[1][1].set_ylim(bottom=geom.DTloaded.min()*0.9, top=geom.DTunloaded.max()*1.1)
fig1ax[1][1].set_yticks(np.arange(15, 45.1, 10))
print "DeltaT unloaded peak =", geom.DTunloaded.max(), "[K]"
print "DeltaT   loaded peak =", geom.DTloaded.max(), "[K]"
## GENERAL PLOT SETUP ##
fig1ax[1][0].set_xlim(xmin*1e3,xmax*1e3)
fig1ax[1][0].set_xlabel("z [mm]")
fig1ax[1][0].set_xticks(np.arange(0, xmax*1e3, 50))
fig1ax[1][1].set_xlim(xmin*1e3,xmax*1e3)
fig1ax[1][1].set_xlabel("z [mm]")
fig1ax[1][1].set_xticks(np.arange(0, xmax*1e3, 50))
fig1.subplots_adjust(hspace=0,wspace=0.05, left=0.13, right=0.87, top=0.97, bottom=0.16)
plt.setp([a.get_xticklabels() for a in fig1.axes[:-2]], visible=False)
if finalOut:
    plt.savefig("fieldsAlongStructure-"+sys.argv[1]+".pdf")#, bbox_inches=(0.0,fig_width,0.0,fig_height)) #"tight")

############################
### SINGLE-COLUMN LAYOUT ###
############################
(fig1, fig1ax) = plt.subplots(4,1,sharex=True,sharey=False,squeeze=True)
##  GRADIENT ##
fig1ax[0].plot(geom.zValues*1e3, geom.Gz_unloaded/1e6, 'k--', label="Unloaded")
fig1ax[0].plot(geom.zValues*1e3, geom.Gz_loaded/1e6, 'k-', label="Loaded")
fig1ax[0].set_ylabel("$E_\mathrm{acc}$ [MV/m]")
fig1ax[0].yaxis.set_label_coords(-0.08,0.5)
#fig1ax[0].tick_params(right="off")
fig1ax[0].set_yticks(np.arange(60, 100.1, 20))
fig1ax[0].legend(loc=3,frameon=False,borderaxespad=0)
## E-field ##
fig1ax[1].plot(geom.zValues*1e3, E_unloaded,'b--')
fig1ax[1].plot(geom.zValues*1e3, E_loaded,'b-')
fig1ax[1].set_ylabel("$|E|^\mathrm{surf}$ [MV/m]")
#fig1ax[1].tick_params(left="off")
for t in fig1ax[1].yaxis.get_major_ticks():
    t.label1On = False
    t.label2On = True
fig1ax[1].yaxis.set_label_coords(1.15,0.5)
fig1ax[1].set_ylim(E_loaded.min()*0.9, E_unloaded.max()*1.1)
fig1ax[1].set_yticks(np.arange(130, 250.1, 40))
## S_C ##
fig1ax[2].plot(geom.zValues*1e3, SC_unloaded, 'g--')
fig1ax[2].plot(geom.zValues*1e3, SC_loaded, 'g-')
fig1ax[2].set_ylabel("$S_c$ [MW/$\mu$m$^2$]")
fig1ax[2].yaxis.set_label_coords(-0.08,0.5)
fig1ax[2].set_ylim(SC_loaded.min()*0.9, SC_unloaded.max()*1.1)
#fig1ax[2].tick_params(right="off")
fig1ax[2].set_yticks(np.arange(1.5, 4.51, 1.0))
## DELTA T ##
#fig1ax[4] = plt.subplot(5,1,5)
fig1ax[3].plot(geom.zValues*1e3, geom.DTunloaded, 'm--')
fig1ax[3].plot(geom.zValues*1e3, geom.DTloaded, 'm-')
fig1ax[3].set_ylabel("$\Delta T$ [K]")
#fig1ax[3].tick_params(left="off")
for t in fig1ax[3].yaxis.get_major_ticks():
    t.label1On = False
    t.label2On = True
fig1ax[3].yaxis.set_label_coords(1.15,0.5)
fig1ax[3].set_ylim(bottom=geom.DTloaded.min()*0.9, top=geom.DTunloaded.max()*1.1)
fig1ax[3].set_yticks(np.arange(15, 45.1, 10))
## GENERAL PLOT SETUP ##
fig1ax[3].set_xlim(xmin*1e3,xmax*1e3)
fig1ax[3].set_xlabel("z [mm]")
fig1ax[3].set_xticks(np.arange(0, xmax*1e3, 50))
fig1.subplots_adjust(hspace=0,wspace=0.00, left=0.12, right=0.88, top=0.99, bottom=0.08)
plt.setp([a.get_xticklabels() for a in fig1.axes[:-1]], visible=False)
if finalOut:
    plt.savefig("fieldsAlongStructure-singlerow"+sys.argv[1]+".pdf",transparent=True)#, bbox_inches=(0.0,fig_width,0.0,fig_height)) #"tight")



#plt.legend(loc=3)

#plt.figure(5)
#plt.plot(geom.zValues, geom.Ib)

plt.figure(6)
powsc = 1e6#scaling
#plt.plot([-geom.trise,0],np.array([0,geom.pulse_pow0])/powsc, '+-')
plt.plot(geom.pulse_tz, geom.pulse_pow/powsc,'-')
#plt.plot([geom.tfill, geom.tfill+geom.tbeam],[geom.P0/powsc,geom.P0/powsc])
#plt.plot([geom.tfill+geom.tbeam,geom.tfill+geom.tbeam+geom.trise, 2*geom.tfill+geom.tbeam+geom.trise], [geom.P0/powsc, geom.pulse_pow0/powsc,0.0],'+-')
plt.plot(geom.pulseShapeFull_t-geom.trise, geom.pulseShapeFull_P/powsc)
plt.xlabel("Time [s]")
plt.ylabel("Input power [MW]")



plt.show()
