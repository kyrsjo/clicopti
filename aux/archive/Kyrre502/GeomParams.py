import numpy as np
import scipy as sp
import scipy.integrate

class GeomParams:
    """ Interface for defining usefull functions """
    
    def getZ_Iris(self,irisNum):
        """
        Returns the z position corresponding to the given irisID.
        z = 0 is taken to be in the middle of iris 1 (first normal iris).
        Negative values are accepted, -1 meaning last iris, -2 second-last etc.
        """
        assert irisNum <= self.N+1
        if irisNum >= 0.0:
            return (irisNum-1)*self.period
        else:
            return self.getZ_Iris(self.N+2+irisNum)
        
    def getZ_Cell(self,cellNum):
        """
        Returns the z-position corresponding to the given cellID.
        z = 0 as getZ_Iris().
        Cell 1 (first normal cell) is defined as
        in the middle between iris 1 and 2,
        so cellID 1 corresponds to irisID 1.5
        """
        assert cellNum <= self.N
        if cellNum >= 0.0:
            return self.getZ_Iris(cellNum+0.5)
        else:
            return self.getZ_Cell(self.N+1+cellNum)
    
    def solveIntegrals(self):
        """
        Solve and initialize power flow integrals.
        Method as described in "Analytical solutions for transient and
        steady state beam loading in arbitrary traveling wave structures" by
        Lunin et. al., PRST14 (2001)
        """
        
        (self.zValues, zh) = np.linspace(0.0,self.N*self.period, 1000, retstep=True)
        z = self.zValues
        print "zh =", zh, "[m]"
        
        #Integral in the exponent of g(z)
        # The integral
        Ia  = np.empty_like(z); Ia[0] = 0.0
        # Integrand 
        Iap = self.omega / (self.vg_interp(z) * self.Q_interp(z))
        for i in xrange(1,len(self.zValues)):
            #Simpson's rule from the beginning (SLOW - but it's OK)
            Ia[i] = sp.integrate.simps(Iap[:i+1], x=z[:i+1])    
        #g(z)
        self.gz = np.sqrt((self.vg_interp(0.0)*self.RoQ_interp(z))  /  \
                          (self.vg_interp(z)*self.RoQ_interp(0.0))) *  \
                          np.exp(-0.5*Ia);
        self.gzInt = sp.integrate.simps(self.gz,z)

        #Input gradient (initial condition)
        self.G0 = np.sqrt(self.omega*self.RoQ_interp(0.0)*self.P0/self.vg_interp(0.0))
        print "P0 =", self.P0/1e6, "[MW], G0 =", self.G0/1e6, "[MV/m]"
        #Unloaded gradient
        self.Gz_unloaded         = self.G0*self.gz
        self.Gz_unloaded_voltage = self.G0*self.gzInt
        self.Gz_unloaded_ave     = self.Gz_unloaded_voltage / (z.max()-z.min())
        print
        print "Unloaded voltage          =", self.Gz_unloaded_voltage/1e6, "[MV]"
        print "Average unloaded gradient =", self.Gz_unloaded_ave/1e6, "[MV/m]"

        #Loaded gradient integral
        # Integral
        Ib = np.empty_like(z);Ib[0] = 0.0
        # Integrand
        print
        print "I =", self.I, "[A]"
        Ibp = self.I * self.omega * self.RoQ_interp(z) / (self.gz * 2*self.vg_interp(z))
        for i in xrange(1,len(self.zValues)):
            #Simpson's rule from the beginning (SLOW - but it's OK)
            Ib[i] = sp.integrate.simps(Ibp[:i+1], x=z[:i+1])    
        self.Ib = Ib
        self.Gz_loaded = self.G0*self.gz - self.gz*Ib
        self.Gz_loaded_voltage = sp.integrate.simps(self.Gz_loaded, z)
        self.Gz_loaded_ave =  self.Gz_loaded_voltage / (z.max()-z.min())
        print "Loaded voltage =", self.Gz_loaded_voltage/1e6, "[MV]"
        print "Average loaded gradient =", self.Gz_loaded_ave/1e6, "[MV/m]"
        self.efficiency = self.I*self.Gz_loaded_voltage/self.P0
        print "RF-to-beam efficiency =", self.efficiency

        #Power to get to target gradient
        print
        print "Target gradient = ", self.Ez_target/1e6, "[MV/m]"
        self.G0_target = (self.Ez_target*z[-1] + sp.integrate.simps(self.gz*Ib,z))/self.gzInt
        print "G0_target =", self.G0_target/1e6, "[MV/m]"
        self.P_target = self.G0_target**2 * self.vg_interp(0.0)/(self.omega*self.RoQ_interp(0.0))
        print "P_target =", self.P_target/1e6, "[MW]"
        self.efficiency_target = self.I*self.Ez_target*(z.max()-z.min())/self.P0
        print "RF-to-beam efficiency =", self.efficiency_target
        print
        
        #Efficiency and pulse-shape dependent stuff
        print
        oneOverVg = 1.0/self.vg_interp(z)
        self.tfill = sp.integrate.simps(oneOverVg,z)
        print "tFill=", self.tfill*1e9, "tRise=", self.trise*1e9, "tBeam=", self.tbeam*1e9, "[ns]"
        self.etaCorrFactor = self.tbeam/(self.tbeam+self.tfill+self.trise)
        print "etaCorrFactor=", self.etaCorrFactor
        self.efficiency_real = self.etaCorrFactor*self.efficiency
        self.efficiency_target_real = self.etaCorrFactor*self.efficiency_target
        print "efficiency_real=", self.efficiency_real*100, "efficiency_target_real=",self.efficiency_target_real*100, "[%]"
        print        

        #Optimal pulse during filling (use self.P0 as peak power)
        self.pulse_tz = np.zeros_like(z)
        self.pulse_pow = np.zeros_like(self.pulse_tz)
        constFact = np.sqrt(self.vg_interp(0)/(self.omega*self.RoQ_interp(0)*self.P0))
        for i in xrange(len(z)):
            self.pulse_tz[i] = sp.integrate.simps(oneOverVg[i:], z[i:])
            self.pulse_pow[i] = self.P0*(1-constFact*Ib[i])**2
        self.pulse_pow0 = self.P0*(1-constFact*Ib[-1])**2

        #self.trise = 68e-9

        #Pulsed heating as function of z
        #self.pulseShapeFull_t = np.linspace(0.0,2*self.trise+2*self.tfill+self.tbeam, 10000)
        self.pulseShapeFull_t = np.linspace(0.0,2*self.trise+2*self.tfill+self.tbeam, 500)
        self.pulseShapeFull_P = np.empty_like(self.pulseShapeFull_t)
        for i in xrange(len(self.pulseShapeFull_t)):
            t = self.pulseShapeFull_t[i]
            if t < self.trise:
                self.pulseShapeFull_P[i] = t*self.pulse_pow0/self.trise
            elif t < self.trise+self.tfill:
                self.pulseShapeFull_P[i] = self.pulse_pow0 + (t-self.trise)*(self.P0-self.pulse_pow0)/self.tfill
            elif t < self.trise+self.tfill+self.tbeam:
                self.pulseShapeFull_P[i] = self.P0
            elif t < self.trise+self.tfill+self.tbeam+self.trise:
                self.pulseShapeFull_P[i] = self.P0 - (t-self.trise-self.tfill-self.tbeam)/self.trise*(self.P0-self.pulse_pow0)
            else:
                self.pulseShapeFull_P[i] = (self.pulse_pow0) - (t-2*self.trise-self.tfill-self.tbeam)/self.tfill*(self.pulse_pow0)
        
        DTintegral = np.zeros_like(self.pulseShapeFull_t)
        for i in xrange(1,len(DTintegral)):
            DTintegral[i] = sp.integrate.simps( (self.pulseShapeFull_P[:i]/self.P0) /np.sqrt(self.pulseShapeFull_t[i]-self.pulseShapeFull_t[:i]),self.pulseShapeFull_t[:i])
        DTperField2 = 0.5*self.surfResistance/np.sqrt(np.pi*self.rho*self.ceps*self.ktherm)*\
            (self.Hmax_interp(z[0])/1e3)**2*DTintegral

        #DTperField2 = 0.5*self.surfResistance/np.sqrt(np.pi*self.rho*self.ceps*self.ktherm)*\
        #    (self.Hmax_interp(z)/1e3)**2
        
        self.DTloaded = DTperField2.max() * self.Gz_loaded**2
        self.DTunloaded = DTperField2.max() * self.Gz_unloaded**2
            
