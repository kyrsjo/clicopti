#!/usr/bin/env python

import sys
assert len(sys.argv)==2 or len(sys.argv)==3, "USAGE: plotWakes.py G|502 (finalOut)"
if sys.argv[1] == "G":
    from GeomParams_CLICG import *
    geom = GeomParams_CLICG()
elif sys.argv[1] == "502":
    from GeomParams_502 import *
    geom = GeomParams_502()
else:
    print "Invalid structure name"
    exit()

finalOut=False
golden_mean = (np.sqrt(5)-1.0)/2.0      # Aesthetic ratio
fig_width = 138.2795/72.27*1.5#3.25  # width in inches 
fig_height = fig_width*golden_mean#*0.89      # height in inches
fig_size =  [fig_width,fig_height]
if len(sys.argv)==3:
    assert sys.argv[2]=="finalOut"
    finalOut=True
    params = {'backend': 'pdf',
              'axes.labelsize': 10,
              'text.fontsize': 10,
              'legend.fontsize': 10,
              'xtick.labelsize': 8,
              'ytick.labelsize': 8,
              'text.usetex': True,
              'figure.figsize': fig_size,
              'font.family' : 'serif',
              'font.serif' : ['Times']}
    import matplotlib
    matplotlib.rcParams.update(params)


import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import scipy.signal

s = np.linspace(0,5,50000)
W = np.zeros_like(s)

wakeFunc = lambda p, s: -p[2]*np.exp(-2*np.pi*p[0]*s/(2*p[1]))*\
    np.sin(2*np.pi*p[0]*s*np.sqrt(1-(4*p[1])**-2))

for modeIdx in xrange(len(geom.w_k_interp)):
    for cellIdx in xrange(1,geom.N+1):
        z = geom.getZ_Cell(cellIdx)
        p = [geom.w_k_interp[modeIdx](z),\
             geom.w_Q_interp[modeIdx](z),\
             geom.w_A_interp[modeIdx](z)*geom.period]
        print cellIdx, z, p
        W += wakeFunc(p,s)#*geom.period
W /= geom.N*geom.period

s2 = 0.5e-9*3e8
s2idx = 0
for i in xrange(len(s)):
    if s[i] > s2:
        s2idx = i
        break
print "Wx(s2) =", W[s2idx]

plt.figure(1)
plt.plot(s,W, c='k')
plt.axvline(x=0.5e-9*3e8, c='k', ls='--')
plt.plot(s[s2idx],W[s2idx], 'r+')
#plt.annotate("%.1f" % (W[s2idx],),(s[s2idx],W[s2idx]),\
#                 arrowprops=dict(arrowstyle="->"),\
#                 xycoords='data', xytext=(5, 20), textcoords='offset points')
plt.ylabel("$W_\perp$ [V/pC/m/mm]")
plt.xlabel("s [m]")
plt.subplots_adjust(left=0.16, right=0.9, bottom=0.16, top=0.9)
plt.xlim(0,0.2)
if finalOut:
    plt.savefig("wakefield-"+sys.argv[1]+".pdf")

plt.figure(2)
plt.semilogy(s,np.abs(W), c='k')
plt.axvline(x=0.5e-9*3e8, c='k', ls='--')
plt.plot(s[s2idx],np.abs(W[s2idx]), 'ro')
plt.xlabel("s [m]")
plt.ylabel("$|W_\perp|$ [V/pC/m/mm]")
if finalOut:
    plt.savefig("wakefieldAbs-"+sys.argv[1]+".pdf")


plt.figure(3)
plt.plot(s,W, c='k')
Wfirst = wakeFunc([geom.w_k[0][0],\
                   geom.w_Q[0][0],\
                   geom.w_A[0][0]], s)
Wfirst += wakeFunc([geom.w_k[1][0],\
                   geom.w_Q[1][0],\
                   geom.w_A[1][0]], s)
plt.plot(s,Wfirst)
Wlast = wakeFunc([geom.w_k[0][1],\
                  geom.w_Q[0][1],\
                  geom.w_A[0][1]], s)
Wlast += wakeFunc([geom.w_k[1][1],\
                   geom.w_Q[1][1],\
                   geom.w_A[1][1]], s)
plt.plot(s,Wlast)

plt.figure(4)
plt.semilogy(s,np.abs(sp.signal.hilbert(W)), 'k-', label="Structure")
plt.semilogy(s,np.abs(sp.signal.hilbert(Wfirst)),'r--', label="First cell")
plt.semilogy(s,np.abs(sp.signal.hilbert(Wlast)),'b:', label="Last cell")
plt.axvline(x=0.5e-9*3e8, c='g', ls='--', label="Bunch pos",zorder=-1)
plt.legend(loc="lower left",labelspacing=0.05,borderpad=0.1, frameon=False)
plt.xlim(0,0.25)
plt.ylim(1e-1,2e2)
plt.xlabel("s [m]")
plt.ylabel("$W_\perp$ envelope [V/pC/m/mm]")
#plt.subplots_adjust(left=0.15, right=0.97, bottom=0.18, top=0.97)
plt.subplots_adjust(left=0.17, right=0.96, bottom=0.18, top=0.97)
if finalOut:
    plt.savefig("wakefield-decomposed-hilbert-"+sys.argv[1]+".pdf",transparent=True)

print "|hilbert(W)|(s2) =", np.abs(sp.signal.hilbert(W))[s2idx]

plt.show()
