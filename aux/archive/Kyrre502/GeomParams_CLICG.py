import numpy as np
import scipy as sp
import scipy.interpolate
from GeomParams import GeomParams

class GeomParams_CLICG(GeomParams):

    name = "CLIC_G"
    
    #### GEOMETRY PARAMETERS ###

    #Number of regular cells
    N = 28

    #Period 120 deg @ 11.994 GHz, vph=c
    period = 8.332e-3 #[m] (NOTE UNITS - THIS IS NOT mm!!!)
    
    #Radius of power input/output WG and beampipe in mm
    dcwg = 21.9
    #Cavity roundoff radius in mm
    rr = 0.5
    #Damping waveguide entrance in mm
    idw = 8
    #Damping waveguide dimension in mm
    adw = 11
    #Damping waveguide length in mm
    ldw = 40
    
    #Radius of irises in mm,
    # including matching irises at end of structure.
    # Usually called "a".
    irisRadii = np.asarray([5.056, 3.15, 3.1167, 3.0833, 3.05, 3.0167,2.9833, 2.95,
                            2.9167, 2.8833, 2.85, 2.8167, 2.7833, 2.75, 2.7167, 2.6833,
                            2.65, 2.6167, 2.5833, 2.55, 2.5167, 2.4833, 2.45, 2.4167,
                            2.3833, 2.35, 4.253])
    #Thickness of irises in mm,
    # including matching irises at ends of structure.
    # Usually called "d".
    irisThick = np.asarray([1.67, 1.67, 1.6421, 1.6142, 1.5862, 1.5583, 1.5304, 1.5025,
                            1.4746, 1.4467, 1.4188, 1.3908, 1.3629, 1.335, 1.3071, 1.2792,
                            1.2513, 1.2233, 1.1954, 1.1675, 1.1396, 1.1117, 1.0838, 1.0558,
                            1.0279, 1, 1])
    #Ellipticity of iris,
    # Usually called "e"
    irisE = None #Calculated by formula in __init__()
    #Cell outer radius in mm,
    # including matching cells at ends of structure.
    # Usually called "b".
    cellRadii = np.asarray([8.9417, 8.6149, 8.602, 8.5893, 8.5768, 8.5645, 8.5524, 8.5406,
                            8.529, 8.5177, 8.5066, 8.4958, 8.4852, 8.4749, 8.4648, 8.4548,
                            8.4451, 8.4355, 8.4262, 8.417, 8.408, 8.3993, 8.3908, 8.3825,
                            8.3745, 8.62136])
    #Cell outer wall flat part length in mm,
    # including matching cells at ends of structure.
    # Usually called "c".
    cellFlat = np.asarray([1.0, 0.6267, 0.62, 0.6133, 0.6067, 0.6, 0.5933, 0.5867, 0.58,
                           0.5733, 0.5667, 0.56, 0.5533, 0.5467, 0.54, 0.5333, 0.5267,
                           0.52, 0.5133, 0.5067, 0.5, 0.4933, 0.4867, 0.48, 0.4733, 0.85])
    #Cell outer wall elipticity,
    # including matching cells at end of structure.
    # Usually called "eow"
    cellEow = np.asarray([3.4, 3.3958, 3.3875, 3.3792, 3.3708, 3.3625, 3.3542, 3.3458,
                          3.3375, 3.3292, 3.3208, 3.3125, 3.3042, 3.2958, 3.2875, 3.2792,
                          3.2708, 3.2625, 3.2542, 3.2458, 3.2375, 3.2292, 3.2208, 3.2125,
                          3.2042, 3.2])
    #Cell cavity length in mm.
    # Usually called "l".
    # Total element length L = l+d = disk length (!= period which is between iris tips!!)
    cellLength = np.asarray([6.662, 6.676, 6.7039, 6.7318, 6.7597, 6.7876, 6.8155, 6.8435,
                             6.8714, 6.8993, 6.9272, 6.9551, 6.983, 7.011, 7.0389, 7.0668,
                             7.0947, 7.1226, 7.1505, 7.1785, 7.2064, 7.2343, 7.2622, 7.2901,
                             7.318, 7.332])

    ### RF PARAMETERS (first/mid/last) ###
    #f     = np.asarray([11.9942, 11.9942, 11.9942]) #[GHz]
    f     = np.asarray([12,12,12])
    Q     = np.asarray([5536, 5635, 5738])
    vg    = np.asarray([1.65, 1.2, 0.83]) #[% of c] 
    RoQ = np.asarray([14587, 16220, 17954]) #[Ohm/m]

    # Structure input
    #P0 = 61.3e6 #Peak input power [W]
    #P0 = 60.36e6 #Using Ptarget
    P0 = 63.46e6
    I  = 1.60217646e-19*3.7e9/0.5e-9 #Beam current [A]
    Ez_target = 100e6 #[MV/m]

    # Peak fields (first/mid/last)
    Emax = np.asarray([1.95,  1.93, 1.9]) #[-]
    Hmax = np.asarray([4.1,   3.85, 3.6])  #[mA/V]
    SCmax = np.asarray([0.41, 0.35, 0.3])#[mA/V]

    trise = 22e-9 #[s]
    #tfill = 67e-9 #[s] #Calculated as \int_0^L 1/v_g(z) dz
    #tbeam = 230*0.5e-9 #[s]
    tbeam = 312*0.5e-9
    
    # Material properties (OFC copper) for thermal calcs
    rho = 8.95e3 #[kg/m^3] Mass density
    ceps = 385.0    #[J/(kg*K)] Specific heat capacity
    ktherm = 391.0  #[W/(m*K)] thermal conductivity
    sigmaElectric = 5.8e7 #[1/(ohm*meter)] electric conductivity
    mu0 = 4e-7*np.pi #[H/m] magnetic permeability
    
    def __init__(self):

        # #Ellipticity of irises
        # self.irisE = 1 + (self.irisThick/self.period) / (self.irisRadii / 2.625)

        # #Check that we have the right ammount of parameters
        # assert len(self.irisRadii) == \
        #        len(self.irisThick) == \
        #        len(self.irisE)     == \
        #        self.N+3
        # assert len(self.cellRadii)  == \
        #        len(self.cellFlat)   == \
        #        len(self.cellEow)    == \
        #        len(self.cellLength) == \
        #        self.N+2

        #self.N = 26 # IN ORDER TO MATCH PAPER FOR RF COMPUTATIONS

        # setup RF parameter interpolation functions
        self.omega         = np.mean(self.f)*1e9*(2*np.pi)
        print "OMEGA =", self.omega, "[radians/s]"
        #self.Zcell = np.asarray(map(self.getZ_Cell, [1, (self.N+1.0)/2.0, -1]))
        #print "Zcell =", self.Zcell*1e3, "[mm]"
        self.Ziris = np.asarray(map(self.getZ_Iris, [1, (self.N+2.0)/2.0, -1]))
        print "Ziris =", self.Ziris*1e3, "[mm]"
        self.Q_interp      = sp.interpolate.lagrange(self.Ziris, self.Q) # [-]
        self.vg_interp     = sp.interpolate.lagrange(self.Ziris, self.vg*3e8/100.0) # [m/s]
        self.RoQ_interp    = sp.interpolate.lagrange(self.Ziris, self.RoQ) # [Ohm/m]
        
        self.Emax_interp   = sp.interpolate.lagrange(self.Ziris, self.Emax)  # [-]
        self.Hmax_interp   = sp.interpolate.lagrange(self.Ziris, self.Hmax)  # [mA/V]
        self.SCmax_interp  = sp.interpolate.lagrange(self.Ziris, self.SCmax) # [mA/V]

        print
        self.skinDepth = np.sqrt(2/(self.mu0*self.sigmaElectric*self.omega))
        print "SkinDepth =", self.skinDepth*1e6, "[um]"
        self.surfResistance = np.sqrt(self.mu0*self.omega/(2*self.sigmaElectric))
        print "SurfResistance =", self.surfResistance, "[ohms]"
        print
