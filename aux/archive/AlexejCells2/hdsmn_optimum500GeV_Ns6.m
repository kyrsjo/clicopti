% optimizing hdsmn structure for 150 MV/m
%a and d are (ap radius and iris thickness) / lambda X 10

close all
clear all

c = 299.792458; %speed of light [mm/ns]
%e = 1.6e-19;   %[C] - electron charge
e = 0.16;   %[nnC] - electron charge
E = 0.5e12*1.6e-19; %[V*C] beams energy 0.5TeV
L1 = 2.0e38; %[Hz/m^2] given peak Luminosity 

%BandFlag = 0; %does not take into account structure bandwidth df
BandFlag = 1; %scale Efficiency and pulse length according to t_rise=1/df

CostFlag = 0; %luminosity per power optimization 
CostFlag = 1; %do total cost optimization,  
CostFlag = 2; %do investment cost optimization,  
CalcCostFlag = 0; %interpolate cost function 
%CalcCostFlag = 1; %calculate cost function 

f = 12; %working frequency in GHz
Ea = 67; %[MV/m] averaged loaded accelerating gradient
dphi = 150; %[degree] phase advance per cell
Nb0 = 154; %reference number of bunches in the train
%dT_Cu2CuZr = sqrt(551/511); %coefficient to take CuZr instead of Cu

%%%%%% RF Constraints
Esurf_max = 260; %[MV/m]
deltaT_max = 56; %[K]
deltaT2_max = deltaT_max^2;
%Pin/C1^Na1*tp^(1/Nrt)*f^Nf/d^Nd = const 
Pin4tp_max = 83;  %[MW] - 18Wue@X-band - T53vg3_mc@BDR=1e-6
tpulse_max = 150; %[ns] - 18Wue@X-band - T53vg3_mc@BDR=1e-6
Nrt = 3; %power of root for pulse length
aref = 3.9; %[mm] - T53vg3_MC first regular iris radius
Na1 = 1; %power for iris radius
fref = 11.424; %[GHz] - reference frequency
Nf = 1; %power for frequency dependece
Pin100ns_NLCa_N = tpulse_max*(Pin4tp_max*fref^Nf/aref)^Nrt;
a1N = aref^Nrt;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CLIC structure at 30 GHz two special constrains
Pinmax = 10000;     %[MW] - maximum input power limit
Pinmax = 64/3*2*1.1;     %[MW] - maximum input power limit
vg_min = 0.5;      %[%] - minimum group velosity
%tp_nom = 242;    %[ns] - nominal pulse length
tp_nom = 323;    %[ns] - nominal pulse length * 4/3
tp_nom = 363;    %[ns] - nominal pulse length * 3/2
%tp_nom = 483;      %[ns] - nominal pulse length * 2
Nc_nom = 6;        %[rf cycles] - nominal bunch spacing
%%%%%%%%%%%%%%%%%%%%%%%%%%

Nb = 1:1500; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% optimization procedure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dav = 0.01;%step for <a>/lambda
dda = 0.05;%step for (a1-a2)/<a>
dd = 0.02; %step for d/h

dav = 0.005;%step for <a>/lambda
dda = 0.01;%step for (a1-a2)/<a>

av_min = 0.09; %a/lambda minimum
av_max = 0.21; %a/lambda maximum
da_min = dda;  %(a1-a2)/av minimum
da_max = 0.6;  %(a1-a2)/av maximum
d_min = 0.1;   %d/h minimum
d_max = 0.4;   %d/h maximum
d_min2 = d_min;
d_max2 = d_max;

av_m = av_min:dav:av_max;
navm = length(av_m);
da_m = da_min:dda:da_max;
ndam = length(da_m);
d_m = d_min:dd:d_max;
ndm = length(d_m)
d_m2 = d_min2:dd:d_max2;
ndm2 = length(d_m2);

dphi_m = 50:10:130;
f_m = [10:1:15,16:2:30];
Ea_m = 50:10:100;
Ea_m = [50,57,67,80,100];
Ea_m = Ea;
f_m = f;
dphi_m = dphi;

tic

nfm = length(f_m);
npm = length(dphi_m);
%eps='high'
%eps='low'
%eps='medium'
eps='norm'
%dir_name = sprintf('data500GeV_%s\\Nc26_',eps);
%dir_name = sprintf('data500GeV_%s\\Nc56_',eps);
dir_name = sprintf('data500GeV_%s\\Nc21_',eps);
%dir_name = sprintf('data500GeV_%s\\Nc45_',eps);
for Ea = Ea_m
  initDatafromDaniel500GeV(eps,Ea);
  dataname = sprintf('%sNs6_E%df%ddphi%ddd%ddav%ddda%d_P%drt_Pin%dkW_dT%dK_tp%dns_vgmin%dpm_tr%d',...
    dir_name,Ea,f_m(1),dphi_m(1),10000*dd,10000*dav,1000*dda,...
    Nrt,1000*Pin4tp_max,deltaT_max,tp_nom,10*vg_min,BandFlag);

Ls_range = [100,100]; %[mm]
tp_range = [100,100]; %[ns]
fr_range = [100,100]; %[Hz]
Ep_range = [100,100]; %[kJ]
for CostFlag = [0]
%CostFlag = 0; %luminosity per power optimization and range calculation
%CostFlag = 1; %do total cost optimization,
%CostFlag = 2; %do investment cost optimization,
if CostFlag == 1
Ls_range
tp_range
fr_range
Ep_range
  Npoints = 15;
  freq_array = [12,15];
  dphi_array = [120,150];
  freq_grid = zeros(length(freq_array),length(dphi_array),Npoints+2);
  dphi_grid = zeros(length(freq_array),length(dphi_array),Npoints+2);
  Ls_grid   = zeros(length(freq_array),length(dphi_array),Npoints+2);
  Cia1_grid = zeros(length(freq_array),length(dphi_array),Npoints+2);
  tp_grid   = zeros(1,Npoints+2);
  Cia2_grid = zeros(1,Npoints+2);
  fr_grid   = zeros(Npoints+2,Npoints+2);
  Ep_grid   = zeros(Npoints+2,Npoints+2);
  Cia3_grid = zeros(Npoints+2,Npoints+2);
%  if all([Ls_range,tp_range,fr_range,Ep_range]) %calc interpolation basic
  if Ls_range(1) < Ls_range(2) %calc interpolation basic
    l10Ls_r = log10(Ls_range);
    l10tp_r = log10(tp_range);
    l10fr_r = log10(fr_range);
    l10Ep_r = log10(Ep_range);
    Ls_array = 10.^(l10Ls_r(1):(l10Ls_r(2)-l10Ls_r(1))/(1+Npoints):l10Ls_r(2));
    tp_array = 10.^(l10tp_r(1):(l10tp_r(2)-l10tp_r(1))/(1+Npoints):l10tp_r(2));
    fr_array = 10.^(l10fr_r(1):(l10fr_r(2)-l10fr_r(1))/(1+Npoints):l10fr_r(2));
    Ep_array = 10.^(l10Ep_r(1):(l10Ep_r(2)-l10Ep_r(1))/(1+Npoints):l10Ep_r(2));
    [freq_grid, dphi_grid, Ls_grid] = ...
      ndgrid(freq_array, dphi_array, Ls_array);
    tp_grid = tp_array;
    [fr_grid, Ep_grid] = ...
      ndgrid(fr_array, Ep_array);
    for iLs = 1:Npoints+2
      for idphi = 1:length(dphi_array)
        for ifreq = 1:length(freq_array)
          [Ci, Ce, Cia] = ...
            calcCost(Ea,freq_array(ifreq),dphi_array(idphi),Ls_array(iLs),10,10,10,10);
          Cia1_grid(ifreq,idphi,iLs) = Cia(1);
        end
      end
    end
    for itp = 1:Npoints+2
      [Ci, Ce, Cia] = ...
        calcCost(Ea,10,10,10,tp_array(itp),10,10,10);
      Cia2_grid(itp) = sum(Cia([2,4:9]));
    end
    for ifr = 1:Npoints+2
      for iEp = 1:Npoints+2
        [Ci, Ce, Cia] = ...
          calcCost(Ea,10,10,10,10,10,fr_array(ifr),Ep_array(iEp));
        Cia3_grid(ifr,iEp) = Cia(3);
      end
    end
  end
  save(sprintf('%s_CostGrid',dataname), 'freq_grid', 'dphi_grid', 'Ls_grid', ...
      'Cia1_grid', 'tp_grid', 'Cia2_grid', 'fr_grid', 'Ep_grid', 'Cia3_grid')
  if 0
    load(sprintf('%sE%df%d_%ddphi%d_%ddd%ddav%ddda%d_P%drt%do3f-%dC_Pin%dkW_tp%dns_dT%dK_SSX%d_vgmin%dpm_tr%d_CostGrid',...
      dir_name,Ea,12,12,120,120,10000*dd,10000*dav,1000*dda,...
      Nrt,round(3*Nf),Na1,1000*Pin4tp_max,tpulse_max,deltaT_max,SpotSizeX,10*vg_min,BandFlag));
%     'freq_grid', 'dphi_grid', 'Ls_grid', ...
%     'Cia1_grid', 'tp_grid', 'Cia2_grid', 'fr_grid', 'Ep_grid', 'Cia3_grid')
  end
elseif CostFlag == 2
  load(sprintf('%s_CostGrid',dataname));
%      'freq_grid', 'dphi_grid', 'Ls_grid', ...
%      'Cia1_grid', 'tp_grid', 'Cia2_grid', 'fr_grid', 'Ep_grid', 'Cia3_grid')    
end

Lum_f = zeros(nfm,npm,navm);
Eff_f = zeros(nfm,npm,navm);
Pin_f = zeros(nfm,npm,navm);
tp_f  = zeros(nfm,npm,navm);
Ne_f  = zeros(nfm,npm,navm);
Nb_f  = zeros(nfm,npm,navm);
Nc_f  = zeros(nfm,npm,navm);
Nl_f  = zeros(nfm,npm,navm);
Ci_f  = zeros(nfm,npm,navm);
Ce_f  = zeros(nfm,npm,navm);
d1_f  = zeros(nfm,npm,navm);

for jf = 1:nfm 
  f = f_m(jf);
  for jp = 1:npm 
    dphi = dphi_m(jp);
    h = c/f*dphi/360; %[mm] - cell length
% read file
    filename = sprintf('%sE%df%ddphi%ddd%ddav%ddda%d',...
      dir_name,Ea,round(f),dphi,10000*dd,10000*dav,1000*dda);
    disp(filename);
    load(filename);
% to get: 'Ne_m', 'Ncyc', 'Ncel', 'Esmx', 'dTmx', 'Effi', 'Pinp', 'Pout', 'Poul', 'tfil'
    Nstr = size(Ncel,5);
    Effi = abs(Effi);
    Esmx = abs(Esmx);
    dTmx = abs(dTmx);
    Pinp = abs(Pinp);
    Pout = abs(Pout);
    Poul = abs(Poul);
%    dTmx = dT_Cu2CuZr*dTmx; %CuZr dT
%    dTmx = dTmx.^2; %dT^2
    PinpN = (Pinp*f^Nf).^Nrt; %(Pin*f^Nf)^Nrt
    if Na1 == 1
      PoutN = (Pout*f^Nf).^Nrt; %(Pout*f^Nf)^Nrt
    end

    NstOK = 0;
    NstAll = 0;
    Lum_max = zeros(Nstr,navm);
    Eff_max = zeros(Nstr,navm);
    Pin_max = zeros(Nstr,navm);
    Es_max = zeros(Nstr,navm);
    dT_max = zeros(Nstr,navm);
    Ne_max = zeros(Nstr,navm);
    Nb_max = zeros(Nstr,navm);
    Nc_max = zeros(Nstr,navm);
    Nl_max = zeros(Nstr,navm);
    tf_max = zeros(Nstr,navm);
    tr_max = zeros(Nstr,navm);
    da_max = zeros(Nstr,navm);
    d_max = zeros(Nstr,navm,2);
    Ci_max = 1e6*ones(Nstr,navm);
    Ce_max = 1e6*ones(Nstr,navm);

    for i = 1:navm
      disp(sprintf('<a/l>=%f',av_m(i)));
      for i2 = 1:ndam
        Ne = Ne_m(i,i2);
        da = da_m(i2);
        if CalcCostFlag == 1 %calc investment cost 
          disp(sprintf('<da/a>=%f',da));toc
        end
        if Na1 == 1
          aN = av_m(i)*[1+da/2,1-da/2]*c/f; %[mm] - first/last cell iris radia
          aN = aN.^Nrt;
        end
        for j = 1:ndm
%        for j = 7 %const iris thickness of 0.055*lambda
         if 1
%         if d_m(j)> 0.15 %if d/h > 0.15
          for j2 = 1:ndm2
%          for j2 = j:ndm2 % d2>=d1
%          for j2 = j %const iris thickness
           NstAll = NstAll+Nstr;
           d = [d_m(j),d_m2(j2)];
           [Q, vg] = calcCellPars(av_m(i)*[1+da/2,1-da/2], d, f, dphi);
           if min(vg) > vg_min
            tr = 0; %rise time due to finite bandwidth of the structure
            if BandFlag == 1 % correct for the rise time
              tr = 100./(f*0.47*min(vg)^1.25); %[ns]
            end
            Ncycl = Ncyc(i,i2,j,j2); %[rf cycles] - bunch spacings
            dtb = Ncycl/f;           %[ns] - bunch spacing
            tb0 = dtb*(Nb0-1);       %[ns] - reference beam length
            tb  = dtb*(Nb-1);        %[ns] - beam length
% surching among Es=const structures           
            for is = 1:Nstr
              Esm = Esmx(i,i2,j,j2,is);
              Pin = Pinp(i,i2,j,j2,is);
%%%%%%%%%%% rise time derivative limit %%%%%%%%%%%%%%%%%%%%%%%%%%
              tf = tfil(i,i2,j,j2,is);
              dp = Poul(i,i2,j,j2,is)/Pout(i,i2,j,j2,is);
              tp = tb+tf+tr;
%              ind = ones(size(tp));
              ind = (tp < tp_nom+dtb/2)&(tp > tp_nom-dtb/2);
              if Esm < Esurf_max & Pin < Pinmax & Ncycl == Nc_nom
%              if Esm < Esurf_max & Pin < Pinmax
                Ncells = Ncel(i,i2,j,j2,is);
                Ls = (Ncells+180/dphi)*h; %[mm] -structure length
                %tapered pulse scaling
                Eff = Effi(i,i2,j,j2,is);
                dTm = dTmx(i,i2,j,j2,is);
%                tp_T = sqrt(tp)-sqrt(tf+tr)/2;
%                dtm = dTm*tp_T/sqrt(tb0+tf);    %of pulse serface heating dT^2                
                tp_T = tp-(tf/2+tr-(tr+tf)*dp/2);%[ns]
                dtm = dTm*sqrt(tp_T/(tb0+tf)); %of pulse serface heating dT^2                
                %rectangular pulse scaling
                eff = Eff*(tb0+tf)./Nb0.*Nb./tp; %of rf-to-beam efficiency
%%%%%%%%%% Power limit %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                tr_ = tr.*0.1./dp;
                tf_ = tf.*0.1./(1-dp).*(dp<0.9)+tf.*(dp>=0.9);
                tp_P = tb+tf_+tr_;
%                Pinput_max = Pin100ns*(100./(tp_P)).^(1/3)*(a1/aref)*(fref/f);%P*f/a*(tp)^1/3=const
                PinN = PinpN(i,i2,j,j2,is);
                if Na1 == 1 %apply P/C constraint
                  a1N = aN(1);
                  PouN = PoutN(i,i2,j,j2,is);
                  if PinN/aN(1) < PouN/aN(2)
                    PinN = PouN;
                    a1N = aN(2);
                  end
                end
                Pinput_max = Pin100ns_NLCa_N*a1N./tp_P;%P/a^Na1*(tp)^1/Nrt=const
                ind = ind&(dtm < deltaT_max)&(PinN < Pinput_max);
% surch for optimum structure
                if any(ind)
% count all structures which satisfy criteria
                  NstOK = NstOK+1;
                  k = 0;
                  if CostFlag == 0 % maximum efficiency structure
                    ind = ind&(Eff_max(is,i) < eff);
                    if any(ind)
                      [Eff_max(is,i),k] = max(eff.*ind);
                      Lum_max(is,i) = calcBunchLuminocity(Ea, av_m(i), da, f);
                    end
                    if CalcCostFlag == 0 %calc range
                      if Ls < Ls_range(1)
                        Ls_range(1) = Ls;
                      elseif Ls > Ls_range(2)
                        Ls_range(2) = Ls;
                      end
                      ind0 = ind+(~ind)*1e6;
                      L1bx = calcBunchLuminocity(Ea, av_m(i), da, f);
                      % repetition rate for a given peak luminosity L1
                      fr = L1./(Nb.*L1bx*1e34); %[Hz]
                      %Pulse energy
                      Ep = abs(E*Ne*1e8.*Nb./eff); %[kJ]
                      if min(tp.*ind0) < tp_range(1)
                        tp_range(1) = min(tp.*ind0);
                      elseif max(ind.*tp) > tp_range(2)
                        tp_range(2) = max(ind.*tp);
                      end
                      if min(fr.*ind0) < fr_range(1)
                        fr_range(1) = min(fr.*ind0);
                      elseif max(ind.*fr) > fr_range(2)
                        fr_range(2) = max(ind.*fr);
                      end
                      if min(Ep.*ind0) < Ep_range(1)
                        Ep_range(1) = min(Ep.*ind0);
                      elseif max(ind.*Ep) > Ep_range(2)
                        Ep_range(2) = max(ind.*Ep);
                      end
                      if Ep_range(1) < 0
                          disp('Ep<0')
                          Ep_range
                          return
                      end
                    end
                  elseif CostFlag == 1  % minimum total cost structure
                    Cost_min = Ci_max(is,i) + Ce_max(is,i);
                    L1bx = calcBunchLuminocity(Ea, av_m(i), da, f);
                    % repetition rate for a given peak luminosity L1
                    fr = L1./(Nb.*L1bx*1e34); %[Hz]
                    %Pulse energy
                    Ep = E*Ne*1e8.*Nb./eff; %[kJ]
                    LNE = L1bx/Ne*eff; %[a.u.]
                    Ce = calcElectricityCost(LNE); %[GigaCHF]
                    if CalcCostFlag == 1 %calc investment cost
                      for ib = Nb
                        if ind(ib)
                          Cib = calcCost(Ea,f,dphi,Ls,tp(ib),LNE(ib),fr(ib),Ep(ib));
                          Cost = Cib + Ce(ib);
                          if Cost_min > Cost 
                            Cost_min = Cost; 
                            k = ib;
                          end
                        end
                      end
                    else %interpolate investment cost
                      Cia1 = interpn(freq_grid, dphi_grid, Ls_grid, Cia1_grid,...
                                     f, dphi, Ls,'linear');
                      Cia2 = interp1(tp_grid, Cia2_grid, tp,'linear');
                      Cia3 = interpn(fr_grid, Ep_grid, Cia3_grid, fr, Ep,'linear');
                      Ci = Cia1 + Cia2 + Cia3; 
                      ind = ind&(Cost_min > Ci+Ce);
                      if any(ind)
                        [Cost_min,k] = min((Ci+Ce).*(ind+(1-ind)*1e6));
                      end                      
                    end
                    if k
                      Ci_max(is,i) = Cost_min-Ce(k);
                      Ce_max(is,i) = Ce(k);
                      Eff_max(is,i) = eff(k);
                      Lum_max(is,i) = L1bx;
                    end
                  elseif CostFlag == 2  % minimum investment cost structure
                    Cost_min = Ci_max(is,i);
                    L1bx = calcBunchLuminocity(Ea, av_m(i), da, f);
                    % repetition rate for a given peak luminosity L1
                    fr = L1./(Nb.*L1bx*1e34); %[Hz]
                    %Pulse energy
                    Ep = E*Ne*1e8.*Nb./eff; %[kJ]
                    LNE = L1bx/Ne*eff; %[a.u.]
                    Ce = calcElectricityCost(LNE); %[GigaCHF]
                    if CalcCostFlag == 1 %calc investment cost
                      for ib = Nb
                        if ind(ib)
                          Cib = calcCost(Ea,f,dphi,Ls,tp(ib),LNE(ib),fr(ib),Ep(ib));
                          Cost = Cib;
                          if Cost_min > Cost 
                            Cost_min = Cost; 
                            k = ib;
                          end
                        end
                      end
                    else %interpolate investment cost
                      Cia1 = interpn(freq_grid, dphi_grid, Ls_grid, Cia1_grid,...
                                     f, dphi, Ls,'linear');
                      Cia2 = interp1(tp_grid, Cia2_grid, tp,'linear');
                      Cia3 = interpn(fr_grid, Ep_grid, Cia3_grid, fr, Ep,'linear');
                      Ci = Cia1 + Cia2 + Cia3; 
                      ind = ind&(Cost_min > Ci);
                      if any(ind)
                        [Cost_min,k] = min((Ci).*(ind+(1-ind)*1e6));
                      end                      
                    end
                    if k
                      Ci_max(is,i) = Cost_min;
                      Ce_max(is,i) = Ce(k);
                      Eff_max(is,i) = eff(k);
                      Lum_max(is,i) = L1bx;
                    end
                  end
                  if k
                    Pin_max(is,i) = Pin;
                    Es_max(is,i) = Esm;
                    dT_max(is,i) = dtm(k);
                    Ne_max(is,i) = Ne;
                    Nb_max(is,i) = k;
                    Nc_max(is,i) = Ncycl;
                    Nl_max(is,i) = Ncells;
                    tf_max(is,i) = tf;
                    tr_max(is,i) = tr;
                    da_max(is,i) = da;
                    d_max(is,i,:) = d;
                  end
                end
              end
            end
           end
          end
        end %if d>0.05
        end
      end
    end
    tp_max = (tr_max+tf_max+(Nb_max-1).*Nc_max./f);
    LNE_max = Eff_max.*Lum_max./Ne_max;    
    
    LNEm = zeros(1,navm);
    Lumm = zeros(1,navm);
    Effm = zeros(1,navm);
    Pinm = zeros(1,navm);
    tpm = zeros(1,navm);
    trm = zeros(1,navm);
    Nem = zeros(1,navm);
    Nbm = zeros(1,navm);
    Ncm = zeros(1,navm);
    Nlm = zeros(1,navm);
    Cim = zeros(1,navm);
    Cem = zeros(1,navm);
    d1m = zeros(1,navm);
    imax = ones(1,navm);
    if Nstr > 1
      if CostFlag == 0 % maximum luminosity per power
        [LNEm,imax] = max(LNE_max); 
      elseif  CostFlag == 1 % minimum total cost structure
        [Cost,imax] = min(Ci_max+Ce_max); 
      elseif  CostFlag == 2 % minimum investment cost structure
        [Cost,imax] = min(Ci_max); 
      end
    else
      if CostFlag == 0 % maximum luminosity per power
        LNEm = LNE_max; 
      elseif  CostFlag == 1 % minimum total cost structure
        Cost = Ci_max+Ce_max; 
      elseif  CostFlag == 2 % minimum investment cost structure
        Cost = Ci_max; 
      end
    end
    for is = 1:Nstr
      Lumm = Lumm + (imax(:)'==is).*Lum_max(is,:);
      Effm = Effm + (imax(:)'==is).*Eff_max(is,:);
      Pinm = Pinm + (imax(:)'==is).*Pin_max(is,:);
      tpm = tpm + (imax(:)'==is).*tp_max(is,:);
      trm = trm + (imax(:)'==is).*tr_max(is,:);
      Nem = Nem + (imax(:)'==is).*Ne_max(is,:);
      Nbm = Nbm + (imax(:)'==is).*Nb_max(is,:);
      Ncm = Ncm + (imax(:)'==is).*Nc_max(is,:);
      Nlm = Nlm + (imax(:)'==is).*Nl_max(is,:);
      d1m = d1m + (imax(:)'==is).*d_max(is,:,1);
    end
    if CostFlag == 0 % maximum luminosity per power
      % repetition rate for a given peak luminosity L1
      frm = L1./(Nbm.*Lumm*1e34); %[Hz]
      %Pulse energy
      Epm = E*Nem.*1e8.*Nbm./Effm; %[kJ]
      for i = 1:navm
        if tpm(i) > 0
          Cim(i) = ...
            calcCost(Ea,f,dphi,(Nlm(i)+180/dphi)*h,tpm(i),LNEm(i),frm(i),Epm(i)); %[a.u.]
        end
      end
      Cem = calcElectricityCost(LNEm); %[a.u.]
    else % minimum (total or investment) cost structure
      LNEm = Lumm./Nem.*Effm;
      for is = 1:Nstr
        Cim = Cim + (imax(:)'==is).*Ci_max(is,:);
        Cem = Cem + (imax(:)'==is).*Ce_max(is,:);
      end
    end
     
    if nfm == 1 & npm == 1
      if 1
        file = fopen(sprintf('E%df%ddphi%ddd%ddav%ddda%d_Cost%d.out',...
          Ea,round(f),dphi,10000*dd,10000*dav,1000*dda,CostFlag),'w');
        if Nstr == 1
          Type(1,:) = 'Nc';
        elseif Nstr == 4
          Type(1,:) = 'Es';
          Type(2,:) = 'dT';
          Type(3,:) = 'Ef';
          Type(4,:) = 'Pa';
        else
          for i=1:Nstr
            if i < 10
              Type(i,:) = sprintf('0%d',i);
            else
              Type(i,:) = sprintf('%d',i);
            end
          end
        end
        fprintf(file,'  <a>/l Type Eff Lbx*10^34 N*10^9 L/N*Eff In.Cost Es_max dT_max  Pin   tp   tr   Nb Ns (da/a; d1/h, d2/h)  Nc\r');
        fprintf(file,'             [%%]   [1/m^2]         [a.u.]  [a.u.] [MV/m]    [K] [MW]  [ns] [ns]\r');
        for i=1:navm  
          fprintf(file,' %7.4f %2s %5.1f %8.2f %6.2f %7.2f %7.2f %6.1f %6.1f %4.0f %5.1f %4.1f %3d %2d (%4.2f;%5.3f,%5.3f) %3d\r',...
            av_m(i), Type(imax(i),:), Effm(i), Lumm(i), Nem(i), LNEm(i), Cim(i),...
            Es_max(imax(i),i), dT_max(imax(i),i), Pinm(i), tpm(i), trm(i), Nbm(i), Ncm(i),...
            da_max(imax(i),i), d1m(i), d_max(imax(i),i,2), Nlm(i));
        end
        fclose(file);
      end
      if 1 %linac paraqmeters calculation
        if CostFlag == 0 % maximum luminosity per power
        % Figure of merite 
          [mLNEm, iav] = max(LNEm); 
        elseif CostFlag == 1 % minimum total cost structure
          [mCtm, iav] = min(Cim+Cem); 
        elseif CostFlag == 2 % minimum investment cost structure
          [mCtm, iav] = min(Cim); 
        end    
        mL1bx = Lumm(iav);
        [mL1bx, mLtbx, sz] = calcBunchLuminocity(Ea, av_m(iav), da_max(imax(iav),iav), f);
        % repetition rate for a given peak luminosity L1
        fr = L1./(Nbm(iav).*mL1bx*1e34); %[Hz]
        % total luminosity at frep
        Lt = fr*(Nbm(iav).*mLtbx*1e34); %[Hz/m^2]
        %Beam power/beam
        Pb = E/2*(Nem(iav)*1e9).*Nbm(iav).*fr/1000000; %[MW]
        %Input power/linac
        Pl = Pb./(Effm(iav)/100); %[MW]
        %Pulse energy/linac
        Ep = 1000*Pl/fr; %[kJ]
        disp(sprintf(' Ea = %d [MV/m], f = %d [GHz], dphi = %d [deg], <a>/lambda = %6.4f',Ea,f,dphi,av_m(iav)))
        disp(sprintf(' frep = %g [Hz], Ltotal = %g [Hz/m^2], sigma_z = %g [um]',fr,Lt,sz))
        disp(sprintf(' Pbeam = %g [MW/beam], Pin = %g [MW/linac], Ep = %g [kJ/linac]',Pb,Pl,Ep))
        disp(sprintf(' Inv. Cost = %g [a.u.], 10-years El. Cost = %g [a.u.]',Cim(iav),Cem(iav)))       
      end
      figure(Ea+CostFlag+1)
      plot(av_m,60*LNEm,'r-',av_m,500*Lumm./Nem,'g-',av_m,Effm,'b-',av_m,Pinm,'m-',...
           av_m,Nbm/10,'kx',av_m,10*Ncm,'kd',av_m,tpm/10,'c-',...
           av_m,(Nlm+180/dphi)*h/10,'r.',av_m,10*(Cim+Cem),'b.',...
           'LineWidth',2)
      axis([.09,.21,0,150]); grid on;
      xlabel('<a/\lambda>')
      ylabel('\eta_r_f_-_b_e_a_m[%], P_i_n[MW], N_b/10, N_c_y_c_l_e_s, t_p/10[ns], l[cm], 10*Cost[a.u.]')
      legend('60FoM','500L/N','\eta_r_f_-_b_e_a_m','P_i_n','N_b/10','10N_c_y_c_l_e_s','t_p/10','l','Cost')
      title(sprintf('NsOK/Ns=%d/%d, E_a=%dMV/m, f=%6.3fGHz, dphi=%d^o',NstOK,NstAll,Ea,f,dphi)) 
    end
    Lum_f(jf,jp,:) = Lumm;
    Eff_f(jf,jp,:) = Effm;
    Pin_f(jf,jp,:) = Pinm;
    tp_f(jf,jp,:) = tpm;
    Ne_f(jf,jp,:) = Nem;
    Nb_f(jf,jp,:) = Nbm;
    Nc_f(jf,jp,:) = Ncm;
    Nl_f(jf,jp,:) = Nlm;
    Ci_f(jf,jp,:) = Cim;
    Ce_f(jf,jp,:) = Cem;
    d1_f(jf,jp,:) = d1m;
  end
end
%save(sprintf('%s_Cost%d',dataname,CostFlag),...
%  'f_m', 'dphi_m', 'av_m', 'Lum_f', 'Eff_f', 'Pin_f', 'tp_f',...
%  'Ne_f', 'Nb_f', 'Nc_f', 'Nl_f', 'Ci_f', 'Ce_f', 'd1_f')
toc
end
end
