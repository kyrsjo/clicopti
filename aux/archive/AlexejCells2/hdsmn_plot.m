% testing hdsmn subroutines
%a and d are radius and iris thickness) / lambda X 10

close all
clear all

global EAC_LOAD EAC_UNLOAD %[MV/m]

c = 299.792458; %speed of light [mm/ns]
E = 3e12*1.6e-19; %[V*C] beams energy 3TeV
%L1 = 3.3e38; %[Hz/m^2] given peak Luminosity for 1.0Ne@?Hz 
%E = 0.5e12*1.6e-19; %[V*C] beams energy at 500 GeV
L1 = 2.03e38; %[Hz/m^2] given peak Luminosity for 0.7Ne@50Hzand em20mk 

f = 12; %working frequency in GHz
Ea = 100; %[MV/m] averaged loaded accelerating gradient
%Ea = 50; %[MV/m] averaged loaded accelerating gradient
dphi = 120; %[degree] phase advance per cell
%Ncycl0 = 20; %number of RF periods between the bunches
Wxmax0 = 10; %reference max value of wake at the second bunch [V/pC/mm/m] at 3TeV
Nemax0 = 4; %reference max number of electrons per bunch [10^9]
Eamax0 = 150; %reference max avereged accelerrating gradient [MV/m]
Nb0 = 154; %reference number of bunches in the train
dT_Cu2CuZr = sqrt(551/511); %coefficient to take CuZr instead of Cu
dT_Cu2Ti = sqrt(58/1.8); %coefficient to take Ti instead of Cu
NcC = 1.5;% - Zero-ggradient coupler length / cell length

% T28vg2.4 - prototype
if 0
name = 'T53vg3MC'
name = 'T53vg3MC_QDS'
f = 11.424; %[GHz]
Ea = 100; %[MV/m]
dphi = 120;
da= 0.204; %da/a
a = 0.1344*[1+da/2,1,1-da/2]; %a/lambda
h = c/f*dphi/360;%[mm]
d = 1.66/h*[1,1,1] %d/h
Nc = 30; Nb = 52; Ncycl = 8; %18Wu in _wds %half length T28_vg2.4
end

% WDS120_CLIC 3/4 of CLIC structure at 11.424 GHz
if 0
name = 'CLIC_vg1_11424MHz'
%name = 'CLIC_vg1_11424MHz_nds'
f = 11.424; %[GHz]
Ea = 100; %[MV/m]
dphi = 120;
a1 = 4.06;
a2 = 2.66;
av = (a1+a2)/2;
da = (a1-a2)/av; %da/<a>
al = av/(c/f);   %<a>/lambda
a = al*[1+da/2,1,1-da/2] %a/lambda
h = c/f*dphi/360;%[mm]
d1 = 2.794;
d2 = 1.314;
d = [d1,(d1+d2)/2,d2]/h; %d/h
Nc = 20; Nb = 262; Ncycl = 8; Ne=4%18 Wu
Nc = 18; Nb = 286; Ncycl = 8; Ne=0%18 Wu
end



% 12 GHz test structures for Nc=26, Ncycl=6
if 0
name = 'CLIC_G' 
name = 'CLIC_G_r05_adw11_idw8p0' 
name = 'TD24' 
switch name
  case 'CLIC_C' %CLIC structure
  da= 0.58; %da/a
  a = 0.12*[1+da/2,1,1-da/2]; %a/lambda
  d = [.32,.21,.1]; %d/h
  Nc = 26; Nb = 311; Ncycl = 8; Ne = 4.0%18Wu
  case 'CLIC_E' %Es=300MV/m;dT=60K
  da= 0.55; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.28,.19,.1]; %d/h
  Nc = 26; Nb = 510; Ncycl = 6;%18Wu
  case 'CLIC_F' %Es=220MV/m;dT=60K
  da= 0.13; %da/a
  a = 0.145*[1+da/2,1,1-da/2]; %a/lambda
  d = [.34,.33,.32]; %d/h
  Nc = 26; Nb = 50; Ncycl = 13;%18Wu
  case 'CLIC_H' %Es=260MV/m;dT=60K
  da= 0.48; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.34,.22,.1]; %d/h
  Nc = 26; Nb = 422; Ncycl = 6;%18Wu
  case 'CLIC_G' %Es=260MV/m;dT=36K
  da= 0.29; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC = 1.5; Nc = 26; Nb = 312; Ncycl = 6; Ne = 3.72;%18Wu
  NcC = 1.5; Nc = 26; Nb = 796; Ncycl = 6;
  NcC = 1.5; Nc = 26; Nb = 1764; Ncycl = 6;
  NcC = 1.5; Nc = 26; Nb = 2732; Ncycl = 6;
  case 'TD24' %
  da= 0.29; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC = 1.5; Nc = 26; Nb = 312; Ncycl = 6; Ne = 3.72;
  NcC = 0; Nc = 24; Nb = 312; Ncycl = 6; Ne = 3.72;
%  NcC = 0; Nc = 24; Nb = 312; Ncycl = 6; Ne = 0;
  case 'T24' %
  da= 0.29; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC = 0; Nc = 24; Nb = 312; Ncycl = 6; Ne = 3.72;
%  NcC = 0; Nc = 24; Nb = 312; Ncycl = 6; Ne = 0;
  case 'CLIC_G_r05_adw11_idw8p0' % OR TD24_vg1p8_diskR05  OR TD26_vg1p8_R05_CC
  da= 0.29; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC = 0; Nc = 24; Nb = 312; Ncycl = 6; Ne = 3.72;
  NcC = 0; Nc = 24; Nb = 312; Ncycl = 6; Ne = 0;
  % OR TD26_vg1p8_R05_CC regular cells
  NcC = 0; Nc = 26; Nb = 312; Ncycl = 6; Ne = 3.72;
%  NcC = 0; Nc = 26; Nb = 312; Ncycl = 6; Ne = 0;
  % OR TD26_vg1p8_R05_CC all cells
%  NcC = 0; Nc = 28; Nb = 312; Ncycl = 6; Ne = 3.72;
%  NcC = 0; Nc = 28; Nb = 312; Ncycl = 6; Ne = 0;
  case 'CLIC_G_r05_adw11_idw8p1' 
  da= 0.29; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC = 0; Nc = 24; Nb = 312; Ncycl = 6; Ne = 3.72;
%  NcC = 0; Nc = 24; Nb = 312; Ncycl = 6; Ne = 0;
  case 'CLIC_G_r05_adw11_idw8p2'
  da= 0.29; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC = 0; Nc = 24; Nb = 312; Ncycl = 6; Ne = 3.72;
%  NcC = 0; Nc = 24; Nb = 312; Ncycl = 6; Ne = 0;
end
end
% CLIC structures for klystron based 500GeV
if 1
Wxmax0 = 20; %reference max value of wake at the second bunch [V/pC/mm/m] at 500GeV
name = 'KLIC500_3'
switch name
  case 'KLIC500_3' 
  Ea=67;
  dphi=120;
  da= 0.55; %da/a
  a = 0.14*[1+da/2,1,1-da/2]; %a/lambda
  d = [.18,.28/2,.1]; %d/h
  Nc = 56; Nb = 335; Ncycl = 6;%
end
end
% CLIC structures for 500GeV
if 0
Wxmax0 = 20; %reference max value of wake at the second bunch [V/pC/mm/m] at 500GeV
name = 'CLIC500_2'
switch name
  case 'CLIC500_2' 
  Ea=80;
  dphi=150;
  da= 0.19; %da/a
  a = 0.145*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.18,.16]; %d/h
  Nc = 21; Nb = 354; Ncycl = 6;%
%%%%%%%%
%  Ea = 73.7;
%  Nc = 21; Nb = 354; Ncycl = 6;% 500GeV operation with Pin=64MW, N=6.51
%  Ea = 72.9;
%  Nc = 21; Nb = 354; Ncycl = 6; Ne = 6.8;% 500GeV operation with Pin=64MW
%%%%%%%%
%  Ea=82;
%  Nc = 21; Nb = 312; Ncycl = 6; Ne = 3.72;% 3TeV operation with Pin=64MW
  case 'CLIC500_3' 
  Ea=50;
  dphi=150;
  da= 0.45; %da/a
  a = 0.16*[1+da/2,1,1-da/2]; %a/lambda
  d = [.1,.1,.1]; %d/h
  Nc = 45; Nb = 810; Ncycl = 6;%
  case 'CLIC500_4' 
  Ea=80;
  dphi=150;
  da= 0.13; %da/a
  a = 0.135*[1+da/2,1,1-da/2]; %a/lambda
  d = [.16,.15,.14]; %d/h
  Nc = 21; Nb = 338; Ncycl = 6;%
%%%%%%%%
  Ea=87;
  Nc = 21; Nb = 312; Ncycl = 6; Ne = 3.72;% 3TeV operation with Pin=64MW
  case 'CLIC500_6' 
  Ea=80;
  dphi=120;
  da= 0.34; %da/a
  a = 0.12*[1+da/2,1,1-da/2]; %a/lambda
  d = [.28,.21,.14]; %d/h
  Nc = 26; Nb = 819; Ncycl = 6;%
%%%%%%%%
%  Ea=93.7;
%  Nc = 26; Nb = 312; Ncycl = 6; Ne = 3.72;% 3TeV operation with Pin=64MW
  case 'CLIC500_7' 
  Ea=67;
  dphi=150;
  da= 0.33; %da/a
  a = 0.155*[1+da/2,1,1-da/2]; %a/lambda
  d = [.32,.24,.16]; %d/h
  Nc = 21; Nb = 595; Ncycl = 6;%
end
end

% CLIC structures for Nc=27(compact coupler), Ncycl=6
if 0
name = 'CLIC_G_r05'
switch name
  case 'CLIC_G_nds' 
  da= 0.29; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC=0; Nc = 27; Nb = 312; Ncycl = 6; Ne = 3.72;%
  case 'CLIC_G_nds_r05' 
  da= 0.29; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC=0; Nc = 27; Nb = 312; Ncycl = 6; Ne = 3.72;%
  case 'CLIC_G' 
  da= 0.29; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC=0; Nc = 27; Nb = 312; Ncycl = 6; Ne = 3.72;%
  case 'CLIC_G_r05' 
  da= 0.29; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC=0; Nc = 27; Nb = 312; Ncycl = 6; Ne = 3.72;%
%  NcC=0; Nc = 27; Nb = 316; Ncycl = 6; Ne = 3.72;%
  case 'CLIC_G_r05_idw8p2' 
  da= 0.29; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC=0; Nc = 27; Nb = 312; Ncycl = 6; Ne = 3.72;%
%  NcC=0; Nc = 27; Nb = 314; Ncycl = 6; Ne = 3.72;%
  case 'CLIC_GLDT_idw7p5'
  da= 0.29; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC=0; Nc = 27; Nb = 312; Ncycl = 6; Ne = 3.72;%
  NcC=0; Nc = 27; Nb = 314; Ncycl = 6; Ne = 3.72;%
  case 'CLIC_GLDT_idw7p5_ydw10'
  da= 0.29; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC=0; Nc = 27; Nb = 312; Ncycl = 6; Ne = 3.72;%
  NcC=0; Nc = 27; Nb = 316; Ncycl = 6; Ne = 3.72;%
  case 'CLIC_GLDT_dgdw'
  da= 0.29; %da/a
  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC=0; Nc = 27; Nb = 312; Ncycl = 6; Ne = 3.72;%
  NcC=0; Nc = 27; Nb = 318; Ncycl = 6; Ne = 3.72;%
  case 'CLIC_I'
  da= 0.3652; %da/a
  a = 0.115*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC=0; Nc = 27; Nb = 312; Ncycl = 6; Ne = 3.72;%
  NcC=0; Nc = 27; Nb = 332; Ncycl = 6; Ne = 3.72;%
  NcC=0; Nc = 27; Nb = 332; Ncycl = 6; Ne = 0%
  case 'CLIC_J'
  da= 0.3158; %da/a
  a = 0.114*[1+da/2,1,1-da/2]; %a/lambda
  d = [.199,.168,.137]; %d/h
  NcC=0; Nc = 27; Nb = 312; Ncycl = 6; Ne = 3.72;%
  NcC=0; Nc = 27; Nb = 326; Ncycl = 6; Ne = 3.72;%
  NcC=0; Nc = 27; Nb = 326; Ncycl = 6; Ne = 0;%
  case 'CLIC_K'
  da= 0.3363; %da/a
  a = 0.113*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC=0; Nc = 27; Nb = 312; Ncycl = 6; Ne = 3.72;%
  NcC=0; Nc = 27; Nb = 326; Ncycl = 6; Ne = 3.72;%
  NcC=0; Nc = 27; Nb = 326; Ncycl = 6; Ne = 0;%
  case 'CLIC_L'
  a1=3.3;a2=2.3;
  al = (a1+a2)/2/25
  da= 2*(a1-a2)/(a1+a2); %da/a
  a = al*[1+da/2,1,1-da/2]; %a/lambda
  d = [.2,.16,.12]; %d/h
  NcC=0; Nc = 27; Nb = 312; Ncycl = 6; Ne = -3.72;%
end
end

format long

%initDatafromDaniel(0.1);
initDatafromDaniel500GeV('norm',Ea); Ne =-1;
if Ne < 0
    Ne = calcBunchCharge(Ea, (a(1)+a(3))/2, da, 12)
end
    %return;
[Q1, f1, A1] = calcCellWake(a, d, f, dphi);
switch name
  case 'CLIC_G'
    Q1 =[10,7.7,6.3]; 
%    Q1 =[8,8,8]; 
    f1 =[16.74, 17.21, 17.67];
    A1 =[117, 140, 156]*5/4; 
  case 'CLIC_G_r05'
    Q1 =[11.1, 8.0,  6.1]; 
    f1 =[16.43,16.88,17.35];
    A1 =[110,  134,  157]*5/4; 
  case 'CLIC_G_r05_idw8p2'
    Q1 =[8.4,  6.0,  4.9]; 
    f1 =[16.41,16.90,17.43];
    A1 =[147,  186,  211]; 
  case 'CLIC_GLDT_idw7p5'
    Q1 =[6.22, 5.12, 4.66]; 
    f1 =[16.41,17.00,17.54];
    A1 =[163,  197,  215]; 
  case 'CLIC_GLDT_idw7p5_ydw10'
    Q1 =[6.93, 6.15, 5.84]; 
    f1 =[16.64,17.22,17.76];
    A1 =[164,  188,  199]; 
  case 'CLIC_GLDT_dgdw'
    Q1 =[9.6,  7.1,  6.2]; 
    f1 =[16.55,17.11,17.64];
    A1 =[118,  148,  162]*5/4; 
  case 'CLIC_I'
    Q1 =[11.6,  7.7,  6.2]; 
    f1 =[16.26,16.93,17.64];
    A1 =[104,  140,  162]*5/4; 
  case 'CLIC_J'
    Q1 =[11.1,  7.7,  6.5]; 
    f1 =[16.37,16.95,17.57];
    A1 =[106,  141,  160]*5/4; 
  case 'CLIC_K'
    Q1 =[7.21, 6.21, 5.84]; 
    f1 =[16.45,17.13,17.76];
    A1 =[155,  185,  199]; 
  case 'CLIC_G_r05_adw11_idw8p1' 
end    
Wx_max = Wxmax0*Nemax0/Ne*Ea/Eamax0;
Ncyc = calcBunchSpacing(Nc, Q1, f1, A1, Wx_max, f);
Ncycl = 6;
%return;

[Q, vg, rQ, Es, dT] = calcCellPars(a, d, f, dphi)
Hs = [0,0,0];
Sc = [0,0,0];
switch name
  case 'T53vg3MC'
    Q=[6808,6785,6779]; 
    vg=[3.29,2.39,1.64];
    rQ=[13509,14600,15713]; 
    Es=[2.0,1.95,1.9];
    Sc=[0.61,0.49,0.42];%[mA/V]
    Hs=[2.75,2.65,2.6];
  case 'T53vg3MC_QDS'
    Q=[5480,5433,5400]; 
    vg=[2.86,2.06,1.42];
    rQ=[11708,12615,13547]; 
    Es=[1.95,1.9,1.8];
    Sc=[0.63,0.52,0.43];%[mA/V]
    Hs=[4.6,4.6,4.5];
  case 'CLIC_vg1_11424MHz'
    Q =[5840, 6036, 6310]; 
    vg=[2.41, 1.56, 0.92];
    rQ=[10940,13405,16180]; 
    Es=[2.0,2.0,2.0];
    Sc=[0.58,0.44,0.32];%[mA/V]
    Hs=[4.9,4.4,4.0];
  case 'CLIC_vg1_11424MHz_nds'
    Q =[6755, 7218, 7596]; 
    vg=[2.62, 1.70, 1.02];
    rQ=[11825,14587,17700]; 
    Es=[2.0,2.0,2.0];
    Sc=[0.56,0.4,0.32];%[mA/V]
    Hs=[3.2,2.8,2.3];
  case 'CLIC_C'
    Q =[5736,6036,6282]; 
    vg=[2.42,1.36,0.66];
    rQ=[12228,14978,18730]; 
    Es=[2.05,1.9,1.95];
    Hs=[4.9,4.3,3.7];%[mA/V]
    Sc=[0.58,0.4,0.3];%[mA/V]
  case 'CLIC_G_nds'
    Q =[7228,7431,7619]; 
    vg=[1.82,1.316,0.915];
    rQ=[15856,17666,19544]; 
    Es=[1.9,1.9,1.9];
    Hs=[2.6,2.43,2.27];
    Sc=[0.37,0.32,0.28];%Sc6[mA/V]
  case 'CLIC_G_nds_r05'
    Q =[6655,6818,6974]; 
    vg=[1.826,1.324,0.922];
    rQ=[15956,17746,19650]; 
    Es=[1.9,1.9,1.9];
    Hs=[2.6,2.43,2.27];
    Sc=[0.37,0.32,0.28];%Sc6[mA/V]
  case 'T24' %HFSSv13,ds=1um
    Q =[6660, 6824, 6990]; 
    vg=[1.815,1.317,0.917];
    rQ=[15994,17816,19689]; 
    Es=[2.00, 2.02, 2.05];
    Hs=[2.64, 2.45, 2.29];
    Sc=[0.375,0.336,0.30];%Sc6[mA/V]
  case 'CLIC_G'
    Q =[6100,6177,6265]; 
    vg=[1.66,1.19,0.83];
    rQ=[14597,16236,17882]; 
    Es=[1.9,1.9,1.9];
    Hs=[4.2,4.0,3.8];
    Sc5=[0.42,0.37,0.32];%Sc5[mA/V]
    Sc=[0.39,0.34,0.29];%Sc6[mA/V]
  case 'TD24' %HFSSv13,ds=1um
    Q =[5654, 5736, 5822]; 
    vg=[1.617,1.168,0.811];
    rQ=[14271,15840,17443]; 
    Es=[2.05, 2.05, 2.08];
    Hs=[4.75, 4.60, 4.45];
    Sc=[0.42, 0.36, 0.315];%Sc6[mA/V]
  case 'CLIC_G_r05'
    Q =[5617,5713,5813]; 
    vg=[1.69,1.22,0.85];
    rQ=[14890,16555,18294]; 
    Es=[1.9,1.9,1.9];
    Hs=[3.9,3.7,3.5];
    Sc=[0.39,0.34,0.29];%Sc6[mA/V]
  case 'CLIC_G_r05_idw8p2'
    Q =[5556,5653,5756]; 
    vg=[1.67,1.21,0.84];
    rQ=[14721,16402,18163]; 
    Es=[1.9,1.9,1.9];
    Hs=[4.05,3.83,3.6];
    Sc=[0.39,0.34,0.29];%Sc6[mA/V]
  case 'CLIC_GLDT_idw7p5'
    Q =[5512,5665,5794]; 
    vg=[1.65,1.205,0.84];
    rQ=[14577,16305,18095]; 
    Hs=[3.75,3.51,3.3];
    Es=[1.9,1.9,1.9];
    Sc=[0.39,0.34,0.29];%Sc6[mA/V]
  case 'CLIC_GLDT_idw7p5_ydw10'
    Q =[5569,5737,5777]; 
    vg=[1.675,1.225,0.855];
    rQ=[14761,16516,18298]; 
    Hs=[3.75,3.51,3.3];
    Es=[1.9,1.9,1.9];
    Sc=[0.39,0.34,0.29];%Sc6[mA/V]
  case 'CLIC_GLDT_dgdw'
    Q =[5682,5796,5913]; 
    vg=[1.705,1.23,0.86];
    rQ=[14954,16695,18465]; 
    Hs=[3.56,3.35,3.13];
    Es=[1.9,1.9,1.9];
    Sc=[0.39,0.34,0.29];%Sc6[mA/V]
  case 'CLIC_I'
    Q =[5679,5802,5913]; 
    vg=[2.215,1.44,0.86];
    rQ=[14148,16262,18465]; 
    Hs=[3.62,3.35,3.13];
    Es=[2.0,1.95,1.9];
    Sc=[0.45,0.36,0.29];%Sc6[mA/V]
  case 'CLIC_J'
    Q =[5605,5781,5881]; 
    vg=[2.005,1.355,0.85];
    rQ=[14486,16285,18152]; 
    Hs=[3.575,3.4,3.18];
    Es=[1.95,1.92,1.9];
    Sc=[0.425,0.35,0.27];%Sc6[mA/V]
  case 'CLIC_K'
    Q =[5562,5733,5777]; 
    vg=[1.97,1.34,0.855];
    rQ=[14283,16267,18298]; 
    Hs=[3.8,3.55,3.3];
    Es=[1.96,1.93,1.9];
    Sc=[0.425,0.35,0.29];%Sc6[mA/V]
  case 'CLIC_G_r05_adw11_idw8p0' %TD24_vg1.8_diskR05, TD26_vg1.8_diskR05_CC
    Q =[5536,5635,5738]; 
    vg=[1.65,1.2,0.83];
    rQ=[14587,16220,17954]; 
    Es=[1.95,1.93,1.9];
    Hs=[4.1,3.85,3.6];
    Sc=[0.41,0.35,0.3];%Sc6[mA/V]
  case 'CLIC_G_r05_adw11_idw8p1' 
    Q =[5471,5576,5689]; 
    vg=[1.64,1.19,0.83];
    rQ=[14468,16152,17797]; 
    Es=[1.95,1.93,1.9];
    Hs=[4.05,3.8,3.55];
    Sc=[0.37,0.34,0.28];%Sc6[mA/V]
  case 'CLIC_G_r05_adw11_idw8p2' 
    Q =[5458,5551,5655]; 
    vg=[1.635,1.19,0.82];
    rQ=[14395,16022,17767]; 
    Es=[1.95,1.93,1.9];
    Hs=[4.1,3.85,3.6];
    Sc=[0.37,0.34,0.28];%Sc6[mA/V]
end 
if Hs > 0
    dT = 2.56.*(1.5.*Hs).^2*sqrt(f/29.985)
end
    %Q = Q*sqrt(0.95);
%return


figure(3)
[Eff, tp, Pin, tp_P, Scmax, dTmax] = plotStructPars(NcC, Nc, dphi, f, Ea, Q, vg, rQ, Es, Sc, dT, Ne, Ncycl, Nb)
PCt = Pin/2/pi/(a(1)*c/f)*tp_P^(1/3)
Sct = Scmax*tp_P^(1/3)
dTmax
L1bx = calcBunchLuminocity(Ea, a(2), da, f)
L1bx/Ne*Eff
return

if 0
[L1bx, Ltbx, sz] = calcBunchLuminocity(Ea, a(2), da, f)
f_m = f;
dphi_m = dphi;
av_m = a(2);
Lum_f = L1bx;
Eff_f = Eff;
Pin_f = Pin;
tp_f = tp;
Ne_f = Ne;
Nb_f = Nb;
Nc_f = Ncycl;
Nl_f = Nc;
Ci_f = 0;
Ce_f = 0;
d1_f = d(1);
save(sprintf('data500GeV_norm\\CLIC_G_Ea%d_tp483ns',Ea),...
  'f_m', 'dphi_m', 'av_m', 'Lum_f', 'Eff_f', 'Pin_f', 'tp_f',...
  'Ne_f', 'Nb_f', 'Nc_f', 'Nl_f', 'Ci_f', 'Ce_f', 'd1_f')
return
end

tr = 100./(f*0.47*(min(vg))^1.25) %[ns]

disp('a');disp(a*c/f)
disp('d');disp(d*c/f*dphi/360)
disp('Ls');disp((Nc+180/dphi)*c/f*dphi/360)
[L1bx, Ltbx, sz] = calcBunchLuminocity(Ea, a(2), da, f)
%L1bx = 2.5; Ltbx = 0;; sz = 0;
%L1bx = 1.3; Ltbx = 0;; sz = 0;
% repetition rate for a given peak luminosity L1
fr = L1./(Nb.*L1bx*1e34); %[Hz]
% total luminosity at frep
Lt = fr*(Nb.*Ltbx*1e34); %[Hz/m^2]
%Beam power/beam
Pb = E/2*(Ne*1e9).*Nb.*fr/1000000; %[MW]
%Input power/linac
Pl = Pb./(Eff/100); %[MW]
%Pulse energy/linac
Ep = 1000*Pl/fr; %[kJ]
h = c/f*dphi/360;%[mm]
Ls = (Nc+180/dphi)*h; %[mm]
Ci=0
Ce=0
%Ci = calcCost(Ea,f,dphi,Ls,tp,L1bx*Eff/Ne,fr,2*Ep); %[a.u.]
%Ce = calcElectricityCost(L1bx*Eff/Ne); %[a.u.]
disp(sprintf(' Ea = %d [MV/m], f = %d [GHz], dphi = %d [deg], <a>/lambda = %6.4f',Ea,f,dphi,a(2)))
disp(sprintf(' frep = %g [Hz], Ltotal = %g [Hz/m^2], sigma_z = %g [um]',fr,Lt,sz))
disp(sprintf(' Pbeam = %g [MW/beam], Pin = %g [MW/linac], Ep = %g [kJ/linac]',Pb,Pl,Ep))
disp(sprintf(' Inv. Cost = %g [a.u.], 10-years El. Cost = %g [a.u.]',Ci,Ce))       
return

nc = 10:2:60
[tf,eff,es,dt,pio] = calcStructPars(nc, dphi, f, Ea, Q, vg, rQ, Es, dT, Ne, Ncycl, Nb);
tb0 = Ncycl*(Nb-1)/f;  %[ns] - assumed beam time
dp = pio(3,:)./pio(2,:);
dpP = 0.1;
tr_ = tr.*dpP./dp;
tf_ = tf.*dpP./(1-dp).*(dp<(1-dpP))+tf.*(dp>=(1-dpP));
%tb_ = (60./pio(1,:)).^3*400 - 0.2*(tr+tf); %[ns]- allowed beam time
tb_ = (60./pio(1,:)).^3*400 - tr_-tf_; %[ns]- allowed beam time
tp_ = tr+tf+tb_;
eff = eff.*(tf+tb0)./(tr+tf+tb0);
eff0= eff.*(tr+tf+tb0)./tb0;
eff_= eff0.*tb_./tp_;
%cost calculation
Nb_ = fix(tb_/(Ncycl/f))
%[L1bx, Ltbx, sz] = calcBunchLuminocity(Ea, a(2), da, f);
L1bx = 1.3; Ltbx = 2.5;; sz = 2.5;
fr_ = L1./(Nb_.*L1bx*1e34); %[Hz]
Lt_ = fr*(Nb_.*Ltbx*1e34); %[Hz/m^2]
Pb_ = E/2*(Ne*1e9).*Nb_.*fr_/1000000; %[MW/beam]
Pl_ = Pb_./(eff_/100); %[MW/linac]
Ep_ = 1000*2*Pl_./fr_; %[kJ]
for i = 1:length(nc)
  Ci(i) = calcCost(Ea,f,dphi,(nc(i)+180/dphi)*h,tp_(i),...
                   L1bx*eff_(i)/Ne,fr_(i),Ep_(i)); %[a.u.]
end
Ce = calcElectricityCost(L1bx*eff_/Ne); %[a.u.]

%return

figure(3)
%a*c/f
subplot(4,1,1),plot(nc,es(1,:),'b-',nc,es(2,:),'r-','LineWidth',1),grid,%axis([30,100,250,300])
title(sprintf('d=[%5.3f,%5.3f]mm, a=[%5.3f,%5.3f]mm, E_{acc}^{av}=150MV/m, N_b = %d',d,a,Nb))
ylabel('E_{surf} [MV/m]')
subplot(4,1,2),plot(nc,dt(1,:),'b-',nc,dt(2,:),'r-','LineWidth',1),grid,%axis([30,100,50,60])
ylabel('\DeltaT [K]')
subplot(4,1,3),plot(nc, pio(1,:)/(a(1)*c/f), 'b-', nc, pio(2,:)/(a(3)*c/f), 'r-','LineWidth',1),grid,%axis([30,100,0,15])
ylabel('P/a [MW/mm]')
subplot(4,1,4),plot(nc,eff,'k--',nc,eff_,'k-',nc,tf,'r-','LineWidth',1),grid,%axis([30,100,0,15])
text(100,12,sprintf('N_e=%4.2f*10^9',Ne)) 
ylabel('\eta[%], t_f[ns]')
xlabel('ncells')

figure(4)
ls = (nc+180/dphi)*h/10;
%plot(nc,es(1,:)/10,'g-',nc,es(2,:)/10,'g--','LineWidth',1)
%hold on
%plot(nc,dt(1,:),'b-',nc,dt(2,:),'b--','LineWidth',1)
%plot(nc,pio(1,:)/(a(1)*c/f), 'k-', nc, pio(2,:)/(a(3)*c/f), 'k--','LineWidth',1)
plot(ls,eff0,'r-.',ls,eff_,'r-','LineWidth',3)
hold on
plot(ls,10*Ci,'b-','LineWidth',3)
hold off
grid
axis([10,55,0,35])
%ylabel('E_{surf} [10MV/m], \DeltaT [K], \eta [%]')
ylabel('\eta [%], C_i [a.u.]')
xlabel('L_s [cm]')
legend('\eta steady-state', '\eta fixed Pt_p^1^/^3','10xC_i',2)
return;

'E_{surf} in first cell','E_{surf} in last cell',...
       '\DeltaT in first cell','\DeltaT in last cell',...
       'P/C in first cell','P/C in last cell',...
       