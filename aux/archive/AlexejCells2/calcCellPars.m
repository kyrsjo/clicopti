function [Q, vg, rQ, Es, dT] = calcCellPars(a, d, f, dphi)
%a - iris radius over lambda
%d - iris thickness over cell length
%f [GHz] - frequency
%dphi [degree] - rf phase advance per cell

% frequency scaling
  f0 = 29.985; %[GHz] reference frequency, all below parameter are for f0
  ff = f/f0;
  f2 = sqrt(ff);

% parameters at 29.985 GHz
  switch dphi
    case 120
      [Q, vg, rQ, Es, Hs] = calcCellPars_120(a, d);
    case 150
      [Q, vg, rQ, Es, Hs] = calcCellPars_150(a, d);
    otherwise 
       m = 3;
       n = length(a);
       Q_m = zeros(m,n);
      vg_m = zeros(m,n);
      rQ_m = zeros(m,n);
      Es_m = zeros(m,n);
      Hs_m = zeros(m,n);
      if dphi > 90
        dp_m = [90, 110, 130];
        [Q_m(1,:), vg_m(1,:), rQ_m(1,:), Es_m(1,:), Hs_m(1,:)] = calcCellPars_090(a, d);
        [Q_m(2,:), vg_m(2,:), rQ_m(2,:), Es_m(2,:), Hs_m(2,:)] = calcCellPars_110(a, d);
        [Q_m(3,:), vg_m(3,:), rQ_m(3,:), Es_m(3,:), Hs_m(3,:)] = calcCellPars_130(a, d);
      else
        dp_m = [50, 70, 90];
        [Q_m(1,:), vg_m(1,:), rQ_m(1,:), Es_m(1,:), Hs_m(1,:)] = calcCellPars_050(a, d);
        [Q_m(2,:), vg_m(2,:), rQ_m(2,:), Es_m(2,:), Hs_m(2,:)] = calcCellPars_070(a, d);
        [Q_m(3,:), vg_m(3,:), rQ_m(3,:), Es_m(3,:), Hs_m(3,:)] = calcCellPars_090(a, d);
      end
       Q = (interpQuad(Q_m', dp_m, dphi))';
      vg = (interpQuad(vg_m', dp_m, dphi))';
      rQ = (interpQuad(rQ_m', dp_m, dphi))';
      Es = (interpQuad(Es_m', dp_m, dphi))';
      Hs = (interpQuad(Hs_m', dp_m, dphi))';
  end
% dT for 150 MV/m, 30 GHz, 130ns rectangular pulse
  dT = 2.56.*(1.5.*Hs).^2;
%scaling parameters to f
  Q = Q./f2;
  rQ = rQ.*ff;
  dT = dT.*f2;
return     
  
function [Q, vg, rQ, Es, Hs] = calcCellPars_120(a, d)
d_n = [0.1,.25,0.4]; %d/h
% wds cell parameters for 150 degree per cell phase advance at 30 GHz
% Q-factor for 100%-Cu-condactivity
Qmn(1,:) = [4041, 3718, 3292];
Qmn(2,:) = [4081, 3764, 3322];
Qmn(3,:) = [4135, 3824, 3386];
Qmn(4,:) = [4232, 3912, 3468];
Qmn(5,:) = [4363, 4028, 3570]; 
% [%]
vgmn(1,:) = [0.32, 0.12, .055]; 
vgmn(2,:) = [1.59, 0.84, 0.61]; 
vgmn(3,:) = [4.15, 2.62, 1.76]; 
vgmn(4,:) = [7.90, 5.59, 4.11]; 
vgmn(5,:) = [12.5, 9.56, 7.49]; 
% [LinacOhm/m]
rQmn(1,:) = [50345, 45470, 38209]; 
rQmn(2,:) = [41848, 38456, 32903]; 
rQmn(3,:) = [33434, 31283, 27372]; 
rQmn(4,:) = [26140, 24880, 22250]; 
rQmn(5,:) = [20267, 19571, 17863]; 
% Emax/Eacc
Esmn(1,:) = [1.8, 1.7, 1.8]; 
Esmn(2,:) = [2.2, 1.9, 2.0]; 
Esmn(3,:) = [2.6, 2.0, 2.0]; 
Esmn(4,:) = [3.0, 2.2, 2.1]; 
Esmn(5,:) = [3.5, 2.6, 2.2]; 
% Hmax/Eacc [mA/V]
Hsmn(1,:) = [3.6, 4.1, 4.9]; 
Hsmn(2,:) = [3.8, 4.3, 5.0]; 
Hsmn(3,:) = [4.1, 4.6, 5.3]; 
Hsmn(4,:) = [4.4, 4.9, 5.5]; 
Hsmn(5,:) = [4.8, 5.3, 6.0]; 
%
%interpolation
Q = interpCellPars(a, d, d_n, Qmn);
vg = interpCellPars(a, d, d_n, vgmn);
rQ = interpCellPars(a, d, d_n, rQmn);
Es = interpCellPars(a, d, d_n, Esmn);
Hs = interpCellPars(a, d, d_n, Hsmn);
return;

function [Q, vg, rQ, Es, Hs] = calcCellPars_150(a, d)
d_n = [0.1,.25,0.4]; %d/h
% wds cell parameters for 150 degree per cell phase advance at 30 GHz
% Q-factor for 100%-Cu-condactivity
Qmn(1,:) = [4409, 4118, 3759];
Qmn(2,:) = [4452, 4175, 3808];
Qmn(3,:) = [4536, 4253, 3899];
Qmn(4,:) = [4662, 4372, 4018];
Qmn(5,:) = [4809, 4520, 4137]; 
% [%]
vgmn(1,:) = [0.15, .045, .018]; 
vgmn(2,:) = [0.80, 0.36, 0.19]; 
vgmn(3,:) = [2.24, 1.27, 0.80]; 
vgmn(4,:) = [4.53, 2.96, 2.09]; 
vgmn(5,:) = [7.59, 5.45, 4.06]; 
% [LinacOhm/m]
rQmn(1,:) = [41445, 39036, 34146]; 
rQmn(2,:) = [34558, 33098, 29574]; 
rQmn(3,:) = [27636, 26947, 24536]; 
rQmn(4,:) = [21679, 21370, 19906]; 
rQmn(5,:) = [16865, 16786, 15959]; 
% Emax/Eacc
Esmn(1,:) = [1.9, 1.75, 1.85]; 
Esmn(2,:) = [2.3, 2.0, 2.2]; 
Esmn(3,:) = [2.8, 2.1, 2.0]; 
Esmn(4,:) = [3.3, 2.3, 2.2]; 
Esmn(5,:) = [3.8, 2.6, 2.3]; 
% Hmax/Eacc [mA/V]
Hsmn(1,:) = [4.0, 4.4, 5.1]; 
Hsmn(2,:) = [4.3, 4.7, 5.4]; 
Hsmn(3,:) = [4.65,5.1, 5.7]; 
Hsmn(4,:) = [5.0, 5.4, 6.0]; 
Hsmn(5,:) = [5.4, 5.75, 6.3]; 
%
%interpolation
Q = interpCellPars(a, d, d_n, Qmn);
vg = interpCellPars(a, d, d_n, vgmn);
rQ = interpCellPars(a, d, d_n, rQmn);
Es = interpCellPars(a, d, d_n, Esmn);
Hs = interpCellPars(a, d, d_n, Hsmn);
return;

