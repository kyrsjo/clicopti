% plot parameters in x-band test structures
%a and d are radius and iris thickness) / lambda X 10

close all
clear all

global EAC_LOAD EAC_UNLOAD %[MV/m]

c = 299.792458; %speed of light [mm/ns]
E = 3e12*1.6e-19; %[V*C] beams energy 3TeV
L1 = 2.03e38; %[Hz/m^2] given peak Luminosity for 0.7Ne@50Hzand em20mk 

f = 11.424; %working frequency in GHz
Ea = 100; %[MV/m] averaged loaded accelerating gradient
dphi = 120; %[degree] phase advance per cell
%Ncycl0 = 20; %number of RF periods between the bunches
Wxmax0 = 10; %reference max value of wake at the second bunch [V/pC/mm/m] at 3TeV
%Wxmax0 = 20; %reference max value of wake at the second bunch [V/pC/mm/m] at 500GeV
Nemax0 = 4; %reference max number of electrons per bunch [10^9]
Eamax0 = 150; %reference max avereged accelerrating gradient [MV/m]
Nb0 = 154; %reference number of bunches in the train
dT_Cu2CuZr = sqrt(551/511); %coefficient to take CuZr instead of Cu
dT_Cu2Ti = sqrt(58/1.8); %coefficient to take Ti instead of Cu

% T26vg2.4 - prototype
if 0
name = 'T53vg3MC'
name = 'T53vg3MC_QDS'
f = 11.424; %[GHz]
Ea = 100; %[MV/m]
dphi = 120;
da= 0.204; %da/a
a = 0.1344*[1+da/2,1,1-da/2]; %a/lambda
h = c/f*dphi/360;%[mm]
d = 1.66/h*[1,1,1] %d/h
Nc = 30; Nb = 52; Ncycl = 8; %18Wu in _wds %half length T28_vg2.4
end

% CLIC_vg1 structure at 11.424 GHz
if 0
name = 'CLIC_vg1_11424MHz'
%name = 'CLIC_vg1_11424MHz_nds'
f = 11.424; %[GHz]
Ea = 100; %[MV/m]
dphi = 120;
a1 = 4.06;
a2 = 2.66;
av = (a1+a2)/2;
da = (a1-a2)/av; %da/<a>
al = av/(c/f);   %<a>/lambda
a = al*[1+da/2,1,1-da/2] %a/lambda
h = c/f*dphi/360;%[mm]
d1 = 2.794;
d2 = 1.314;
d = [d1,(d1+d2)/2,d2]/h; %d/h
Nc = 20; Nb = 262; Ncycl = 8; Ne=4%18 Wu
Nc = 18; Nb = 286; Ncycl = 8; Ne=0%18 Wu
end


format long

%initDatafromDaniel(0.1);
%Ne = calcBunchCharge(Ea, (a(1)+a(3))/2, da, 12)
%return;
%[Q1, f1, A1] = calcCellWake(a, d, f, dphi);
%Wx_max = Wxmax0*Nemax0/Ne*Ea/Eamax0;
%Ncyc = calcBunchSpacing(Nc, Q1, f1, A1, Wx_max, f)
%Ncycl = 6;
%return;

%[Q, vg, rQ, Es, dT] = calcCellPars(a, d, f, dphi)
%Sc = [0,0,0];
name = 'T18_vg2p6_disk'
%name = 'TD18_vg2p4_quad'
%name = 'TD18_vg2p4_disk'
%name = 'T24_vg1p8_disk'
%name = 'TD25_vg1p7_sdsk'
%name = 'TD24_vg1p7_disk'
%name = 'T18_vg2p6_half' %or quad but no slots
%name = 'T18_vg2p6_quad_slot' %quad but with slots
switch name
  case 'T18_vg2p6_disk'
  % T18_KEK tp=252ns, BDR=1e-6
%    Ea=101; 
  %
    a =4.06;
    Nc=18;
%    Nc=18+17;
%    Nc=18+17+17;
    Ne=0;%Ne=3.72;
    Nb=312; 
    Ncycl=8;
    Q =[6384, 6751, 7242]; 
    vg=[2.61, 1.70, 1.02];
    rQ=[11860,14612,17719]; 
    Es=[1.95, 1.9,  1.85];
    Hs=[3.2,  2.8,  2.4];%[mA/V]
    Sc=[0.47, 0.36, 0.28];%[mA/V]
  case 'TD18_vg2p4_quad'
    a =4.06;
    Nc=18;
    Ne=0;
    Nb=312; 
    Ncycl=8;
    Q =[5841, 6065, 6303]; 
    vg=[2.41, 1.57, 0.93];
    rQ=[10879,13432,16188]; 
    Es=[1.95, 1.85, 1.82];
    Hs=[5.0,  4.45, 4.0];%[mA/V]
    Sc=[0.51, 0.38, 0.29];%[mA/V]
  case 'TD18_vg2p4_disk'
  % TD18_KEK tp=252ns, BDR=1e-6
  %  Ea=86.6; 
  %
    a =4.06;
    Nc=18;
    Ne=0;
  %  Ne=3.72;
    Nb=312; 
    Ncycl=8;
    Q =[5100, 5400, 5500]; 
    vg=[2.23, 1.45, 0.87];
    rQ=[10200,12600,15100]; 
    Es=[2.0, 1.95, 1.95];
    Hs=[6.0, 5.6,  5.0];%[mA/V]
    Sc=[0.53, 0.4, 0.31];%[mA/V]
  case 'T24_vg1p8_disk'
    a =3.307;
    Nc=24;
    Ne=0;
    Nb=312; 
    Ncycl=6;
    Q =[6814, 6980, 7157]; 
    vg=[1.83, 1.33, 0.92];
    rQ=[15198,16960,18713]; 
    Es=[1.95, 1.95, 1.9];
    Hs=[2.6,  2.45, 2.3];%[mA/V]
    Sc=[0.37, 0.34, 0.28];%[mA/V]
  case 'TD25_vg1p7_sdsk'
    a =3.307;
    Nc=25+2;
    Nc=25;
    Ne=0;
    %Ne=3.72;
    Nb=312; 
    Ncycl=6;
    Q =[6198, 6305, 6407]; 
    vg=[1.66, 1.19, 0.83];
    rQ=[13832,15430,17003]; 
    Es=[1.95, 1.925, 1.9];
    Hs=[4.3,  4.05, 3.8];%[mA/V]
    Sc=[0.37, 0.34, 0.28];%[mA/V]
  case 'TD24_vg1p7_disk'
    a =3.307;
    Nc=24;
    Ne=0;
 %   Ne=3.72;
    Nb=312; 
    Ncycl=6;
    Q =[5782, 5857, 5968]; 
    vg=[1.61, 1.16, 0.81];
    rQ=[13536,15060,16612]; 
    Es=[1.95, 1.925, 1.9];
%    Hs=[4.5,  4.1, 3.9];%[mA/V]
    Hs=[4.7,  4.4, 4.2];%[mA/V]
    Sc=[0.4, 0.34, 0.29];%[mA/V]
  case 'T18_vg2p6_half'
    a =4.06;
    Nc=18;
    Ne=0;
    %Ne=3.72;
    Nb=312; 
    Ncycl=8;
    Q =[6763, 7216, 7639]; 
    vg=[2.60, 1.69, 1.02];
    rQ=[11845,14572,17685]; 
    Es=[1.95, 1.9,  1.85];
    Hs=[3.2,  2.8,  2.4];%[mA/V]
    Sc=[0.47, 0.36, 0.28];%[mA/V]
  case 'T18_vg2p6_quad_slot'
    a =4.06;
    Nc=18;
    Ne=0;
    Ne=3.72;
    Nb=312; 
    Ncycl=8;
    Q =[6662, 7131, 7562]; 
    vg=[2.61, 1.7, 1.02];
    rQ=[11835,14583,17702]; 
    Es=[2.15, 2.1,  2.05];
    Hs=[3.5,  3.1,  2.6];%[mA/V]
    Sc=[0.51, 0.4, 0.32];%[mA/V]
end    
dT = 2.56.*(1.5.*Hs).^2*sqrt(f/29.985)
%return

figure(1)
NcCoup=0; %Zero - gradient coupler length / cell length
[Eff, tp, Pin, tp_P] = plotStructPars(NcCoup, Nc, dphi, f, Ea, Q, vg, rQ, Es, Sc, dT, Ne, Ncycl, Nb)
Pin/2/pi/a(1)*tp_P^(1/3)
figure(1)
title(name)

nc=1:Nc;
% Parameters of the 1st, middle, and last cells from the data for
ncel=[1,fix((Nc+1)/2),Nc]; %cell numbers
hs = interpQuad(Hs, ncel, nc);
Hs_unload = hs.*EAC_UNLOAD(1:Nc); %[MV/m]


return

