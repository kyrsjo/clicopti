% testing hdsmn subroutines
%a and d are radius and iris thickness) / lambda X 10

close all
clear all

global EAC_LOAD EAC_UNLOAD %[MV/m]

c = 299.792458; %speed of light [mm/ns]
E = 3e12*1.6e-19; %[V*C] beams energy 3TeV
%L1 = 3.3e38; %[Hz/m^2] given peak Luminosity for 1.0Ne@?Hz 
%E = 0.5e12*1.6e-19; %[V*C] beams energy at 500 GeV
L1 = 2.03e38; %[Hz/m^2] given peak Luminosity for 0.7Ne@50Hzand em20mk 

f = 12; %working frequency in GHz
Ea = 100; %[MV/m] averaged loaded accelerating gradient
dphi = 120; %[degree] phase advance per cell
Ncycl = 6; %number of RF periods between the bunches

% 12 GHz test structures for Nc=26, Ncycl=6
% Based on the 'CLIC_G_r05_adw11_idw8p0'
a0 =[3.15,2.75,2.35];
Q0 =[5536,5635,5738]; 
vg0=[1.65,1.2,0.83];
rQ0=[14587,16220,17954]; 
Es0=[1.95,1.93,1.9];
Hs0=[4.1,3.85,3.6];
Sc0=[0.41,0.35,0.3];%Sc6[mA/V]


name = 'CLIC-G'
name = 'CLIC-M'
name = 'CLIC-N'
name = 'CLIC-O'
name = 'CLIC-P'
switch name
  case 'CLIC-G' % OR TD24_vg1p7_diskR05  OR TD26_vg1p7_R05_CC
%  da= 0.29; %da/a
%  a = 0.11*[1+da/2,1,1-da/2]; %a/lambda
  a_1st = 3.15;
  a_last = 2.35;
  a_mid = (a_1st+a_last)/2; a_mid/25
  a = [a_1st,a_mid,a_last]; %a [mm]
  d = [.2,.16,.12]; %d/h
  NcC = 0; Nc = 24; Nb = 312; Ncycl = 6; Ne = 0;
  NcC = 0; Nc = 26; Nb = 312; Ncycl = 6; Ne = 3.72;
%  NcC = 0; Nc = 26; Nb = 312; Ncycl = 6; Ne = 0;
%  NcC = 0; Nc = 26; Nb = 312; Ncycl = 6; Ne = -1;
  case 'CLIC-M' % constSc, TD26_vg1p9_R05
  a_1st = 3.41;
  a_last = 2.35;
  a_mid = (a_1st+a_last)/2; 
  a_mid/25
  a = [a_1st,a_mid,a_last]; %a [mm]
  NcC = 0; Nc = 26; Nb = 322; Ncycl = 6; Ne = 0;
%  NcC = 0; Nc = 26; Nb = 322; Ncycl = 6; Ne = 3.72;
%  NcC = 0; Nc = 26; Nb = 322; Ncycl = 6; Ne = -1;
  case 'CLIC-N' % constSc TD26_vg1p8_R05
  a_1st = 3.34;
  a_last = 2.24;
  a_mid = (a_1st+a_last)/2;
  a_mid/25
  a = [a_1st,a_mid,a_last]; %a [mm]
  NcC = 0; Nc = 26; Nb = 303; Ncycl = 6; Ne = 0;
%  NcC = 0; Nc = 26; Nb = 303; Ncycl = 6; Ne = 3.72;
%  NcC = 0; Nc = 26; Nb = 306; Ncycl = 6; Ne = -1;
  case 'CLIC-O' % constSc 
  a_1st = 3.6;
  a_last = 2.1;
  a_mid = (a_1st+a_last)/2
  a_mid/25
  a = [a_1st,a_mid,a_last]; %a [mm]
  NcC = 0; Nc = 26; Nb = 295; Ncycl = 6; Ne = 0;
%  NcC = 0; Nc = 26; Nb = 295; Ncycl = 6; Ne = 3.72;
%  NcC = 0; Nc = 26; Nb = 295; Ncycl = 6; Ne = -1;
  case 'CLIC-P' % constSc 
  a_1st = 4.04;
  a_last = 1.94;
  a_mid = (a_1st+a_last)/2
  a_mid/25
  a = [a_1st,a_mid,a_last]; %a [mm]
  NcC = 0; Nc = 26; Nb = 282; Ncycl = 6; Ne = 0;
%  NcC = 0; Nc = 26; Nb = 282; Ncycl = 6; Ne = 3.72;
%  NcC = 0; Nc = 26; Nb = 282; Ncycl = 6; Ne = -1;
end
initDatafromDaniel(0.1);
if Ne < 0
    Ne = calcBunchCharge(Ea, a_mid/25, (a(1)-a(3))/a_mid, 12)
end

format long


% switch name
%   case 'TD26_vg1p65' %TD24_vg1.8_diskR05, TD26_vg1.8_diskR05_CC
%     Q =Q0; 
%     vg=vg0;
%     rQ=rQ0; 
%     Es=Es0;
%     Hs=Hs0;
%     Sc=Sc0;%Sc6[mA/V]
%   case 'TD26_vg2p0_constSc' %TD26_constSc
    Q =[interpQuad(Q0,a0,a(1)),interpQuad(Q0,a0,a(2)),interpQuad(Q0,a0,a(3))] ;
    vg=[interpQuad(vg0,a0,a(1)),interpQuad(vg0,a0,a(2)),interpQuad(vg0,a0,a(3))]
    rQ=[interpQuad(rQ0,a0,a(1)),interpQuad(rQ0,a0,a(2)),interpQuad(rQ0,a0,a(3))];
    Es=[interpQuad(Es0,a0,a(1)),interpQuad(Es0,a0,a(2)),interpQuad(Es0,a0,a(3))];
    Hs=[interpQuad(Hs0,a0,a(1)),interpQuad(Hs0,a0,a(2)),interpQuad(Hs0,a0,a(3))];
    Sc=[interpQuad(Sc0,a0,a(1)),interpQuad(Sc0,a0,a(2)),interpQuad(Sc0,a0,a(3))];%Sc6[mA/V]
% end 
if Hs > 0
    dT = 2.56.*(1.5.*Hs).^2*sqrt(f/29.985)
end
    %Q = Q*sqrt(0.95);
%return

figure(3)
[Eff, tp, Pin, tp_P, Scmax, dTmax] = plotStructPars(NcC, Nc, dphi, f, Ea, Q, vg, rQ, Es, Sc, dT, Ne, Ncycl, Nb)
PCt = Pin/2/pi/(a(1)*c/f)*tp_P^(1/3)
Sct = Scmax*tp_P^(1/3)
dTmax

L1bx = calcBunchLuminocity(Ea, a_mid/25, (a(1)-a(3))/a_mid, f)
L1bx/Ne*Eff
return

if 0
[L1bx, Ltbx, sz] = calcBunchLuminocity(Ea, a(2), da, f)
f_m = f;
dphi_m = dphi;
av_m = a(2);
Lum_f = L1bx;
Eff_f = Eff;
Pin_f = Pin;
tp_f = tp;
Ne_f = Ne;
Nb_f = Nb;
Nc_f = Ncycl;
Nl_f = Nc;
Ci_f = 0;
Ce_f = 0;
d1_f = d(1);
save(sprintf('data500GeV_norm\\CLIC_G_Ea%d_tp483ns',Ea),...
  'f_m', 'dphi_m', 'av_m', 'Lum_f', 'Eff_f', 'Pin_f', 'tp_f',...
  'Ne_f', 'Nb_f', 'Nc_f', 'Nl_f', 'Ci_f', 'Ce_f', 'd1_f')
return
end

tr = 100./(f*0.47*(min(vg))^1.25) %[ns]

disp('a');disp(a*c/f)
disp('d');disp(d*c/f*dphi/360)
disp('Ls');disp((Nc+180/dphi)*c/f*dphi/360)
[L1bx, Ltbx, sz] = calcBunchLuminocity(Ea, a(2), da, f)
%L1bx = 2.5; Ltbx = 0;; sz = 0;
%L1bx = 1.3; Ltbx = 0;; sz = 0;
% repetition rate for a given peak luminosity L1
fr = L1./(Nb.*L1bx*1e34); %[Hz]
% total luminosity at frep
Lt = fr*(Nb.*Ltbx*1e34); %[Hz/m^2]
%Beam power/beam
Pb = E/2*(Ne*1e9).*Nb.*fr/1000000; %[MW]
%Input power/linac
Pl = Pb./(Eff/100); %[MW]
%Pulse energy/linac
Ep = 1000*Pl/fr; %[kJ]
h = c/f*dphi/360;%[mm]
Ls = (Nc+180/dphi)*h; %[mm]
Ci=0
Ce=0
%Ci = calcCost(Ea,f,dphi,Ls,tp,L1bx*Eff/Ne,fr,2*Ep); %[a.u.]
%Ce = calcElectricityCost(L1bx*Eff/Ne); %[a.u.]
disp(sprintf(' Ea = %d [MV/m], f = %d [GHz], dphi = %d [deg], <a>/lambda = %6.4f',Ea,f,dphi,a(2)))
disp(sprintf(' frep = %g [Hz], Ltotal = %g [Hz/m^2], sigma_z = %g [um]',fr,Lt,sz))
disp(sprintf(' Pbeam = %g [MW/beam], Pin = %g [MW/linac], Ep = %g [kJ/linac]',Pb,Pl,Ep))
disp(sprintf(' Inv. Cost = %g [a.u.], 10-years El. Cost = %g [a.u.]',Ci,Ce))       
return


       