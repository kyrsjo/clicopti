function [Ci Ce, Cia]=calcCost(G,freq,dphi,Lstruct,Tpulse,FOM,frep,Epuls)
%function [Ci Ce]=calcCost(G,freq,Lstruct,Tpulse,FOM,frep,Epuls)
% parameters: G[MV/m], freq[GHz], Lstruct[mm], Tpulse[ns], FOM[a.u.],
% frep[Hz], Epuls[kJ], dphi[degree]
%  [Ci, Ce] = ComputeCost_10_08_2006(G,freq,Lstruct,Tpulse,FOM,frep,Epuls);
  [Ci, Ce, Cia] = ComputeCost(G,freq,Lstruct,Tpulse,FOM,frep,Epuls,dphi);
%  [Ci, Ce, Cia] = ComputeCostFix20070531(G,freq,Lstruct,Tpulse,FOM,frep,Epuls,dphi);
  Ci = Ci/12e9; %investment cost in a.u.
  Ce = Ce/12e9; %10 years electricity cost in a.u.
  Cia = Cia/12e9; %array of partial costs in a.u.: Ci=sum(Cia)
return;