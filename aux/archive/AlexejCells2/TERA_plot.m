% testing hdsmn subroutines
%a and d are radius and iris thickness) / lambda X 10

close all
clear all

c = 299.792458; %speed of light [mm/ns]
E = 3e12*1.6e-19; %[V*C] beams energy 3TeV
%L1 = 3.3e38; %[Hz/m^2] given peak Luminosity for 1.0Ne@?Hz 
%E = 0.5e12*1.6e-19; %[V*C] beams energy at 500 GeV
L1 = 2.03e38; %[Hz/m^2] given peak Luminosity for 0.7Ne@50Hzand em20mk 

f = 12; %working frequency in GHz
Ea = 30; %[MV/m] averaged loaded accelerating gradient
dphi = 120; %[degree] phase advance per cell
%Ncycl0 = 20; %number of RF periods between the bunches
Wxmax0 = 10; %reference max value of wake at the second bunch [V/pC/mm/m] at 3TeV
%Wxmax0 = 20; %reference max value of wake at the second bunch [V/pC/mm/m] at 500GeV
Nemax0 = 4; %reference max number of electrons per bunch [10^9]
Eamax0 = 150; %reference max avereged accelerrating gradient [MV/m]
Nb0 = 154; %reference number of bunches in the train
dT_Cu2CuZr = sqrt(551/511); %coefficient to take CuZr instead of Cu
dT_Cu2Ti = sqrt(58/1.8); %coefficient to take Ti instead of Cu

%%%% 120 degree beta=0.5 structure with nose cones for TERA
rc_ = [1.5, 2, 2.5, 2.9];
Q_  = [3624,3455,3261,2860];
rQ_ = [21057,22288,25334,32403];
vg_ = [0.57,1.52,2.77,4.72];
Es_ = [3.05,3.1,3.1,3.5];
Hs_ = [4.3,5.3,5.6,6.4];
Sc_ = [0.35,0.58,0.82,1.4];

rc1 = 2.78;
rc2 = 1.48;
rc = [rc1,(rc1+rc2)/2,rc2];
Q  = spline(rc_,Q_,rc);
rQ  = spline(rc_,rQ_,rc);
vg  = spline(rc_,vg_,rc)
Es  = spline(rc_,Es_,rc);
Hs  = spline(rc_,Hs_,rc);
Sc  = spline(rc_,Sc_,rc);
dT = 2.56.*(1.5.*Hs).^2*sqrt(f/29.985)
Nc = 99; Ls = 25*Nc/6
dphi = 60;
Ne = 0;
Nb = 1;
Ncycl = 1;

figure(1)
[Eff, tp, Pin, tp_P] = plotStructPars(0, Nc, dphi, f, Ea, Q, vg, rQ, Es, Sc, dT, Ne, Ncycl, Nb)
return

[L1bx, Ltbx, sz] = calcBunchLuminocity(Ea, a(2), da, f)
f_m = f;
dphi_m = dphi;
av_m = a(2);
Lum_f = L1bx;
Eff_f = Eff;
Pin_f = Pin;
tp_f = tp;
Ne_f = Ne;
Nb_f = Nb;
Nc_f = Ncycl;
Nl_f = Nc;
Ci_f = 0;
Ce_f = 0;
d1_f = d(1);
save(sprintf('data500GeV_norm\\CLIC_G_Ea%d_tp483ns',Ea),...
  'f_m', 'dphi_m', 'av_m', 'Lum_f', 'Eff_f', 'Pin_f', 'tp_f',...
  'Ne_f', 'Nb_f', 'Nc_f', 'Nl_f', 'Ci_f', 'Ce_f', 'd1_f')

%return

tr = 100./(f*0.47*(min(vg))^1.25) %[ns]

disp('a');disp(a*c/f)
disp('d');disp(d*c/f*dphi/360)
disp('Ls');disp((Nc+180/dphi)*c/f*dphi/360)
[L1bx, Ltbx, sz] = calcBunchLuminocity(Ea, a(2), da, f)
%L1bx = 2.5; Ltbx = 0;; sz = 0;
%L1bx = 1.3; Ltbx = 0;; sz = 0;
% repetition rate for a given peak luminosity L1
fr = L1./(Nb.*L1bx*1e34); %[Hz]
% total luminosity at frep
Lt = fr*(Nb.*Ltbx*1e34); %[Hz/m^2]
%Beam power/beam
Pb = E/2*(Ne*1e9).*Nb.*fr/1000000; %[MW]
%Input power/linac
Pl = Pb./(Eff/100); %[MW]
%Pulse energy/linac
Ep = 1000*Pl/fr; %[kJ]
h = c/f*dphi/360;%[mm]
Ls = (Nc+180/dphi)*h; %[mm]
Ci=0
Ce=0
%Ci = calcCost(Ea,f,dphi,Ls,tp,L1bx*Eff/Ne,fr,2*Ep); %[a.u.]
%Ce = calcElectricityCost(L1bx*Eff/Ne); %[a.u.]
disp(sprintf(' Ea = %d [MV/m], f = %d [GHz], dphi = %d [deg], <a>/lambda = %6.4f',Ea,f,dphi,a(2)))
disp(sprintf(' frep = %g [Hz], Ltotal = %g [Hz/m^2], sigma_z = %g [um]',fr,Lt,sz))
disp(sprintf(' Pbeam = %g [MW/beam], Pin = %g [MW/linac], Ep = %g [kJ/linac]',Pb,Pl,Ep))
disp(sprintf(' Inv. Cost = %g [a.u.], 10-years El. Cost = %g [a.u.]',Ci,Ce))       
return

nc = 10:2:60
[tf,eff,es,dt,pio] = calcStructPars(nc, dphi, f, Ea, Q, vg, rQ, Es, dT, Ne, Ncycl, Nb);
tb0 = Ncycl*(Nb-1)/f;  %[ns] - assumed beam time
dp = pio(3,:)./pio(2,:);
dpP = 0.1;
tr_ = tr.*dpP./dp;
tf_ = tf.*dpP./(1-dp).*(dp<(1-dpP))+tf.*(dp>=(1-dpP));
%tb_ = (60./pio(1,:)).^3*400 - 0.2*(tr+tf); %[ns]- allowed beam time
tb_ = (60./pio(1,:)).^3*400 - tr_-tf_; %[ns]- allowed beam time
tp_ = tr+tf+tb_;
eff = eff.*(tf+tb0)./(tr+tf+tb0);
eff0= eff.*(tr+tf+tb0)./tb0;
eff_= eff0.*tb_./tp_;
%cost calculation
Nb_ = fix(tb_/(Ncycl/f))
%[L1bx, Ltbx, sz] = calcBunchLuminocity(Ea, a(2), da, f);
L1bx = 1.3; Ltbx = 2.5;; sz = 2.5;
fr_ = L1./(Nb_.*L1bx*1e34); %[Hz]
Lt_ = fr*(Nb_.*Ltbx*1e34); %[Hz/m^2]
Pb_ = E/2*(Ne*1e9).*Nb_.*fr_/1000000; %[MW/beam]
Pl_ = Pb_./(eff_/100); %[MW/linac]
Ep_ = 1000*2*Pl_./fr_; %[kJ]
for i = 1:length(nc)
  Ci(i) = calcCost(Ea,f,dphi,(nc(i)+180/dphi)*h,tp_(i),...
                   L1bx*eff_(i)/Ne,fr_(i),Ep_(i)); %[a.u.]
end
Ce = calcElectricityCost(L1bx*eff_/Ne); %[a.u.]

%return

figure(3)
%a*c/f
subplot(4,1,1),plot(nc,es(1,:),'b-',nc,es(2,:),'r-','LineWidth',1),grid,%axis([30,100,250,300])
title(sprintf('d=[%5.3f,%5.3f]mm, a=[%5.3f,%5.3f]mm, E_{acc}^{av}=150MV/m, N_b = %d',d,a,Nb))
ylabel('E_{surf} [MV/m]')
subplot(4,1,2),plot(nc,dt(1,:),'b-',nc,dt(2,:),'r-','LineWidth',1),grid,%axis([30,100,50,60])
ylabel('\DeltaT [K]')
subplot(4,1,3),plot(nc, pio(1,:)/(a(1)*c/f), 'b-', nc, pio(2,:)/(a(3)*c/f), 'r-','LineWidth',1),grid,%axis([30,100,0,15])
ylabel('P/a [MW/mm]')
subplot(4,1,4),plot(nc,eff,'k--',nc,eff_,'k-',nc,tf,'r-','LineWidth',1),grid,%axis([30,100,0,15])
text(100,12,sprintf('N_e=%4.2f*10^9',Ne)) 
ylabel('\eta[%], t_f[ns]')
xlabel('ncells')

figure(4)
ls = (nc+180/dphi)*h/10;
%plot(nc,es(1,:)/10,'g-',nc,es(2,:)/10,'g--','LineWidth',1)
%hold on
%plot(nc,dt(1,:),'b-',nc,dt(2,:),'b--','LineWidth',1)
%plot(nc,pio(1,:)/(a(1)*c/f), 'k-', nc, pio(2,:)/(a(3)*c/f), 'k--','LineWidth',1)
plot(ls,eff0,'r-.',ls,eff_,'r-','LineWidth',3)
hold on
plot(ls,10*Ci,'b-','LineWidth',3)
hold off
grid
axis([10,55,0,35])
%ylabel('E_{surf} [10MV/m], \DeltaT [K], \eta [%]')
ylabel('\eta [%], C_i [a.u.]')
xlabel('L_s [cm]')
legend('\eta steady-state', '\eta fixed Pt_p^1^/^3','10xC_i',2)
return;

'E_{surf} in first cell','E_{surf} in last cell',...
       '\DeltaT in first cell','\DeltaT in last cell',...
       'P/C in first cell','P/C in last cell',...
       