function y = initDatafromDaniel500GeV(eps,Ea)
%eps - 'high', 'low', or 'medium'
%Ea[MV/m] - Average loaded gradient

global FREQUENCY A_OVER_LAMBDA DA_OVER_LAMBDA BUNCH_CHARGE BUNCH_LENGTH LUMI_TOTAL LUMI_PEAK

FREQUENCY = 12; %[GHz]
A_OVER_LAMBDA = 0.07:0.01:0.22; % aol
DA_OVER_LAMBDA = 0:0.3:0.6; % aol_1-aol_2
nf = length(FREQUENCY);
na = length(A_OVER_LAMBDA);
nd = length(DA_OVER_LAMBDA);
BUNCH_CHARGE = zeros(nd,na,nf); %[10^9] - bunch population
BUNCH_LENGTH = zeros(nd,na,nf); %[mkm] - bunch population
LUMI_TOTAL = zeros(nd,na,nf);   %[1e34/m^2/bx] - total luminosity
LUMI_PEAK = zeros(nd,na,nf);   %[1e34/m^2/bx] - peak luminosity

Ea0=fix(Ea/10)*10;

filename = sprintf('result_%s.250.%d.txt',eps,Ea0)
file = fopen(filename,'r')
data = fscanf(file, '%g', [14, nd*na*nf]);
fclose(file);
for jf = 1:nf
  for ja = 1:na
    index = (jf-1)*na*nd+(ja-1)*nd+[1:nd]; 
    BUNCH_CHARGE(:,ja,jf) = data(3,index); 
    BUNCH_LENGTH(:,ja,jf) = data(4,index); 
    LUMI_TOTAL(:,ja,jf) = 1e-34*data(8,index); 
    LUMI_PEAK(:,ja,jf) = 1e-34*data(9,index);
  end
end

if Ea > Ea0
  w = (Ea-Ea0)/10;
  w1 = 1-w;
  filename = sprintf('result_%s.250.%d.txt',eps,Ea0+10)
  file = fopen(filename,'r')
  data = fscanf(file, '%g', [14, nd*na*nf]);
  fclose(file);
  for jf = 1:nf
    for ja = 1:na
      index = (jf-1)*na*nd+(ja-1)*nd+[1:nd]; 
      BUNCH_CHARGE(:,ja,jf) = BUNCH_CHARGE(:,ja,jf)*w1 + data(3,index)'*w; 
      BUNCH_LENGTH(:,ja,jf) = BUNCH_LENGTH(:,ja,jf)*w1 + data(4,index)'*w; 
      LUMI_TOTAL(:,ja,jf) = LUMI_TOTAL(:,ja,jf)*w1 + 1e-34*data(8,index)'*w; 
      LUMI_PEAK(:,ja,jf) = LUMI_PEAK(:,ja,jf)*w1 + 1e-34*data(9,index)'*w;
    end
  end
end

return;

a = A_OVER_LAMBDA;
N = BUNCH_CHARGE;
L = LUMI_PEAK;
S = BUNCH_LENGTH;
for jd = 1
  plot(a,L(jd,:,jf)./N(jd,:,jf),'r-o'); hold on
end
return;
