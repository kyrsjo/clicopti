function y = hdsmn_LNE_Ea 

clear all
close all

c = 299.792458; %speed of light [mm/ns]
E = 3.0e12*1.6e-19; %[V*C] beams energy 3TeV
E = 0.5e12*1.6e-19; %[V*C] beams energy 0.5TeV
L1 = 2.0e38; %[Hz/m^2] given Luminosity 

Ea = 100; %MV/m avereged loaded accelerating gradient
dphi = 120; %[degree] phase advance per cell
dav = 0.01;%step for <a>/lambda
dda = 0.05;%step for (a1-a2)/<a>
dd = 0.02; %step for d/h

dav = 0.005;%step for <a>/lambda
dda = 0.01;%step for (a1-a2)/<a>

CostFlag = 0; %0 - Luminosity/power; 1 - total cost optimum

% pulse compression parameters from Igor
tp_klys = 1600; %klystron pulse length [ns]
P_klys = 75; %klystron output power [MW]
N_pc = [1:7,8:14];
tp_pc = tp_klys./N_pc;
Eff_pc = [[1,.85,0.823,0.8,0.756,0.702,0.651],0.651-cumsum(0.054./[1:7].^(1/3))];
Ntp_pc_ = 100;
tp_pc_ = min(tp_pc):(max(tp_pc)-min(tp_pc))/Ntp_pc_:max(tp_pc);
N_pc_ = tp_klys./tp_pc_;
Eff_pc_ = spline(tp_pc,Eff_pc,tp_pc_);
figure(100)
subplot(2,1,1)
plot(N_pc,Eff_pc,'bo',N_pc_,Eff_pc_,'b-'),grid on;
xlabel('compression factor')
ylabel('compression efficiency')
subplot(2,1,2)
plot(N_pc,N_pc.*Eff_pc,'r*',N_pc_,N_pc_.*Eff_pc_,'r-'),grid on;
xlabel('compression factor')
ylabel('power gain')
figure(101)
subplot(2,1,1)
plot(tp_pc,Eff_pc,'bo',tp_pc_,Eff_pc_,'b-'),grid on;
xlabel('t_p [ns]')
ylabel('compression efficiency')
subplot(2,1,2)
plot(tp_pc,N_pc.*Eff_pc,'r*',tp_pc_,N_pc_.*Eff_pc_,'r-'),grid on;
xlabel('t_p [ns]')
ylabel('power gain')
figure(102)
plot(tp_pc,N_pc,'r*',tp_pc_,N_pc_,'r-')
%return

%names
% 18Wu@X-band


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for dphi=[120,150]
for ieps = 1:6
if 0
Ea_m = 50:10:100;
rf_name='P3rt_Pin83000kW_tp150ns_dT56K_vgmin5pm_tr1_Cost0';
switch ieps
  case 1
    eps='high'
    linecolor = 'r';
  case 2
    eps='medium'
    linecolor = 'b';
  case 3
    eps='low'
    linecolor = 'k';
  case 4
    eps='high_full'
    linecolor = 'm';
    Ea_m = 50:10:70;
end
dir_name = sprintf('data500GeV_%s\\',eps);
end
if 0
eps='high'
Ea_m = [50,57,67,80,100];
switch ieps
  case 1
    Ea_m = [50,60,70,80,90,100];
    dir_name = sprintf('data500GeV_%s\\',eps);
    rf_name='P3rt_Pin83000kW_tp150ns_dT56K_vgmin5pm_tr1_Cost0';
    linecolor = 'r';
  case 2
    Ea_m = [50,60,70,80,90,100];
    dir_name = sprintf('data500GeV_%s\\',eps);
    rf_name='P3rt_Pin83000kW_tp150ns_dT40K_vgmin5pm_tr1_Cost0';
    linecolor = 'c';
  case 3
    dir_name = sprintf('data500GeV_%s\\Nc56_',eps);
    rf_name='P3rt_Pin83000kW_tp150ns_dT40K_vgmin5pm_tr1_Cost0';
    linecolor = 'b';
  case 4
    dir_name = sprintf('data500GeV_%s\\Nc56_Ns6_',eps);
    rf_name='P3rt_Pin83000kW_tp150ns_dT40K_vgmin5pm_tr1_Cost0';
    linecolor = 'k';
  case 5
    dir_name = sprintf('data500GeV_%s\\Nc56_Ns6_',eps);
    rf_name='P3rt_Pin83000kW_dT40K_tp483ns_vgmin5pm_tr1_Cost0';
    linecolor = 'm';
end
end
if 0
eps='high'
name = 'Nc56_Wt20_'
Ea_m = [50,57,67,80,100];
switch ieps
  case 1
    Ea_m = [50,60,70,80,90,100];
    dir_name = sprintf('data500GeV_%s\\',eps);
    rf_name='P3rt_Pin83000kW_tp150ns_dT56K_vgmin5pm_tr1_Cost0';
    linecolor = 'r';
  case 2
    dir_name = sprintf('data500GeV_%s\\%s',eps,name);
    rf_name='P3rt_Pin83000kW_tp150ns_dT56K_vgmin5pm_tr1_Cost0';
    linecolor = 'b';
  case 3
    dir_name = sprintf('data500GeV_%s\\%sNs6_',eps,name);
    rf_name='P3rt_Pin83000kW_dT56K_tp242ns_vgmin5pm_tr1_Cost0';
    linecolor = 'k';
  case 4
    dir_name = sprintf('data500GeV_%s\\%sNs6_',eps,name);
    rf_name='P3rt_Pin83000kW_dT56K_tp483ns_vgmin5pm_tr1_Cost0';
    linecolor = 'm';
end
end
if 1 % different struture and pulse length for nominal beam parameters
eps='norm'
Ea_m = [50,57,67,80,100];
switch ieps
  case 1
    name = '';
    linecolor = 'r-o';
    if dphi==150
      linecolor = 'r--^';
    end   
    Ea_m = [50,60,70,80,90,100];
    dir_name = sprintf('data500GeV_%s\\',eps);
    rf_name='P3rt_Pin83000kW_dT56K_vgmin5pm_tr1_Cost0';
  case 2
    name = 'Nc26_';
    linecolor = 'b-o';
    if dphi==150
      name = 'Nc21_';
      linecolor = 'b--^';
    end   
    dir_name = sprintf('data500GeV_%s\\%sNs6_',eps,name);
    rf_name='P3rt_Pin83000kW_dT56K_tp242ns_vgmin5pm_tr1_Cost0';
  case 3
    name = 'Nc56_';
    linecolor = 'k-o';
    if dphi==150
      name = 'Nc45_';
      linecolor = 'k--^';
    end   
    dir_name = sprintf('data500GeV_%s\\%sNs6_',eps,name);
    rf_name='P3rt_Pin83000kW_dT56K_tp242ns_vgmin5pm_tr1_Cost0';
  case 4
    name = 'Nc56_';
    linecolor = 'm-o';
    if dphi==150
      name = 'Nc45_';
      linecolor = 'm--^';
    end   
    dir_name = sprintf('data500GeV_%s\\%sNs6_',eps,name);
    rf_name='P3rt_Pin83000kW_dT56K_tp483ns_vgmin5pm_tr1_Cost0';
  case 5
    name = 'CLIC_G';
    linecolor = 'c-o';
    dir_name = sprintf('data500GeV_%s\\%s_',eps,name);
    rf_name='';
  case 6
    Ea_m = [50,57,67,80];
    name = 'CLIC_G';
    linecolor = 'g-o';
    dir_name = sprintf('data500GeV_%s\\%s_',eps,name);
    rf_name='_tp483ns';
end
end
if 0 % nominal struture length and nominal beam parameters (more pulse lengthes)
eps='norm'
Ea_m = [50,57,67,80,100];
switch ieps
  case 1
    name = '';
    linecolor = 'r-o';
    if dphi==150
      linecolor = 'r--^';
    end   
    Ea_m = [50,60,70,80,90,100];
    dir_name = sprintf('data500GeV_%s\\',eps);
    rf_name='P3rt_Pin83000kW_dT56K_vgmin5pm_tr1_Cost0';
  case 2
    name = 'Nc26_';
    linecolor = 'b-o';
    if dphi==150
      name = 'Nc21_';
      linecolor = 'b--^';
    end   
    dir_name = sprintf('data500GeV_%s\\%sNs6_',eps,name);
    rf_name='P3rt_Pin83000kW_dT56K_tp242ns_vgmin5pm_tr1_Cost0';
  case 3
    name = 'Nc26_';
    linecolor = 'k-o';
    if dphi==150
      name = 'Nc21_';
      linecolor = 'k--^';
    end   
    dir_name = sprintf('data500GeV_%s\\%sNs6_',eps,name);
    rf_name='P3rt_Pin83000kW_dT56K_tp323ns_vgmin5pm_tr1_Cost0';
  case 4
    name = 'Nc26_';
    linecolor = 'c-o';
    if dphi==150
      name = 'Nc21_';
      linecolor = 'c--^';
    end   
    dir_name = sprintf('data500GeV_%s\\%sNs6_',eps,name);
    rf_name='P3rt_Pin83000kW_dT56K_tp363ns_vgmin5pm_tr1_Cost0';
  case 5
    name = 'Nc26_';
    linecolor = 'm-o';
    if dphi==150
      name = 'Nc21_';
      linecolor = 'm--^';
    end   
    dir_name = sprintf('data500GeV_%s\\%sNs6_',eps,name);
    rf_name='P3rt_Pin83000kW_dT56K_tp483ns_vgmin5pm_tr1_Cost0';
end
end
if 0 % doubled struture length and nominal beam parameters (more pulse lengthes)
eps='norm'
Ea_m = [50,57,67,80,100];
switch ieps
  case 1
    name = '';
    linecolor = 'r-o';
    if dphi==150
      linecolor = 'r--^';
    end   
    Ea_m = [50,60,70,80,90,100];
    dir_name = sprintf('data500GeV_%s\\',eps);
    rf_name='P3rt_Pin83000kW_dT56K_vgmin5pm_tr1_Cost0';
  case 2
    name = 'Nc56_';
    linecolor = 'b-o';
    if dphi==150
      name = 'Nc45_';
      linecolor = 'b--^';
    end   
    dir_name = sprintf('data500GeV_%s\\%sNs6_',eps,name);
    rf_name='P3rt_Pin83000kW_dT56K_tp242ns_vgmin5pm_tr1_Cost0';
  case 3
    name = 'Nc56_';
    linecolor = 'k-o';
    if dphi==150
      name = 'Nc45_';
      linecolor = 'k--^';
    end   
    dir_name = sprintf('data500GeV_%s\\%sNs6_',eps,name);
    rf_name='P3rt_Pin83000kW_dT56K_tp323ns_vgmin5pm_tr1_Cost0';
  case 4
    name = 'Nc56_';
    linecolor = 'c-o';
    if dphi==150
      name = 'Nc45_';
      linecolor = 'c--^';
    end   
    dir_name = sprintf('data500GeV_%s\\%sNs6_',eps,name);
    rf_name='P3rt_Pin83000kW_dT56K_tp363ns_vgmin5pm_tr1_Cost0';
  case 5
    name = 'Nc56_';
    linecolor = 'm-o';
    if dphi==150
      name = 'Nc45_';
      linecolor = 'm--^';
    end   
    dir_name = sprintf('data500GeV_%s\\%sNs6_',eps,name);
    rf_name='P3rt_Pin83000kW_dT56K_tp483ns_vgmin5pm_tr1_Cost0';
end
end
if 0 %different disraption parameter
name = '';
Ea_m = [50,60,70,80,90,100];
switch ieps
  case 1
    eps='norm'
    linecolor = 'r-o';
    rf_name='P3rt_Pin83000kW_dT56K_vgmin5pm_tr1_Cost0';
  case 2
    eps='norm_10'
    linecolor = 'r-.o';
    rf_name='P3rt_Pin83000kW_tp150ns_dT56K_vgmin5pm_tr1_Cost0';
end
dir_name = sprintf('data500GeV_%s\\',eps);
end

nem = length(Ea_m);
for i = 1:nem 
%  Ea = Ea_m(i);
%  initDatafromDaniel500GeV(eps,Ea);
  if length(name) == length('CLIC_G')
    load(sprintf('%sEa%d%s',dir_name,Ea_m(i),rf_name));
  else
    load(sprintf('%sE%df12dphi%ddd%ddav%ddda%d_%s',...
      dir_name,Ea_m(i),dphi,10000*dd,10000*dav,1000*dda,rf_name));
  end
  % 'f_m', 'dphi_m', 'av_m', 'Lum_f', 'Eff_f', 'Pin_f', 'tp_f', 'Ne_f',
  % 'Nb_f', 'Nc_f', 'Nl_f', 'Ci_f', 'Ce_f'
  Lum_m(i,:,:,:) = Lum_f; 
  Eff_m(i,:,:,:) = Eff_f; 
  Pin_m(i,:,:,:) = Pin_f; 
  tp_m(i,:,:,:) = tp_f; 
  Ne_m(i,:,:,:) = Ne_f; 
  Nb_m(i,:,:,:) = Nb_f; 
  Nc_m(i,:,:,:) = Nc_f; 
  Nl_m(i,:,:,:) = Nl_f; 
  Ci_m(i,:,:,:) = Ci_f; 
  Ce_m(i,:,:,:) = Ce_f; 
end
nfm = length(f_m);
npm = length(dphi_m);
nam = length(av_m);

% Figure of merite 
LNE = Eff_m./Ne_m.*Lum_m; %[100/10^9*10^34 1/m^2/bx]
% repetition rate for a given luminosity L1
fr = L1./(Nb_m.*Lum_m*1e34); %[Hz]
%Beam power/beam
Pb = E/2*(Ne_m*1e9).*Nb_m.*fr/1000000; %[MW]
%Input power/linac
Pl = Pb./(Eff_m/100); %[MW]

%Total Cost
Ct_m = Ci_m+Ce_m;

if CostFlag == 0
  [mLNE4, iLNE4] = max(LNE,[],4); %best av
  [mLNE3, iLNE3] = max(mLNE4,[],3); %best phase advance
  [mLNE2, iLNE2] = max(mLNE3,[],2); %best frequency
  [mLNE1, iLNE1] = max(mLNE2,[],1); %best gradient
else
  [mLNE4, iLNE4] = min(Ct_m,[],4); %best av
  [mLNE3, iLNE3] = min(mLNE4,[],3); %best phase advance
  [mLNE2, iLNE2] = min(mLNE3,[],2); %best frequency
  [mLNE1, iLNE1] = min(mLNE2,[],1); %best gradient
end

%disp(sprintf('optimum: Ea = %d MV/m, f= %d GHz, dphi = %d deg, <a>/lambda = %5.4f',...
%    Ea_m(iLNE1), ...
%    f_m(iLNE2(iLNE1)), ...
%    dphi_m(iLNE3(iLNE1,iLNE2(iLNE1))),...
%    av_m(iLNE4(iLNE1,iLNE2(iLNE1),iLNE3(iLNE1,iLNE2(iLNE1))))))

mL_Lum = zeros(nem,nfm);
mL_Eff = zeros(nem,nfm);
mL_Pin = zeros(nem,nfm);
mL_tp = zeros(nem,nfm);
mL_Nc = zeros(nem,nfm);
mL_Nl = zeros(nem,nfm);
mL_Ne = zeros(nem,nfm);
mL_av = zeros(nem,nfm);
mL_dp = zeros(nem,nfm);
mL_fr = zeros(nem,nfm);
mL_Pb = zeros(nem,nfm);
mL_Pl = zeros(nem,nfm);
mL_Ci = zeros(nem,nfm);
mL_Ce = zeros(nem,nfm);
for i=1:nem
  for j=1:nfm
    mL_Lum(i,j) = Lum_m(i,j,iLNE3(i,j),iLNE4(i,j,iLNE3(i,j)));
    mL_Eff(i,j) = Eff_m(i,j,iLNE3(i,j),iLNE4(i,j,iLNE3(i,j)));
    mL_Pin(i,j) = Pin_m(i,j,iLNE3(i,j),iLNE4(i,j,iLNE3(i,j)));
    mL_tp(i,j) = tp_m(i,j,iLNE3(i,j),iLNE4(i,j,iLNE3(i,j)));
    mL_Nc(i,j) = Nc_m(i,j,iLNE3(i,j),iLNE4(i,j,iLNE3(i,j)));
    mL_Nl(i,j) = Nl_m(i,j,iLNE3(i,j),iLNE4(i,j,iLNE3(i,j)));
    mL_Ne(i,j) = Ne_m(i,j,iLNE3(i,j),iLNE4(i,j,iLNE3(i,j)));
    mL_fr(i,j) = fr(i,j,iLNE3(i,j),iLNE4(i,j,iLNE3(i,j)));
    mL_Pb(i,j) = Pb(i,j,iLNE3(i,j),iLNE4(i,j,iLNE3(i,j)));
    mL_Pl(i,j) = Pl(i,j,iLNE3(i,j),iLNE4(i,j,iLNE3(i,j)));
    mL_Ci(i,j) = Ci_m(i,j,iLNE3(i,j),iLNE4(i,j,iLNE3(i,j)));
    mL_Ce(i,j) = Ce_m(i,j,iLNE3(i,j),iLNE4(i,j,iLNE3(i,j)));
    mL_av(i,j) = av_m(iLNE4(i,j,iLNE3(i,j)));
    mL_dp(i,j) = dphi_m(iLNE3(i,j));
  end
end

disp(' Ea [MV/m], f [GHz], frep [Hz], Pbeam [MW/beam], Pin [MW/linac]')
disp(sprintf('     100       12    %g      %g        %g',...
    mL_fr(nem,nfm),mL_Pb(nem,nfm),mL_Pl(nem,nfm)))


figure(1)
plot(Ea_m,mL_Lum.*mL_Eff./mL_Ne,sprintf('%s',linecolor),'LineWidth',2); hold on
title('L/P optimum')
xlabel('<E_a_c_c> [MV/m]'),grid on;
ylabel('L_b_x/N*\eta [a.u.]')

figure(2)
plot(Ea_m,mL_Eff,sprintf('%s',linecolor),'LineWidth',2); hold on
title('L/P optimum')
xlabel('<E_a_c_c> [MV/m]'),grid on;
ylabel('\eta [%]')

figure(3)
plot(Ea_m,mL_av,sprintf('%s',linecolor),'LineWidth',2); hold on
title('L/P optimum')
xlabel('<E_a_c_c> [MV/m]'),grid on;
ylabel('a/\lambda')

figure(4)
plot(Ea_m,mL_Lum./mL_Ne,sprintf('%s',linecolor),'LineWidth',2); hold on
title('L/P optimum')
xlabel('<E_a_c_c> [MV/m]'),grid on;
ylabel('L_b_x/N [a.u.]')

figure(5)
plot(Ea_m,mL_Pin,sprintf('%s',linecolor),'LineWidth',2); hold on
title('L/P optimum')
xlabel('<E_a_c_c> [MV/m]'),grid on;
ylabel('P_i_n [MW]')
figure(6)
plot(Ea_m,mL_tp,sprintf('%s',linecolor),'LineWidth',2); hold on
title('L/P optimum')
xlabel('<E_a_c_c> [MV/m]'),grid on;
ylabel('t_p [ns]')
figure(7)
plot(Ea_m,mL_Nc,sprintf('%s',linecolor),'LineWidth',2); hold on
title('L/P optimum')
xlabel('<E_a_c_c> [MV/m]'),grid on;
ylabel('bunch spacing [rf cycles]')
figure(8)
plot(Ea_m,25*(mL_Nl*dphi/360+0.5),sprintf('%s',linecolor),'LineWidth',2); hold on
title('L/P optimum')
xlabel('<E_a_c_c> [MV/m]'),grid on;
ylabel('Structure length [mm]')
figure(9)
plot(Ea_m,mL_Pb,sprintf('%s',linecolor),'LineWidth',2); hold on
title('L_1 = 2x10^3^4 [1/s/cm^2]')
xlabel('<E_a_c_c> [MV/m]'),grid on;
ylabel('Beam power [MW/beam]')
figure(10)
plot(Ea_m,mL_Pl,sprintf('%s',linecolor),'LineWidth',2); hold on
title('L_1 = 2x10^3^4 [1/s/cm^2]')
xlabel('<E_a_c_c> [MV/m]'),grid on;
ylabel('Linac input power [MW/linac]')
figure(11)
plot(Ea_m,mL_fr,sprintf('%s',linecolor),'LineWidth',2); hold on
title('L_1 = 2x10^3^4 [1/s/cm^2]')
xlabel('<E_a_c_c> [MV/m]'),grid on;
ylabel('repetition rate [Hz]')
figure(12)
plot(Ea_m,mL_fr/50,sprintf('%s',linecolor),'LineWidth',2); hold on
title('Repetition rate is limited to 50 Hz')
xlabel('<E_a_c_c> [MV/m]'),grid on;
ylabel('Luminosity reduction factor')
figure(13)
plot(Ea_m,(mL_Pl-mL_Pb)./((50.4-14)/(3/0.5)*100./Ea_m'),sprintf('%s',linecolor),'LineWidth',2); hold on
title('Power loss per meter is limited to the nominal at 3TeV')
xlabel('<E_a_c_c> [MV/m]'),grid on;
ylabel('Luminosity reduction factor')
if 1 % calculation of klystron peak power for 500GeV klystron based CLIC
  mL_Eff_pc = spline(tp_pc,Eff_pc,mL_tp);
  mL_N_pc = tp_klys./mL_tp;
  mL_Pl_peak = 1e9*mL_Pl./mL_fr./mL_tp; %peak power per linac [MW]
  mL_Pl_klys = mL_Pl_peak./mL_N_pc./mL_Eff_pc; %peak power from klystrons per linac [MW]
  mL_N_klys = mL_Pl_klys/P_klys;
  mL_N_klys = mL_N_klys*1.1/0.92; % Daniel request for emergy overhead in CLIC
  figure(14)
  plot(Ea_m,mL_N_klys,sprintf('%s',linecolor),'LineWidth',2); hold on
  title('Klystron based 500 GeV CLIC')
  xlabel('<E_a_c_c> [MV/m]'),grid on;
  ylabel('Number of klystrons')    
  figure(15)
  plot(Ea_m,mL_N_pc.*mL_Eff_pc,sprintf('%s',linecolor),'LineWidth',2); hold on
  title('Klystron based 500 GeV CLIC')
  xlabel('<E_a_c_c> [MV/m]'),grid on;
  ylabel('power gain')    
  figure(16)
  plot(Ea_m,mL_Lum.*mL_Eff./mL_Ne./mL_N_klys,sprintf('%s',linecolor),'LineWidth',2); hold on
  title('Klystron based 500 GeV CLIC')
  xlabel('<E_a_c_c> [MV/m]'),grid on;
  ylabel('FoM / Nklys')    
  figure(17)
  plot(Ea_m,mL_Lum.*mL_Eff./mL_Ne./mL_N_klys.*(Ea_m'.^0.5),sprintf('%s',linecolor),'LineWidth',2); hold on
  title('Klystron based 500 GeV CLIC')
  xlabel('<E_a_c_c> [MV/m]'),grid on;
  ylabel('FoM / Nklys x <E_a_c_c>^1^/^2')    
  figure(18)
  plot(1000./mL_N_klys,mL_Lum.*mL_Eff./mL_Ne,sprintf('%s',linecolor),'LineWidth',2); hold on
  title('Klystron based 500 GeV CLIC')
  xlabel('1000/Nklys'),grid on;
  ylabel('FoM')    
  figure(19)
  plot(1000./mL_N_klys,L1*50./mL_fr,sprintf('%s',linecolor),'LineWidth',2); hold on
  title('Klystron based 500 GeV CLIC')
  xlabel('1000/Nklys'),grid on;
  ylabel('L_1@50Hz [Hz/m^2] ')    
  figure(20)
  plot(Ea_m,147500./Ea_m+mL_N_klys',sprintf('%s',linecolor),'LineWidth',2); hold on
  title('Klystron based 500 GeV CLIC')
  xlabel('<E_a_c_c> [MV/m]'),grid on;
  ylabel('Cost [a.u.]')    
  figure(21)
  plot(mL_Lum.*mL_Eff./mL_Ne,147500./Ea_m+mL_N_klys',sprintf('%s',linecolor),'LineWidth',2); hold on
  title('Klystron based 500 GeV CLIC')
  xlabel('FoM'),grid on;
  ylabel('Cost [a.u.]')    
  figure(22)
  plot(mL_Eff,147500./Ea_m+mL_N_klys',sprintf('%s',linecolor),'LineWidth',2); hold on
  title('Klystron based 500 GeV CLIC')
  xlabel('\eta[%]'),grid on;
  ylabel('Cost [a.u.]')    
end
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for ifig=1:8
  figure(ifig)
  legend('high','medium','low','high_full')
end
%return
for ifig=1:8
  figure(ifig)
  legend('E_s<260MV/m, \DeltaT<56K',...
         'E_s<200MV/m, \DeltaT<40K',...
         'L_s=0.5m, E_s<200MV/m, \DeltaT<40K',...
         'L_s=0.5m, N_s=6, t_p=241.6ns',...
         'L_s=0.5m, N_s=6, t_p=483ns')
end
%return
for ifig=1:8
  figure(ifig)
  legend('E_s<260MV/m, \DeltaT<56K',...
         'L_s=0.5m',...
         'L_s=0.5m, N_s=6, t_p=241.6ns',...
         'L_s=0.5m, N_s=6, t_p=483ns')
end
for ifig=1:8
  figure(ifig)
  legend('Wt=10V, E_s<260MV/m, \DeltaT<56K',...
         'Wt=20V, L_s=0.5m',...
         'Wt=20V, L_s=0.5m, N_s=6, t_p=241.6ns',...
         'Wt=20V, L_s=0.5m, N_s=6, t_p=483ns')
end
for ifig=1:22
  figure(ifig)
  legend('2\pi/3: N_s=free, L_s>200mm, t_p=free',...
         '2\pi/3: N_s=6, L_s=230mm, t_p=242ns',...
         '2\pi/3: N_s=6, L_s=480mm, t_p=242ns',...
         '2\pi/3: N_s=6, L_s=480mm, t_p=483ns',...
         'CLIC\_G, t_p=242ns',...
         'CLIC\_G, t_p=483ns',...
         '5\pi/6: N_s=free, L_s>200mm, t_p=free',...
         '5\pi/6: N_s=6, L_s=230mm, t_p=242ns',...
         '5\pi/6: N_s=6, L_s=480mm, t_p=242ns',...
         '5\pi/6: N_s=6, L_s=480mm, t_p=483ns',...
         'Location','NorthEastOutside')
%         'Location','SouthOutside')
end
if 0
for ifig=1:13
  figure(ifig)
  legend('2\pi/3: N_s=free, L_s>200mm, t_p=free',...
         '2\pi/3: N_s=6, L_s=230mm, t_p=242ns',...
         '2\pi/3: N_s=6, L_s=230mm, t_p=323ns',...
         '2\pi/3: N_s=6, L_s=230mm, t_p=363ns',...
         '2\pi/3: N_s=6, L_s=230mm, t_p=483ns',...
         '5\pi/6: N_s=free, L_s>200mm, t_p=free',...
         '5\pi/6: N_s=6, L_s=230mm, t_p=242ns',...
         '5\pi/6: N_s=6, L_s=230mm, t_p=323ns',...
         '5\pi/6: N_s=6, L_s=230mm, t_p=363ns',...
         '5\pi/6: N_s=6, L_s=230mm, t_p=483ns',...
         'Location','NorthEastOutside')
end
for ifig=1:13
  figure(ifig)
  legend('2\pi/3: N_s=free, L_s>200mm, t_p=free',...
         '2\pi/3: N_s=6, L_s=480mm, t_p=242ns',...
         '2\pi/3: N_s=6, L_s=480mm, t_p=323ns',...
         '2\pi/3: N_s=6, L_s=480mm, t_p=363ns',...
         '2\pi/3: N_s=6, L_s=480mm, t_p=483ns',...
         '5\pi/6: N_s=free, L_s>200mm, t_p=free',...
         '5\pi/6: N_s=6, L_s=480mm, t_p=242ns',...
         '5\pi/6: N_s=6, L_s=480mm, t_p=323ns',...
         '5\pi/6: N_s=6, L_s=480mm, t_p=363ns',...
         '5\pi/6: N_s=6, L_s=480mm, t_p=483ns',...
         'Location','NorthEastOutside')
end
end
return

subplot(2,1,2)
[c,h] = contour(f_m,Ea_m,mL_Ci+mL_Ce,[0.8:0.02:0.9,0.95:.05:1.4,1.5:0.1:2.0]); clabel(c,h);
set(h,'LineWidth',2)
title('Total Cost [a.u.]. ')
ylabel('<E_a_c_c> [MV/m]');grid on;
xlabel('f [GHz]')

%%%%%%%%%
switch rf_name
  case 'P3rt3o3f-1C_Pin15800kW_tp70ns_dT56K_SSX60_tpmin0ns_tr1_d1eqd2_Cost1'
    text(8,160,'Total Cost optimum. Nstr = 1189188. Bandwidth-scaled. Pt_p^1^/^3/C = 6.0 MWns^1^/^3/mm, f^-^1, \DeltaT^m^a^x = 56 K, \sigma^*_x = 60 nm.')
end
grid on;
xlabel('f [GHz]')

if 1
figure(2)
%[c,h] = contour(f_m,Ea_m,mL_Eff,10:2:60,'b-'); clabel(c,h)
%[c,h] = contour(f_m,Ea_m,mL_av,0.09:0.01:0.21,'k-'); clabel(c,h)
[c,h] = contour(f_m,Ea_m,mL_dp,50:10:130,'b-'); clabel(c,h)
set(h,'LineWidth',2)
title('\Delta\phi [^o]. ')
ylabel('<E_a_c_c> [MV/m]');grid on;
grid on;
xlabel('f [GHz]')
end
%return

figure(2)
initDatafromDaniel(60,0);
for i = 1:length(f_m)
  for ia = 1:length(Ea_m)
    Ne_(i,ia) = calcBunchCharge(Ea_m(ia), mL_av(ia,i), 0, f_m(i));
    L1_(i,ia) = calcBunchLuminocity(Ea_m(ia), mL_av(ia,i), 0, f_m(i));
  end
end
%subplot(2,2,1)
[c,h] = contour(f_m,Ea_m,mLNE3,5:1:25); clabel(c,h); set(h,'LineWidth',2); grid on;
ylabel('<E_a_c_c> [MV/m]')
title('L_1/N*\eta [a.u.]')
xlabel('f [GHz]')
%return
subplot(2,2,2)
[c,h] = contour(f_m,Ea_m,mL_Eff,10:2:60); clabel(c,h); set(h,'LineWidth',2); grid on;
title('\eta [%]')
subplot(2,2,3)
[c,h] = contour(f_m,Ea_m,mL_av,0.09:0.01:0.21); clabel(c,h); set(h,'LineWidth',2); grid on;
xlabel('f [GHz]')
ylabel('<E_a_c_c> [MV/m]')
title('<a>/\lambda')
subplot(2,2,4)
[c,h] = contour(f_m,Ea_m,10*(L1_./Ne_)',[3:0.2:5]); clabel(c,h); set(h,'LineWidth',2); grid on;
xlabel('f [GHz]')
title('L_1/N [10^2^4/bx/m^2]')
%return


mLNE3 = mL_Lum.*mL_Eff./mL_Ne;

figure(3)
hold on;plot(f_m, squeeze(mLNE3(1,:)),'r-o','LineWidth',2)
hold on;plot(f_m, squeeze(mLNE3(2,:)),'b-^','LineWidth',2)
hold on;plot(f_m, squeeze(mLNE3(4,:)),'k-*','LineWidth',2)
hold on;plot(f_m, squeeze(mLNE3(6,:)),'m-d','LineWidth',2)
grid on
xlabel('f [GHz]')
legend('<E_a_c_c> = 90 MV/m',...
       '<E_a_c_c> = 100 MV/m',...
       '<E_a_c_c> = 120 MV/m',...
       '<E_a_c_c> = 140 MV/m',4)
%ylabel('Total Cost [a.u.]')
%axis([10,30,1,1.8])
ylabel('L_b_x/N*\eta [a.u.]')
axis([10,30,0,16])

figure(4)
hold on;plot(Ea_m, squeeze(mLNE3(:,1)),'r-o','LineWidth',2)
hold on;plot(Ea_m, squeeze(mLNE3(:,3)),'b-^','LineWidth',2)
hold on;plot(Ea_m, squeeze(mLNE3(:,6)),'k-*','LineWidth',2)
hold on;plot(Ea_m, squeeze(mLNE3(:,9)),'m-d','LineWidth',2)
hold on;plot(Ea_m, squeeze(mLNE3(:,14)),'c-<','LineWidth',2)
grid on
xlabel('<E_a_c_c> [MV/m]')
legend('f = 10 GHz',...
       'f = 12 GHz',...
       'f = 15 GHz',...
       'f = 20 GHz',...
       'f = 30 GHz',2)
%ylabel('Total Cost [a.u.]')
%axis([90,150,1,1.8])
ylabel('L_b_x/N*\eta [a.u.]')
axis([90,150,0,16])
return

