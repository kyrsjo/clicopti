function Ncycl = calcBunchSpacing(Nc, Q1, f1, A1, Wx_max, f)
Ncycl = 1;

c = 299.792458;%[mm/ns]

% tapered structure
nc=1:Nc;
% Parameters of the 1st, middle, and last cells from the data for
ncel=[1,fix((Nc+1)/2),Nc]; %cell numbers
%dx = [1.0, 1.0, 1.0]; %[mm] beam offset

fpol = interpQuad(f1, ncel, nc);
acels = interpQuad(A1, ncel, nc);
qcels = interpQuad(Q1, ncel, nc);

omega=2*pi*fpol;

Ncycl_max = 20; %
t=(1:Ncycl_max)/f;
omegat = omega'*t;
omegaQt = (0.5*omega./qcels)'*t;
wt = abs(acels*exp(-omegaQt+sqrt(-1)*omegat))/Nc;
wt1 = abs(acels(1).*exp(-omegaQt(1,:)+sqrt(-1)*omegat(1,:)));
wt2 = abs(acels(Nc).*exp(-omegaQt(Nc,:)+sqrt(-1)*omegat(Nc,:)));
wt3 = abs(acels(round(Nc/2)).*exp(-omegaQt(round(Nc/2),:)+sqrt(-1)*omegat(round(Nc/2),:)));
ind = (wt > Wx_max);
%[wtmin,Ncycl] = min(ind.*wt);
[x,Ncycl] = max(cumsum(ind));
Ncycl = Ncycl + 1;
%return

A1
Q1
figure(3)
semilogy(f*t, abs(wt),'b-',[0,20],Wx_max*[1,1],'r-',Ncycl*[1,1],[0.001,10000],'b--','LineWidth',2),grid on
hold on
semilogy(f*t, abs(wt1),'k-',f*t, abs(wt2),'r-',f*t, abs(wt3),'m-'),grid on
%title(sprintf('d=[%5.3f,%5.3f]mm, a=[%5.3f,%5.3f]mm, E_{acc}^{av}=150MV/m',d,a))
xlabel('Number of rf cycles')
ylabel('W_t [V/pC/mm/m]')
axis([0,20,0.03,300])
return;

% output wakes parameters for each cell
file = fopen('wake.out','w');
for i = nc
  fprintf(file, '%d %g %g %g \n', nc(i), fpol(i), acels(i), qcels(i));
end
fclose(file);
  
%plot(st,wt,'m-')