function [tf,Pin_unload] = calcStructEacc(NcC, Nc, dphi, f, Ea, Q, vg, rQ, Ne, Ncycl)
%tf [ns] - filling time of the structure
%Pin_load [MW] - input power of the loaded structure for average gradient = Ea
%Pin_unload [MW] - input power to get the same output power in unloaded structure 
%                  as the output power in loaded structure with input power of Pin_load 
%Pout_unload [MW] - output power in unloaded structure with input power of Pin_load 

global EAC_LOAD EAC_UNLOAD %[MV/m]
global P_LOAD P_UNLOAD %[MW]

% Calculation of accelerating gradient along the structure 
% from the parameters of the first, middle and last cells.
%NcC - Zero-ggradient coupler length / cell length
%Nc - number of regular cells
%dphi [degree] - rf phase advance per cell
%f [GHz] - frequency
%Ea [MV/m] - averaged loaded accelerating gradient
%Q - Q-factor in first,middle,last cells
%vg [%] - group velocity to speed of light ratio in first,middle,last cells
%rQ [LinacOhm/m] - r upon Q in first,middle,last cells
%Ne [10^9] - bunch population
%Ncycl - bunch spacing in rf cycles

c = 299792458; %[m/s] - speed of light
e = 1.6e-19;   %[C] - electron charge

Ea = Ea*(1+NcC/Nc)*1e6;%[V/m] - averaged loaded accelerating gradient (active)
f = f*1e9;            %[Hz]  - frequency
h = c/f*dphi/360;     %[m] - cell length
I = e*Ne*1e9*f/Ncycl; %[A] - beam current
% tapered structure
nc=1:Nc;
% Parameters of the 1st, middle, and last cells from the data for
ncel=[1,fix((Nc+1)/2),Nc]; %cell numbers

q = interpQuad(Q, ncel, nc);
v = 0.01*c*interpQuad(vg, ncel, nc);
r = interpQuad(rQ.*Q, ncel, nc);
a = 2*pi*f./(v.*q);
ar = a.*r;
ah = h*a;
ah2 = 2+ah;
ar2ah = ar./ah2/2;
hI = h*I;
hI2 = hI^2;

%calcultion of filling time
tf = sum(h./v)*1e9; %[ns]

%calculation of input power to maintain Eav_loaded but without beam
cump = cumprod((2-ah)./ah2);
SarP = ones(1,Nc);
SarP(2:Nc) = cump(1:Nc-1);
SarP = 2*SarP./ah2;
SarP = sqrt(ar.*SarP);
Pin_unload = (Ea*Nc/sum(SarP))^2;%[W] - input power for averege unloaded acc. gradient of Ea

Ean = 0;
%P_load = zeros(1,Nc+1);
P_beam = hI*Ea*Nc;%[W] - beam power for averege loaded acc. gradient of Ea
Pin_load = Pin_unload+P_beam;
pind = 1 + (P_beam < Pin_unload);

imax = 100;
while imax & abs(Ean-1) > 0.001
  P_LOAD(1) = Pin_load;
  ind = 1;
  for i = 1:Nc
    EAC_LOAD(i) = -ar2ah(i)*(hI - ind*sqrt(abs(hI2+4*P_LOAD(i)/ar2ah(i))));
    if EAC_LOAD(i) < 0
      ind = -1;
    end
    P_LOAD(i+1) = P_LOAD(i) - 2*h*(a(i)*P_LOAD(i)+I*EAC_LOAD(i))/ah2(i);
  end
  Ean = sum(EAC_LOAD(1:Nc))/(Nc*Ea);
  Pin_load = Pin_load*(abs(Ean)^(-pind));
  imax = imax-1;
end
EAC_LOAD(1:Nc) = EAC_LOAD(1:Nc)*1e-6; %[MV/m] - loaded acc. gradient for average loaded acc. gradient of Ea
EAC_UNLOAD(1:Nc) = sqrt(P_LOAD(1))*SarP*1e-6; %[MV/m] - unloaded acc. gradient for average loaded acc. gradient of Ea
P_LOAD(1:Nc+1) = P_LOAD(1:Nc+1)*1e-6; %[MW] - input power for average loaded acc. gradient of Ea
%Pin_load = Pin_load*1e-6; %[MW] - input power for average loaded acc. gradient of Ea
%Pin_unload = Pin_unload*1e-6; %[MW] - input power for averege unloaded acc. gradient of Ea
Pin_unload = P_LOAD(Nc+1)/cump(Nc); %[MW] - input power to get the same output power
                                         % in unloaded structure as the output power in loaded 
                                         % structure with input power of Pin_load  
P_UNLOAD(1:Nc+1) = P_LOAD(1)*[1,cump]; %[MW] - output power in unloaded structure 
                                         % with input power of Pin_load  
return

            disp('eff - imag')
            Nc
            P_load(Nc+1)
            EAC_LOAD(Nc)
            subplot(2,1,1),plot(1:Nc,real(EAC_LOAD(1:Nc)),'b-o',...
                1:Nc,imag(EAC_LOAD(1:Nc)),'b--o')
            subplot(2,1,2),plot(1:Nc,real(P_load(2:Nc+1)),'r-o',...
                1:Nc,imag(P_load(2:Nc+1)),'r--o')



