% plot parameters in x-band test structures
%a and d are radius and iris thickness) / lambda X 10

close all
clear all

c = 299.792458; %speed of light [mm/ns]
E = 3e12*1.6e-19; %[V*C] beams energy 3TeV
L1 = 2.03e38; %[Hz/m^2] given peak Luminosity for 0.7Ne@50Hzand em20mk 

f = 11.424; %working frequency in GHz
Ea = 100; %[MV/m] averaged loaded accelerating gradient
dphi = 120; %[degree] phase advance per cell
%Ncycl0 = 20; %number of RF periods between the bunches
Wxmax0 = 10; %reference max value of wake at the second bunch [V/pC/mm/m] at 3TeV
%Wxmax0 = 20; %reference max value of wake at the second bunch [V/pC/mm/m] at 500GeV
Nemax0 = 4; %reference max number of electrons per bunch [10^9]
Eamax0 = 150; %reference max avereged accelerrating gradient [MV/m]
Nb0 = 154; %reference number of bunches in the train
dT_Cu2CuZr = sqrt(551/511); %coefficient to take CuZr instead of Cu
dT_Cu2Ti = sqrt(58/1.8); %coefficient to take Ti instead of Cu



format long

name = 'T18_vg2p6_disk'
switch name
  case 'T18_vg2p6_disk'
    a =4.06; %input aperture
    Nc=18;   %Number of cells
    Ne=0;    %Ne=3.72; % bunche population
    Nb=312;  % number of bunches
    Ncycl=8; % bunch sepration in the numbers of rf periods
    Q =[6384, 6751, 7242]; %fundamental Q-factor in 1st middle and last cells
    vg=[2.61, 1.70, 1.02]; %group velosity
    rQ=[11860,14612,17719]; %shunt impedance per meter in Linac ohm over Q
    Es=[1.95, 1.9,  1.85];  %ratio of the surface electric field to the accelerating gradient
    Hs=[3.2,  2.8,  2.4];%[mA/V] %Ratio of the surface magnetic field to acc. gradient
    Sc=[0.47, 0.36, 0.28];%[mA/V] %Ratio of the surface Sc to the acc. gradient
end    
dT = 2.56.*(1.5.*Hs).^2*sqrt(f/29.985)
%return

figure(1)
NcCoup=0; %Zero - gradient coupler length / cell length
[Eff, tp, Pin, tp_P] = plotStructPars(NcCoup, Nc, dphi, f, Ea, Q, vg, rQ, Es, Sc, dT, Ne, Ncycl, Nb)
Pin/2/pi/a(1)*tp_P^(1/3)
figure(1)
title(name)
return

