% testing hdsmn subroutines
%a and d are radius and iris thickness) / lambda X 10

close all
clear all

global EAC_LOAD EAC_UNLOAD %[MV/m]

c = 299.792458; %speed of light [mm/ns]
E = 3e12*1.6e-19; %[V*C] beams energy 3TeV
%E = 0.5e12*1.6e-19; %[V*C] beams energy at 500 GeV
L1 = 2.03e38; %[Hz/m^2] given peak Luminosity for 0.7Ne@50Hzand em20mk 

f = 12; %working frequency in GHz
Ea = 100; %[MV/m] averaged loaded accelerating gradient
%Ea = 50; %[MV/m] averaged loaded accelerating gradient
dphi = 120; %[degree] phase advance per cell
Nb = 312; 
Ncycl = 6; 
Ne = 3.72;%

name = 'T24_vg2p6_disk' 
switch name
  case 'T24_vg2p6_disk'
  %Ne = 0;%
  NcC=0; Nc = 18;
  a =[3.87, 3.2,  2.53]; % [mm]
  Q =[6110, 6494, 6862]; 
  vg=[2.62, 1.7, 1.02]; % [% of c]
  rQ=[12500,15450,18723];% [LinacOhm/m] 
  Es=[1.98, 1.95, 1.98]; 
  Hs=[3.16, 2.74, 2.34]; %   [mA/V]
  Sc=[0.47, 0.37, 0.295];%Sc6[mA/V]
end
if Hs > 0
    dT = 2.56.*(1.5.*Hs).^2*sqrt(f/29.985)
end

vg_ = spline([1,(Nc+1)/2,Nc], vg, 1:Nc);

%figure(1)
%plot(1:Nc, vg_)
%figure(2)
%plot(1:Nc-1, (vg_(2:Nc)-vg_(1:Nc-1))./(vg_(2:Nc)+vg_(1:Nc-1)))


%return

%for Nc = [12,18,24,36,48]
for Nc = 48
    
[Eff, tp, Pin, tp_P, Scmax, dTmax] = plotStructPars(NcC, Nc, dphi, f, Ea, Q, vg, rQ, Es, Sc, dT, Ne, Ncycl, Nb)
PCt = Pin/2/pi/(a(1))*tp_P^(1/3)
Sct = Scmax*tp_P^(1/3)
dTmax

Ea3(1,1:3) = spline(1:Nc, EAC_UNLOAD, [1,(Nc+1)/2,Nc]);

figure(3)
plot(Nc*[1,1,1],Ea3, 'r-d'); hold on
plot(Nc*[1,1,1],Ea3.*Es, 'g-d'); hold on
plot(Nc*[1,1,1],Ea3.*Hs, 'b-d'); hold on
figure(4)
plot(Nc,sqrt(Pin/2/pi/(a(1))*f), 'ko'); hold on
plot(Nc*[1,1,1],sqrt(Ea3.^2.*Sc/1000), 'm-d'); hold on
end

return

       