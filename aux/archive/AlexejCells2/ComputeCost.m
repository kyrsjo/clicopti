function [Ci Ce Cia]=ComputeCost(G,freq,Lstruct,Tpulse,FOM,frep,Epuls,PhaseAdvance)
channel = ddeinit('excel','AlexejTableCosts20061106.xls:AlexejValues');
ddepoke(channel,'r4c12:r11c12',[G freq Lstruct Tpulse FOM frep Epuls PhaseAdvance]');
ddeterm(channel);
%pause(1)
channel = ddeinit('excel','AlexejTableCosts20061106.xls:Total');
Ci = ddereq(channel,'r15c3:r15c3');
Cia = ddereq(channel,'r4c3:r12c3');
Ce = ddereq(channel,'r34c3:r34c3');
ddeterm(channel);