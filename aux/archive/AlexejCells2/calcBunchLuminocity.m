function [L1, Lt, sz] = calcBunchLuminocity(Ea, a, delta, f)
%Ea [MV/m] - averaged loaded accelerating gradient
%a - average iris radius over lambda
%delta - (a1-a2)/a
%f [GHz] - frequency
%%%%%%%%
%L1 [1e34/m^2] - luminosity in 1% of energy band
%Lt [1e34/m^2] - total luminosity
%sz [um] - bunch length sigma


global FREQUENCY A_OVER_LAMBDA DA_OVER_LAMBDA BUNCH_CHARGE BUNCH_LENGTH LUMI_TOTAL LUMI_PEAK

nf = length(FREQUENCY);
na = length(A_OVER_LAMBDA);
nd = length(DA_OVER_LAMBDA);

if f == 12
  a0 = A_OVER_LAMBDA(1);
  da = A_OVER_LAMBDA(2)-a0;
  ja = (a-a0)/da+1;
  ja0 = floor(ja);
  if ja0 > 1 & abs(ja0-ja) < 0.000001
    ja0 = ja0 - 1;
  end
  wa = ja-ja0;
  l = BUNCH_LENGTH(:,ja0,1)*(1-wa) ...
    + BUNCH_LENGTH(:,ja0+1,1)*wa;
  lt = LUMI_TOTAL(:,ja0,1)*(1-wa) ...
     + LUMI_TOTAL(:,ja0+1,1)*wa;
  l1 = LUMI_PEAK(:,ja0,1)*(1-wa) ...
     + LUMI_PEAK(:,ja0+1,1)*wa;
  sz = interpQuad(l', DA_OVER_LAMBDA, delta); 
  Lt = interpQuad(lt', DA_OVER_LAMBDA, delta);
  L1 = interpQuad(l1', DA_OVER_LAMBDA, delta);
end
return
