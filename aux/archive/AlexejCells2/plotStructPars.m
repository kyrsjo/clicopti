function [y, z, x, xx, xxx,xxxx] = plotStructPars(NcC, Nc, dphi, f, Ea, Q, vg, rQ, Es, Sc, dT, Ne, Ncycl, Nb)
%NcC - Zero-ggradient coupler length / cell length
%Nc - number of cells
%dphi [degree] - rf phase advance per cell
%f [GHz] - frequency
%Ea [MV/m] - averaged loaded accelerating gradient
%Q - Q-factor in first,middle,last cells
%vg [%] - group velocity to speed of light ratio in first,middle,last cells
%rQ [LinacOhm/m] - r upon Q in first,middle,last cells
%Es - max surface field to accelerating gradient in first,middle,last cells
%Sc - max surface power flow to accelerating gradient square in first,middle,last cells
%dT - max delta T for Eacc=150MV/m and tp=130ns in first,middle,last cells
%Ne [10^9] - bunch population
%Ncycl - bunch spacing in rf cycles
%Nb - number of bunches in the train

global EAC_LOAD EAC_UNLOAD %[MV/m]
global P_LOAD P_UNLOAD %[MW]

[tf,Pin_unload] = ...
  calcStructEacc(NcC, Nc, dphi, f, Ea, Q, vg, rQ, Ne, Ncycl);

c = 299792458; %[m/s] - speed of light
e = 1.6e-19;   %[C] - electron charge

h = c/f*1e9*dphi/360;  %[m] - cell length
Ls = h*(Nc + NcC);%[m] - total (clls+couplers) structure length
Qb = e*Ne*Nb;          %[nC] - beam charge
tb = (Nb-1)*Ncycl/f;   %[ns] - beam length
tr = 100./(f*0.47*(min(vg))^1.25); %[ns] - rise time
if Ne == 0
  tf = 0.01;
  tr = 0.01;
  tb = 100;
%  tb = 252;
end
tp = tf + tb + tr;          %[ns] - rf pulse length
eff = 100*Ls*Ea*Qb/(P_LOAD(1)*tp); % [%] - rectangular pulse rf-to-beam efficiency
% tapered structure
nc=1:Nc;
% Parameters of the 1st, middle, and last cells from the data for
ncel=[1,fix((Nc+1)/2),Nc]; %cell numbers
es = interpQuad(Es, ncel, nc);
sc = interpQuad(Sc, ncel, nc);
dt = interpQuad(dT, ncel, nc);

Es_load = es.*EAC_LOAD(1:Nc); %[MV/m]
Es_unload = es.*EAC_UNLOAD(1:Nc); %[MV/m]
Sc_load = sc/1000.*(EAC_LOAD(1:Nc)).^2; %[MW/mm2]
Sc_unload = sc/1000.*(EAC_UNLOAD(1:Nc)).^2; %[MW/mm2]
%calc effective pulse length for temperuture rise
pl = Pin_unload/P_LOAD(1);
t = [0,tr,tr+tf,tp,tp+tr,tp+tr+tf];
p = [0,pl,1,1,1-pl,0];
t_ = 0:0.01:max(t);
p_ = interp1(t,p,t_,'linear');
switch 2
case 0 %rect pulse shape
  tp_T = tp %[ns]
case 1 %any pulse shape approximation
  tp_T = tp-(tf/2+tr-(tr+tf)*pl/2)%[ns]
  dT_t = sqrt(t_);%[sqrt(ns)]
case 2 %any pulse shape convolution
  dT_t = conv(p_,[0,1./sqrt(t_(2:length(t_)))])*(t_(2)-t_(1))/2;%[sqrt(ns)]
  tp_T = (max(dT_t)).^2%[ns]
end
tpn = sqrt(tp_T/130);
dT_load = tpn*dt.*(EAC_LOAD(1:Nc)/150).^2;
dT_unload = tpn*dt.*(EAC_UNLOAD(1:Nc)/150).^2;
%%%%%%%%%%%%%%%
nc2 = nc-0.5;
figure(1)
plot([0,nc],P_LOAD(1:Nc+1),'k-',[0,nc],P_UNLOAD(1:Nc+1),'k--',...
     nc2,EAC_LOAD(1:Nc),'r-',nc2,EAC_UNLOAD(1:Nc),'r--',...
     nc2,Es_load,'g-',nc2,Es_unload,'g--',...
     nc2,Sc_load*50,'m-',nc2,Sc_unload*50,'m--',...
     nc2,dT_load,'b-',nc2,dT_unload,'b--','LineWidth',2)
grid on
xlabel('iris number','FontWeight','bold')
ylabel('P [MW] (black), E_s (green), E_a (red) [MV/m], \DeltaT [K] (blue), S_c*50 [MW/mm^2] (magenta)','FontWeight','bold')
title('Parameters of unloaded (deshed) and loaded (solid) structure','FontWeight','bold')
text(1,   6+dT_unload(1),sprintf('%4.1f',dT_unload(1)),'FontWeight','bold')
text(Nc-1,6+dT_unload(Nc),sprintf('%4.1f',dT_unload(Nc)),'FontWeight','bold')
text(1,   6+Es_unload(1),sprintf('%3.0f',Es_unload(1)),'FontWeight','bold')
text(Nc-1,6+Es_unload(Nc),sprintf('%3.0f',Es_unload(Nc)),'FontWeight','bold')
text(1,   6+Sc_unload(1)*50,sprintf('%3.1f',Sc_unload(1)),'FontWeight','bold')
text(Nc-1,6+Sc_unload(Nc)*50,sprintf('%3.1f',Sc_unload(Nc)),'FontWeight','bold')
text(1,   6+EAC_UNLOAD(1),sprintf('%3.0f',EAC_UNLOAD(1)),'FontWeight','bold')
text(Nc-1,6+EAC_UNLOAD(Nc),sprintf('%3.0f',EAC_UNLOAD(Nc)),'FontWeight','bold')
text(1,   6+P_UNLOAD(1),sprintf('%4.1f',P_UNLOAD(1)),'FontWeight','bold')
text(Nc-1,6+P_UNLOAD(Nc+1),sprintf('%4.1f',P_UNLOAD(Nc+1)),'FontWeight','bold')

text(3,100,sprintf(...
'P_i_n^l^o^a^d = %5.1f MW, P_o_u_t^l^o^a^d = %5.1f MW \nEff = %4.1f %%  \nt_r = %4.1f ns, t_f = %4.1f ns, t_p = %5.1f ns',...
                     P_LOAD(1), P_LOAD(Nc+1), eff, tr, tf, tp),'FontWeight','bold')
              
figure(2)
plot(t,p,'r-','LineWidth',2),hold on
%effective rectangular pulse length for breakdown
p_P = p_>0.9;
plot(t_,p_P,'k-',t_,dT_t(1:length(t_))/sqrt(tp_T),'b-','LineWidth',2),grid
xlabel('t [ns]')
ylabel('P_i_n/P_i_n^l^o^a^d, \DeltaT/\DeltaT^m^a^x')
axis([0,tp+tr+tf,0,1.1])
tp_P = sum(p_P)*(t_(2)-t_(1))
text(3,1.05,sprintf(...
  't_r = %4.1f ns, t_f = %4.1f ns, t_p = %5.1f ns, t_p^P = %5.1f ns',...
   tr, tf, tp, tp_P))

if 0
figure(3)
%p_T = (cumsum(p_(2:length(t_))./sqrt(max(t)-t_(2:length(t_))))*(t_(2)-t_(1))/2)/sqrt(130);%[ns]
p_T = (conv(p_,1./sqrt(t_(2:length(t_))))*(t_(2)-t_(1))/2)/sqrt(130);%[ns]
if 0
t = [0,tp,tp+0.01,tp+tr+tf];
p = [1,1,0,0];
t_ = 0:0.01:max(t);
p_ = interp1(t,p,t_,'linear');
%p_T = (cumsum(p_(2:length(t_))./sqrt(t_(2:length(t_))))*(t_(2)-t_(1))/2)/sqrt(130);%[ns]
p_T0 = (conv(p_,1./sqrt(t_(2:length(t_))))*(t_(2)-t_(1))/2)/sqrt(130);%[ns]
%plot(t_(2:length(t_)),p_T,'r-','LineWidth',2)
plot(t_(1:length(t_)),p_T0(1:length(t_)),'r-','LineWidth',2),hold on
end
plot(t_(1:length(t_)),p_T(1:length(t_)),'b-','LineWidth',2),hold off
max(p_T(1:length(t_)))
xlabel('t_p [ns]')
ylabel('\DeltaT/\DeltaT_0')
axis([0,tp+tr+tf,0,1.3]), grid on
end

y = eff;
z = tp;
x = P_LOAD(1);
xx= tp_P;
xxx = max(Sc_unload);
xxxx = max(dT_unload);
return



