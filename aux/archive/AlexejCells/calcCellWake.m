function [Q1, f1, A1] = calcCellWake(a, d, f, dphi)
%a - iris radius over lambda
%d - iris thickness over cell length
%f [GHz] - frequency
%dphi [degree] - rf phase advance per cell

% bunch length form factor scaling 
  c = 299.792458; %speed of light [mm/ns]
  sigma_z = 0.6; %[mm]
% frequency scaling
  f0 = 29.985; %[GHz] reference frequency, all below parameter are for f0
  ff = f/f0;
  f3 = ff^3;
% parameters at 29.985 GHz
  switch dphi
    case 120
      [Q1, f1, A1] = calcCellWake_120(a, d);
    case 150
      [Q1, f1, A1] = calcCellWake_150(a, d);
    otherwise
      m = 3;
      n = length(a);
      Q1_m = zeros(m,n);
      f1_m = zeros(m,n);
      A1_m = zeros(m,n);
      if dphi > 90
        dp_m = [90, 110, 130];
        [Q1_m(1,:), f1_m(1,:), A1_m(1,:)] = calcCellWake_090(a, d);
        [Q1_m(2,:), f1_m(2,:), A1_m(2,:)] = calcCellWake_110(a, d);
        [Q1_m(3,:), f1_m(3,:), A1_m(3,:)] = calcCellWake_130(a, d);
      else
        dp_m = [50, 70, 90];
        [Q1_m(1,:), f1_m(1,:), A1_m(1,:)] = calcCellWake_050(a, d);
        [Q1_m(2,:), f1_m(2,:), A1_m(2,:)] = calcCellWake_070(a, d);
        [Q1_m(3,:), f1_m(3,:), A1_m(3,:)] = calcCellWake_090(a, d);
      end
      Q1 = (interpQuad(Q1_m', dp_m, dphi))';
      f1 = (interpQuad(f1_m', dp_m, dphi))';
      A1 = (interpQuad(A1_m', dp_m, dphi))';
  end
% scaling to zero-bunch length at reference frequency f1
  A1 = A1.*exp((2*pi*f1.*sigma_z/c).^2/2);
%scaling parameters to f
  f1 = f1.*ff;
  A1 = A1.*f3;
return;

function [Q1, f1, A1] = calcCellWake_120(a, d)
  d_n = [0.1,0.25,0.4]; %d/h
% parameters at 29.985 GHz dphi = 120 degree
  Q1mn(1,:) = [ 6.7,  7.8,  9.0];
  Q1mn(2,:) = [ 8.3,  9.9, 11.7];
  Q1mn(3,:) = [12.8, 17.8, 22.4];
  Q1mn(4,:) = [35, 52, 71];
  Q1mn(5,:) = [95,156,209]; 
%
  f1mn(1,:) = [45.32, 44.77, 43.74]; 
  f1mn(2,:) = [42.97, 42.56, 41.65]; 
  f1mn(3,:) = [39.91, 39.87, 39.38]; 
  f1mn(4,:) = [37.47, 37.47, 37.47]; 
  f1mn(5,:) = [36.22, 36.22, 36.22]; 
%
  A1mn(1,:) = [2588, 2647, 2476]; 
  A1mn(2,:) = [2292, 2276, 2050]; 
  A1mn(3,:) = [1610, 1522, 1359]; 
  A1mn(4,:) = [894, 894, 821]; 
  A1mn(5,:) = [560, 577, 552]; 

%interpolation 
  Q1 = interpCellPars(a, d, d_n, Q1mn);
  f1 = interpCellPars(a, d, d_n, f1mn);
  A1 = interpCellPars(a, d, d_n, A1mn);
return;

function [Q1, f1, A1] = calcCellWake_150(a, d)
  d_n = [0.1,0.25,0.4]; %d/h
% parameters at 29.985 GHz dphi = 150 degree
  Q1mn(1,:) = [ 5.9,  6.8,  8.5];
  Q1mn(2,:) = [ 6.8,  7.4, 10.4];
  Q1mn(3,:) = [12.8, 11.1, 17.8];
  Q1mn(4,:) = [23, 24, 35];
  Q1mn(5,:) = [70, 89,168]; 
%
  f1mn(1,:) = [45.46, 44.97, 43.61]; 
  f1mn(2,:) = [43.68, 43.14, 41.96]; 
  f1mn(3,:) = [40.38, 40.31, 39.75]; 
  f1mn(4,:) = [37.63, 37.93, 37.93]; 
  f1mn(5,:) = [36.10, 36.10, 36.10]; 
%
  A1mn(1,:) = [1582, 1814, 1824]; 
  A1mn(2,:) = [1495, 1771, 1586]; 
  A1mn(3,:) = [1069, 1364, 1161]; 
  A1mn(4,:) = [708, 776, 712]; 
  A1mn(5,:) = [423, 458, 460]; 

%interpolation 
  Q1 = interpCellPars(a, d, d_n, Q1mn);
  f1 = interpCellPars(a, d, d_n, f1mn);
  A1 = interpCellPars(a, d, d_n, A1mn);
return;
