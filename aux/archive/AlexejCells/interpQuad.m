function [yn] = interpQuad(ynm, xm, xn)
% quadratic interpolation

dx1 = xm(2)-xm(1);
dx2 = xm(3)-xm(2);
dx3 = dx1 + dx2;
a = (ynm(:,1)*dx2 - ynm(:,2)*dx3 + ynm(:,3)*dx1) / (dx1*dx2*dx3);
b = (ynm(:,2)-ynm(:,1))/dx1 - a*(xm(2)+xm(1));
c = ynm(:,1) - b*xm(1) - a*xm(1)*xm(1);
yn = a*xn.^2 + b*xn +c*ones(1,length(xn));
return;

