/*
 *  This file is part of CLICopti.
 *
 *  Authors: Kyrre Sjobak, Daniel Schulte, Alexej Grudiev, Andrea Latina, Jim Ögren
 *
 *  We have invested a lot of time and effort in creating the CLICopti library,
 *  please cite it when using it; see the CITATION file for more information.
 *
 *  CLICopti is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  CLICopti is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with CLICopti.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CELL_H
#define CELL_H

#include <cstddef>

#include <vector>
#include <iostream>

/** Description of the parameters of a given cell.
 *  Some may be set to NaN!
 */
struct CellParams {
  //Geometry
  double h;   //Cell length [m]
  double a;   //Iris aperture [m]

  double d_n; //Iris thickness / h (normalized)
  double a_n; //Normalized iris aperture: a/lambda

  //Main mode
  double f0;   //Frequency [GHz]
  double psi; //Phase advance [deg]

  double Q;  //Q-factor
  double vg; //Group velocity [%c]
  double rQ; //r/Q [linacOhm / m]

  double Es; //Esurf/Eacc
  double Hs; //Hsurf/Eacc [mA/V]
  double Sc; //Sc/Eacc^2 [mA/V]

  //Wakefield 1st transverse mode
  double f1mn; //Frequency [GHz]
  double Q1mn; //Q-factor
  double A1mn; //Amplitude [V/pC/mm/m]
};

inline double getByOffset(const CellParams& cell, const size_t off) {
  return *( (double*) ( (char*)(&cell) + off ) );
};
inline double getByOffset(const CellParams* const cell, const size_t off) {
  return *( (double*) ( (char*)(cell) + off ) );
};

//Output
std::ostream& operator<< (std::ostream &out, const CellParams& cell);

//Initializers
CellParams Cell_TD_30GHz_v1_fileParse(std::string& line);
CellParams Cell_TD_12GHz_v1_fileParse(std::string& line);

//Math operators
CellParams operator*(const CellParams& lhs, const double rhs);
inline CellParams operator*(const double lhs, const CellParams& rhs) {return rhs*lhs;};
//inline CellParams operator*=(const CellParams lhs, const double& rhs) {return lhs*rhs;};
inline CellParams operator/(const CellParams& lhs, const double rhs) {return lhs*(1.0/rhs);};
//inline CellParams operator/=(const CellParams& lhs, const double rhs) {return lhs/rhs;};
CellParams operator+(const CellParams& lhs, const CellParams& rhs);
//inline CellParams operator+=(const CellParams& lhs, const CellParams& rhs) {return lhs+rhs;};
inline CellParams operator-(const CellParams& lhs, const CellParams& rhs) {return lhs+((-1)*rhs);};
//inline CellParams operator-=(const CellParams& lhs, const CellParams& rhs) {return lhs-rhs;};




/** CellParams "output" parameters for a CLIC "TD" type cell
 *
 */
/* class Cell_ClicTD : public CellParams { */
/*   Cell_ClicTD(double a, double d, */
/* 	      double eow, double c, */
/* 	      double sFrac, double e, double b); */
/*   const double a;      //Iris aperture radius      [mm] */
/*   const double d;      //Iris thickness            [mm] */
/*   const double eow;    //Outer wall ellipticity */
/*   const double c;      //Outer wall flat lenght */
/*   const double sFrac;  //Iris flat length fraction */
/*   const double e;      //Iris ellipticity */
/*   const double b;      //Cell radius               [mm] */
/* }; */


#endif
