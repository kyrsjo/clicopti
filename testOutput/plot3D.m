fmatrix = dlmread('testCellLibrary_compareDBs_quad12v1CellDataDump.dat');
%fmatrix = dlmread('testCellLibrary_compareDBs_quad12v2CellDataDump.dat');

shape1 = 31
shape2 = 61

CEs = 220
CSc = 4.0
CPC = 2.3

an   = reshape(fmatrix(:,1) , shape1, shape2);
dn   = reshape(fmatrix(:,2) , shape1, shape2);
a    = reshape(fmatrix(:,3) , shape1, shape2);
Q    = reshape(fmatrix(:,4) , shape1, shape2);
vg   = reshape(fmatrix(:,5) , shape1, shape2);
rQ   = reshape(fmatrix(:,6) , shape1, shape2);
Es   = reshape(fmatrix(:,7) , shape1, shape2);
Hs   = reshape(fmatrix(:,8) , shape1, shape2);
Sc   = reshape(fmatrix(:,9) , shape1, shape2);
f1mn = reshape(fmatrix(:,10), shape1, shape2);
Q1mn = reshape(fmatrix(:,11), shape1, shape2);
A1mn = reshape(fmatrix(:,12), shape1, shape2);

%mesh(an,dn,Es,'blue')
%mesh(an,dn,Sc,'ue')
%mesh(an,dn,Sc,'blue')

t_Es = (CEs^6 * 200.0) ./ Es.^6;
t_Sc = (CSc^3 * 200.0) ./ (Sc*1e-3).^3;
t_PC = 1e-9*(CPC^3 * 200.0) * (2.0*pi*a * 2.0*pi*11.9942e9 .* rQ ./ (vg*3e8/100.0) ).^3;

G_Es = (t_Es/200.0).^(1.0/6.0);
G_Sc = (t_Sc/200.0).^(1.0/6.0);
G_PC = (t_PC/200.0).^(1.0/6.0);

%figure(1)
%plot(an(:),Es(:))

figure(1)
hSurface1 = surf(an,dn,G_Es)
set(hSurface1,'FaceColor',[0 0 1],'FaceAlpha',0.7, 'LineStyle','none');
hold on;
hSurface2 = surf(an,dn,G_Sc)
set(hSurface2,'FaceColor',[0 1 0],'FaceAlpha',0.7, 'LineStyle','none');
hSurface3 = surf(an,dn,G_PC)
set(hSurface3,'FaceColor',[1 0 0],'FaceAlpha',0.7, 'LineStyle','none');
hold off
xlabel('a/\lambda')
ylabel('d/h')
zlabel('Max G [MV/m] @ 200 ns')
zlim( [20,200] )