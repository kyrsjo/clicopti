#include <iostream>
#include <cmath>

#include "structure.h"
#include "constants.h"

using namespace std;

int main(int argc, char* argv[]) {

  /** ** Minimal example ** **/

  //Nominal length, 80 MV/m (nominal gradient),
  // bunch charge 6.8e9 electrons, max kick 6.3 V/pC/mm/m
  AccelStructure_CLIC502 acs502(22);
  double voltage = 80e6*acs502.getL(); // [V]
  double q_bunch = 6.8e9*Constants::electron_charge*1e12; // [pC]
  double Wk_max = 20 * (4e9/6.8e9) * (80e6/150e6); //V/pC/mm/m
  
  cout << "FIRST=" << (acs502.getCellFirst()) << endl;
  cout << "MID  =" << (acs502.getCellMid()) << endl;
  cout << "LAST =" << (acs502.getCellLast()) << endl;
  cout << endl;  

  cout << "Length = " << acs502.getL() << " [m], L/2.0 = " << acs502.getL()/2.0 << " [m]" << endl;

  cout << endl << "Calculating integrals...";
  acs502.calc_g_integrals(1000);
  cout << "done." << endl;
  
  //Get the minimum bunch spacing
  int bunchSpacing = acs502.getMinBunchSpacing(Wk_max);
  cout << "Min bunch spacing = " << bunchSpacing << " cycles = " << bunchSpacing*2*M_PI/acs502.getOmega()*1e9 << " ns" << endl;
  bunchSpacing = 6; //Force old current (else power becomes very big, and t_beam = 0 due to RF constraints)
  double beamCurrent = q_bunch*1e-12/(bunchSpacing*2*M_PI/acs502.getOmega());
  cout << "Beam current = " << beamCurrent << " A" << endl;

  //Get the input power
  double P0 = acs502.getPowerLoaded(voltage, beamCurrent);
  cout << "Input power = " << P0/1e6 << " MW" << endl;
  
  //Steady-state efficiency
  cout << "Steady-state efficiency = " << acs502.getFlattopEfficiency(P0, beamCurrent)*100 << " %" << endl;

  //Get max beam time
  double t_beam = acs502.getMaxAllowableBeamTime(P0,beamCurrent);
  cout << "Maximum t_beam = " << t_beam*1e9 << " ns" << endl;
  
  //Total efficiency
  cout << "Total efficiency = " << acs502.getTotalEfficiency(P0, beamCurrent, t_beam)*100 << " %" << endl;

  cout << endl << endl << endl;

  /** ** Scan: Power vs. beam time ** **/
  cout << "Scan: Power VS beam time -> /tmp/testOptimize_acs502_P-tBeam.dat" << endl;
  
  ofstream ofile("/tmp/testOptimize_acs502_P-tBeam.dat");
  ofile << "# P[MW] gradient[MV/m] t_beam[ns] t_beam[cycles] totalEfficiency[%] maxEs[MV/m] maxSc[MW/mm^2] maxDeltaT[K]" << endl;
  ofile << "# beamCurrent = " << beamCurrent << " A" << endl;
  
  double Pmin = acs502.getPowerLoaded(0,beamCurrent);                   // 0    V/m
  double Pmax = acs502.getPowerLoaded(120e6*acs502.getL(),beamCurrent); // 120 MV/m
  cout << "Pmin = " << Pmin/1e6 << " MW, Pmax = " << Pmax/1e6 << " MW" << endl;
  
  int Psteps = 0; int tSteps = 40;
  for (int Pidx = 0; Pidx < Psteps; Pidx++) {
    double P = (Pmax-Pmin)/(Psteps-1)*Pidx + Pmin;
    double gradient = acs502.getVoltageLoaded(P,beamCurrent)/acs502.getL();
    double t_beam_max = acs502.getMaxAllowableBeamTime(P,beamCurrent);
    //cout << "P = " << P/1e6 << " MW, gradient = " << gradient/1e6 << " MV/m, t_beam_max = " << t_beam_max*1e9 << " ns" << endl;

    int maxPeriods = 8000000;
    if (t_beam_max > maxPeriods*2*M_PI/acs502.getOmega()) {
      cout << "Cut to " << maxPeriods << " periods = " << maxPeriods*2*M_PI/acs502.getOmega()*1e9 << " ns." << endl;
      t_beam_max = maxPeriods*2*M_PI/acs502.getOmega();
    }

    for (int tIdx = 0; tIdx < tSteps; tIdx++) {
      t_beam = t_beam_max/(tSteps-1)*tIdx;
      return_AccelStructure_getMaxFields maxFields = acs502.getMaxFields(P, beamCurrent);
      return_AccelStructure_getMaxDeltaT maxDeltaT = acs502.getMaxDeltaT(P, t_beam, beamCurrent);
      double totalEfficiency = acs502.getTotalEfficiency(P, beamCurrent, t_beam);
      //cout << "\t " << acs502.getTotalEfficiency(P, beamCurrent, t_beam)*100 << " %, " 
      //     << maxFields.maxEs << " " << maxFields.maxSc << " " << maxDeltaT.maxDeltaT << endl;
      ofile << P/1e6 << " " << gradient/1e6 << " " << t_beam*1e9 << " " << t_beam/(2*M_PI/acs502.getOmega()) << " "
	    << totalEfficiency*100 << " " << maxFields.maxEs << " " << maxFields.maxSc << " " << maxDeltaT.maxDeltaT << endl;
    }
  }
  ofile.close();
  cout << endl << endl;

  /** ** Scan: Optimum at different structure lengths ** **/
  cout << "Scan: Optimim at different structure lengths -> /tmp/testOptimize_acs502_N.dat" << endl;
  ofile.open("/tmp/testOptimize_acs502_N.dat");
  ofile << "# N L[m] P0[MW] t_ramp[ns] t_beam[ns] efficiency[%]" << endl;

  //for (size_t N = 1; N < 50; N++) {
  for (size_t N = 2; N < 50; N++) {
    AccelStructure_CLIC502 acsTest(N);
    voltage = 80e6*acsTest.getL(); // [V] (80 MV/m)
    q_bunch = 6.8e9*Constants::electron_charge*1e12; // [pC]
    
    cout << "Length = " << acsTest.getL() << " [m], L/2.0 = " << acsTest.getL()/2.0 << " [m]" << endl;

    cout << "Calculating integrals...";
    acsTest.calc_g_integrals(1000);
    cout << "done." << endl;
    
    //Get the minimum bunch spacing
    bunchSpacing = acsTest.getMinBunchSpacing(Wk_max);
    cout << "Min bunch spacing = " << bunchSpacing << " cycles = " << bunchSpacing*2*M_PI/acsTest.getOmega()*1e9 << " ns" << endl;
    bunchSpacing = 6; //Force old current (else power becomes very big, and t_beam = 0 due to RF constraints)
    beamCurrent = q_bunch*1e-12/(bunchSpacing*2*M_PI/acsTest.getOmega());
    cout << "Beam current = " << beamCurrent << " A" << endl;

    //Get the input power
    P0 = acsTest.getPowerLoaded(voltage, beamCurrent);
    cout << "Input power = " << P0/1e6 << " MW" << endl;
  
    //Steady-state efficiency
    cout << "Steady-state efficiency = " << acsTest.getFlattopEfficiency(P0, beamCurrent)*100 << " %" << endl;

    //Get max beam time
    t_beam = acsTest.getMaxAllowableBeamTime(P0,beamCurrent);
    cout << "Maximum t_beam = " << t_beam*1e9 << " ns" << endl;
  
    //Total efficiency
    cout << "Total efficiency = " << acsTest.getTotalEfficiency(P0, beamCurrent, t_beam)*100 << " %" << endl;

    ofile << N << " " << acsTest.getL() << " " << P0/1e6 << " " 
	  << (acsTest.getTrise() + acsTest.getTfill())*1e9 << " " << t_beam*1e9 << " " << acsTest.getTotalEfficiency(P0, beamCurrent, t_beam)*100 << endl;

    cout << endl;
  }
  ofile.close();

  

  // *** CLIC_G base structure ***
  CellBase_compat base("cellBase/TD_30GHz.dat", 11.9942);
  double G0 = 100; //MV/m
  double I_beam = 1.60217646e-19*3.72e9/0.5e-9; //[A]
  t_beam = 312*0.5e-9; //ns
  cout << "I_beam = " << I_beam << "[A]" << endl;
  cout << "t_beam = " << t_beam*1e9 << "[ns]" << endl;

  // Power, efficiency, max pulse length (plot all four limits) and minimum bunch spacing as function of structure *length*, at fixed voltage and current
  ofile.open("/tmp/testfile_acs_Ncells.dat");
  
  // ofile.open("/tmp/testfile_acs_Ncells_optimistic.dat");
  // AccelStructure::maxConstE  = pow(250.0,6.0)*200e-9;
  // AccelStructure::maxConstSc = pow(5.0,3.0)*200e-9;
  // AccelStructure::maxConstPC = pow(2.9,3.0)*200e-9;
  
  ofile << "# N L[mm] P0[MW] t_E[ns] t_Sc[ns] t_PC[ns] t_T[ns] t[ns] eff[%] eff2[%]" << endl;
  for (size_t Ncells = 1; Ncells < 200; Ncells++) {
    //cout << "Ncells=" << Ncells <<endl;
    ofile << Ncells << " ";
    
    //AccelStructure_paramSet2 acs(&base, Ncells, 120.0, 0.110022947942206,  0.016003337882503*2,  0.160233420548558, 0.040208386429788*2);  //Use the mm measurements. 24 cells to get acsG.
    AccelStructure_CLICG acs(Ncells);
    acs.calc_g_integrals(500);

    //cout << "Length = " << acs.getL()*1e3 << " mm" << endl;
    ofile << acs.getL()*1e3 << " ";

    //double Pin = acs.getPowerUnloaded(G0*1e6*acs.getL());
    double Pin = acs.getPowerLoaded(G0*1e6*acs.getL(), I_beam);
    //cout << "Pin at 100 MV/m = " << Pin/1e6  << " MW" << endl;
    ofile << Pin/1e6 << " ";

    return_AccelStructure_getMaxAllowableBeamTime_detailed maxTime = acs.getMaxAllowableBeamTime_detailed(Pin, I_beam);
    //cout << "Max beam time (P(G_L = 100 MV/m), I = I_beam): " << maxTime << endl;
    ofile << maxTime.time_E*1e9 << " " << maxTime.time_Sc*1e9 << " " << maxTime.time_PC*1e9 << " " << maxTime.time_dT*1e9 << " " << maxTime.time*1e9 << " ";
    
    double eff = acs.getTotalEfficiency(P0, I_beam, t_beam)*100;
    //cout << "Efficiency: " << eff << "%" << endl;
    ofile << eff << " ";
    
    ofile << acs.getTotalEfficiency(P0, I_beam, maxTime.time)*100 << " ";

    //cout << "Min bunch spacing = " << acs.getMinBunchSpacing(6.6) << endl;

    //cout << endl;
    ofile << endl;
  }
  ofile.close();
  
  // Power, efficiency, max pulse length (plot all four limits) and minimum bunch spacing as function of structure *average aperture*, at fixed voltage and current
  ofile.open("/tmp/testfile_acs_avgAperture.dat");
  ofile << "# a_n a[mm] a L[mm] P0[MW] t_E[ns] t_Sc[ns] t_PC[ns] t_T[ns] t[ns] eff[%] eff2[%]" << endl;
  for (double a_n = 0.07; a_n <= 0.23; a_n += 0.001) {
    ofile << a_n << " ";
    //cout << a_n << " ";
    
    AccelStructure_paramSet2 acs(&base, 26, 120.0, a_n,  0.0,  0.160233420548558, 0.0);
    ofile << acs.getCellFirst().a*1e3 << " ";
    ofile << acs.getL()*1e3 << " ";

    acs.calc_g_integrals(500);

    //double Pin = acs.getPowerUnloaded(G0*1e6*acs.getL());
    double Pin = acs.getPowerLoaded(G0*1e6*acs.getL(), I_beam);
    //cout << "Pin at 100 MV/m = " << Pin/1e6  << " MW" << endl;
    ofile << Pin/1e6 << " ";

    return_AccelStructure_getMaxAllowableBeamTime_detailed maxTime = acs.getMaxAllowableBeamTime_detailed(Pin, I_beam);
    //cout << "Max beam time (P(G_L = 100 MV/m), I = I_beam): " << maxTime << endl;
    ofile << maxTime.time_E*1e9 << " " << maxTime.time_Sc*1e9 << " " << maxTime.time_PC*1e9 << " " << maxTime.time_dT*1e9 << " " << maxTime.time*1e9 << " ";
    
    double eff = acs.getTotalEfficiency(P0, I_beam, t_beam)*100;
    //cout << "Efficiency: " << eff << "%" << endl;
    ofile << eff << " ";
    eff = acs.getTotalEfficiency(P0, I_beam, maxTime.time)*100;
    ofile << eff << " ";

    //cout << "Min bunch spacing = " << acs.getMinBunchSpacing(6.6) << endl;

    //cout << endl;
    ofile << endl;
  }
  ofile.close();


  // Power, efficiency, max pulse length (plot all four limits) and minimum bunch spacing as function of structure *aperture tapering*, at fixed voltage and current
  double aperture = 2.9; //mm, optimum efficiency from aperture scan
  double a_n = aperture / 25.01208917643528; // a / lambda

  ofile.open("/tmp/testfile_acs_deltaAperture.dat");
  ofile << "# a_n delta_a_n delta_a[mm] L[mm] P0[MW] t_E[ns] t_Sc[ns] t_PC[ns] t_T[ns] t[ns] eff[%] eff2[%]" << endl;
  for (double delta_a_n = 0.0; delta_a_n < 0.09; delta_a_n += 0.001) {
    AccelStructure_paramSet2 acs(&base, 26, 120.0, a_n,  delta_a_n,  0.160233420548558, 0.0);
    ofile << delta_a_n << " ";
    ofile << delta_a_n*25.01208917643528 << " ";
    ofile << acs.getL()*1e3 << " ";

    acs.calc_g_integrals(500);

    //double Pin = acs.getPowerUnloaded(G0*1e6*acs.getL());
    double Pin = acs.getPowerLoaded(G0*1e6*acs.getL(), I_beam);
    //cout << "Pin at 100 MV/m = " << Pin/1e6  << " MW" << endl;
    ofile << Pin/1e6 << " ";

    return_AccelStructure_getMaxAllowableBeamTime_detailed maxTime = acs.getMaxAllowableBeamTime_detailed(Pin, I_beam);
    //cout << "Max beam time (P(G_L = 100 MV/m), I = I_beam): " << maxTime << endl;
    ofile << maxTime.time_E*1e9 << " " << maxTime.time_Sc*1e9 << " " << maxTime.time_PC*1e9 << " " << maxTime.time_dT*1e9 << " " << maxTime.time*1e9 << " ";
    
    double eff = acs.getTotalEfficiency(P0, I_beam, t_beam)*100;
    //cout << "Efficiency: " << eff << "%" << endl;
    ofile << eff << " ";
    eff = acs.getTotalEfficiency(P0, I_beam, maxTime.time)*100;
    ofile << eff << " ";

    //cout << "Min bunch spacing = " << acs.getMinBunchSpacing(6.6) << endl;

    //cout << endl;
    ofile << endl;    
  }
  ofile.close();

  //Wakefield for two test structures
  AccelStructure_paramSet2 acs_noTaper(&base, 26, 120.0, a_n,  0.0,  0.160233420548558, 0.0);
  acs_noTaper.writeWakeFile("/tmp/testfile_acs_noTaper_wake.dat", 1.0, 0.0001);
  acs_noTaper.calc_g_integrals(500);
  acs_noTaper.writeProfileFile("/tmp/testfile_acs_noTaper_profile.dat", acs_noTaper.getPowerLoaded(G0*1e6*acs_noTaper.getL(), I_beam), I_beam);
    
  AccelStructure_paramSet2 acs_taper(&base, 26, 120.0, a_n,  1.75/25.01208917643528,  0.160233420548558, 0.0);
  acs_taper.writeWakeFile("/tmp/testfile_acs_taper_wake.dat", 1.0, 0.0001);
  acs_taper.calc_g_integrals(500);
  acs_taper.writeProfileFile("/tmp/testfile_acs_taper_profile.dat", acs_taper.getPowerLoaded(G0*1e6*acs_taper.getL(), I_beam), I_beam);  
}
