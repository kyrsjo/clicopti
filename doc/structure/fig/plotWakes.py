#! /usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

fname = "testfile_acsG_R05_wakeFile.dat"

(z, Wt, fabs, env, env2) = np.loadtxt(fname, unpack=True)

plt.plot(z, Wt)
plt.plot(z,env2,'r--')
plt.plot(z,-env2,'r--')

plt.xlim(0,0.2)
plt.xlabel("s [m]")
plt.ylabel(r"$W_\perp$ [V/pC/mm/m]")

plt.savefig("wake_lin.pdf")

plt.figure()
plt.semilogy(z, np.abs(Wt))
plt.semilogy(z,env2,'r--')
#plt.plot(z,-env2,'r--')

plt.xlim(0,1.0)
plt.ylim(1e-7,1e3)

plt.xlabel("s [m]")
plt.ylabel(r"$W_\perp$ [V/pC/mm/m]")

plt.savefig("wake_log.pdf")

plt.show()
