import numpy as np
import matplotlib.pyplot as plt

Z = []
W = []
Wabs = []
Env1 = [] #with detuning
Env2 = [] #sans detuning

#ifile = open("testfile_acs2_wakeFile.dat", 'r')
ifile = open("testfile_acsG_R05_wakeFile.dat", 'r')

ifile.readline()
for line in ifile:
    l = line.split()
    Z.append(float(l[0]))
    W.append(float(l[1]))
    Wabs.append(float(l[2]))
    Env1.append(float(l[3]))
    Env2.append(float(l[4]))
ifile.close()

plt.semilogy(Z,Wabs, label=r"|$W_\perp$|")
plt.semilogy(Z,Env2, "--", label="Envelope")


plt.xlim(0.0,1.0)
plt.ylim(1e-7,1e3)

plt.xlabel("z [m]")
plt.ylabel("$W_\\perp$ [V/pC/mm/m]")
#plt.legend()
#plt.savefig("plotWake.png")

plt.axhline(y=6.6, color="red", ls="--", label="Limit")
for i in xrange(6):
    if i == 1:
        plt.axvline(x=i*0.025, color="black", ls=":", label="RF cycle")
    else:
        plt.axvline(x=i*0.025, color="black", ls=":")
    
plt.axvline(x=6*0.025, color="red", ls="-", label="Min. spacing")
i=7
while i*0.025 <= 1.0:
    plt.axvline(x=i*0.025, color="black", ls=":")
    i +=1

plt.legend(loc=5)

plt.savefig("plotWake_limits.pdf");

plt.show()
