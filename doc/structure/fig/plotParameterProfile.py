#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

#import matplotlib
#matplotlib.use('pgf')

LATEX_TEXTWIDTH = 452.9679 #pt
LATEX_TEXTWIDTH *= 1.0/72.27 #inches
FIGWIDTH = 0.45*LATEX_TEXTWIDTH
FIGHEIGTH = FIGWIDTH / ( (1+np.sqrt(5))/2.0)
FIGSIZE = (FIGWIDTH, FIGHEIGTH)

FIGSIZE = map(lambda x : x*2.0, FIGSIZE) #1/2 size font


fname = "testfile_acsG_R05_parameterProfile.dat"

ifile = open(fname, 'r')
ifile.readline()
ifile.readline()
cellFirst_txt = ifile.readline()
cellMid_txt = ifile.readline()
cellLast_txt = ifile.readline()
ifile.close()

def parseCell_txt(intxt):
    ret = {}
    data = intxt.split(":")[1].strip()
    data = data.split(",")
    print data
    def getNumber(txt):
        n = txt.split("=")[1]
        n2 = n.split("[")[0]
        #print "'"+txt+"','"+n+"','"+n2+"'"
        return float(n2)

    ret['a']    = getNumber(data[1]);     print 'a',    ret['a']
    ret['Q']    = getNumber(data[6]);     print 'Q',    ret['Q']
    ret['vg']   = getNumber(data[7]);     print 'vg',   ret['vg']
    ret['rQ']   = getNumber(data[8]);     print 'rQ',   ret['rQ']
    ret['Es']   = getNumber(data[9]);     print 'Es',   ret['Es']
    ret['Hs']   = getNumber(data[10]);    print 'Hs',   ret['Hs']
    ret['Sc']   = getNumber(data[11]);    print 'Sc',   ret['Sc']
    ret['f1mn'] = getNumber(data[12]);    print 'f1mn', ret['f1mn']
    ret['Q1mn'] = getNumber(data[13]);    print 'Q1mn', ret['Q1mn']
    ret['A1mn'] = getNumber(data[14]);    print 'A1mn', ret['A1mn']
    
    return ret

cellFirst = parseCell_txt(cellFirst_txt)
cellMid   = parseCell_txt(cellMid_txt)
cellLast  = parseCell_txt(cellLast_txt)
#print cellFirst

(z, a, Q, vg, rQ, Es, Hs, Sc, f1mn, Q1mn, A1mn) = np.loadtxt(fname, unpack=True)

z *= 1000

rQ /= 1e3
cellFirst['rQ'] /= 1e3 #kOhm/m
cellMid  ['rQ'] /= 1e3 #kOhm/m
cellLast ['rQ'] /= 1e3 #kOhm/m


L = z[-1]

plt.figure(1,figsize=FIGSIZE)
plt.plot((0.0,L/2.0,L),(cellFirst['Q'],cellMid['Q'],cellLast['Q']),'r*', ms=15)
plt.plot(z,Q)
plt.xlim(0,L)
plt.xlabel('z [mm]')
plt.ylabel('Q')
plt.subplots_adjust(bottom=0.12)
plt.savefig('parameters_Q.pdf')

plt.figure(2, figsize=FIGSIZE)
plt.plot((0.0,L/2.0,L),(cellFirst['vg'],cellMid['vg'],cellLast['vg']),'r*', ms=15)
plt.plot(z,vg)
plt.xlim(0,L)
plt.xlabel('z [mm]')
plt.ylabel('$v_g$ [%c]')
plt.subplots_adjust(bottom=0.12)
plt.savefig('parameters_vg.pdf')

plt.figure(3, figsize=FIGSIZE)
plt.plot((0.0,L/2.0,L),(cellFirst['rQ'],cellMid['rQ'],cellLast['rQ']),'r*', ms=15)
plt.plot(z,rQ)
plt.xlim(0,L)
plt.xlabel('z [mm]')
plt.ylabel("$R'/Q$ [k$\Omega$/m]")
plt.subplots_adjust(bottom=0.12)
plt.savefig('parameters_rQ.pdf')

plt.figure(4, figsize=FIGSIZE)
plt.plot((0.0,L/2.0,L),(cellFirst['Es'],cellMid['Es'],cellLast['Es']),'r*', ms=15)
plt.plot(z,Es)
plt.xlim(0,L)
plt.xlabel('z [mm]')
plt.ylabel(r'$\bar E$')
plt.subplots_adjust(bottom=0.12)
plt.savefig('parameters_Es.pdf')

plt.figure(5, figsize=FIGSIZE)
plt.plot((0.0,L/2.0,L),(cellFirst['Hs'],cellMid['Hs'],cellLast['Hs']),'r*', ms=15)
plt.plot(z,Hs)
plt.xlim(0,L)
plt.xlabel('z [mm]')
plt.ylabel(r'$\bar H$ [mA/V]')
plt.subplots_adjust(bottom=0.12)
plt.savefig('parameters_Hs.pdf')

plt.figure(6, figsize=FIGSIZE)
plt.plot((0.0,L/2.0,L),(cellFirst['Sc'],cellMid['Sc'],cellLast['Sc']),'r*', ms=15)
plt.plot(z,Sc)
plt.xlim(0,L)
plt.xlabel('z [mm]')
plt.ylabel(r'$\bar S_c$ [mA/V]')
plt.subplots_adjust(bottom=0.12)
plt.savefig('parameters_Sc.pdf')


plt.show()
