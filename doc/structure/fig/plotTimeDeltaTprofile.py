#! /usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

fname = "testfile_acsG_R05_timeDeltaTprofile.dat"

(t,tb,T) = np.loadtxt(fname, unpack=True)

plt.plot(tb,T)
plt.xlim(0,600)

plt.ylabel(r"$\Delta T$ [K]")
plt.xlabel(r"$t_b = t - t_2$ [ns]")

plt.axhline(50, color='k', ls="--")

plt.savefig("timeDeltaT.pdf")

plt.show()
