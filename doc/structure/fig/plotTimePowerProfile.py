import numpy as np
import matplotlib.pyplot as plt

fname = "testfile_acsG_R05_timePowerProfile.dat"
#fname = "testfile_acs2_DB12v2_timePowerProfile.dat"

fname_temp = "testfile_acsG_R05_timeDeltaTprofile.dat"

ifile = open(fname,'r')
headerline =  ifile.readline()
ifile.close()
print "Got header line: '" + headerline + "'"
headerline = headerline.split()
P0 = float(headerline[3].split('=')[1][:-5])
Pbreak = float(headerline[4].split('=')[1][:-5])
tbeam = float(headerline[5].split('=')[1][:-4])
print "P0     =", P0,     "[MW]"
print "Pbreak =", Pbreak, "[MW]"
print "tbeam  =", tbeam,  "[ns]"

(t,P) = np.loadtxt(fname, unpack=True)
plt.plot(t*1e9,P,zorder=1000)

(t2,t2_beam,deltaT) = np.loadtxt(fname_temp,unpack=True)
print t2, deltaT

tr = 21.0 #15.3 pre-bugfix
tf = 61.9257
tb = 0.5*312
t1 = tr
t2 = tr+tf
t3 = tr+tf+tb
t4 = tr+tf+tb+tr
t5 = tr+tf+tb+tr+tf


plt.axvline(t1, ls='--',color="black")
plt.annotate("$t_1$", (t1,5))
plt.axvline(t2, ls='--',color="black")
plt.annotate("$t_2$", (t2,5))
plt.axvline(t3, ls='--',color="black")
plt.annotate("$t_3$", (t3,5))
plt.axvline(t4, ls='--',color="black")
plt.annotate("$t_4$", (t4,5))
plt.axvline(t5, ls='--',color="black")

plt.xlim(0,t5)
plt.ylim(0,70)

plt.xlabel("t [ns]")
plt.ylabel("$P$ [MW]")

maxP = max(P)

powerTextXpos = 285
plt.axhline(P0, ls='--', color='black')
plt.annotate("$P_0$",(powerTextXpos,P0+1), ha='center')

plt.axhline(Pbreak, ls='--', color='black')
plt.annotate(r"$P_\mathrm{start}$",(powerTextXpos,Pbreak+1), ha='center')

plt.axhline(P0-Pbreak, ls='--', color='black')
plt.annotate(r"$P_0 - P_\mathrm{start}$",(powerTextXpos,P0-Pbreak+1), ha='center')

#plt.axhline(P0*0.85, ls='--', color='red')
#plt.annotate("0.85 $P_0$",(powerTextXpos,52), ha='center')

#plt.annotate('', xy=(t2-18,P0*0.85), xycoords = 'data', xytext = (t3+8, maxP*0.85), textcoords = 'data', arrowprops = {'arrowstyle':'<->', 'color':'red', 'linewidth':2})
#plt.annotate("Counted for pulse length $\\tau$",(90,P0*0.85+1))


plt.annotate('', xy=(0,30), xycoords = 'data', xytext = (t1, 30), textcoords = 'data', arrowprops = {'arrowstyle':'<->'})
plt.annotate('$t_r$', xy=(tr/2.0,32), ha='center')

plt.annotate('', xy=(t1,30), xycoords = 'data', xytext = (t2, 30), textcoords = 'data', arrowprops = {'arrowstyle':'<->'})
plt.annotate('$t_f$', xy=(t1+tf/2.0,32), ha='center')

plt.annotate('', xy=(t2,30), xycoords = 'data', xytext = (t3, 30), textcoords = 'data', arrowprops = {'arrowstyle':'<->'})
plt.annotate('$t_b$', xy=(t2+tb/2.0,32), ha='center')

plt.annotate('', xy=(t3,30), xycoords = 'data', xytext = (t4, 30), textcoords = 'data', arrowprops = {'arrowstyle':'<->'})
plt.annotate('$t_r$', xy=(t3+tr/2.0,32), ha='center')

plt.annotate('', xy=(t4,30), xycoords = 'data', xytext = (t5, 30), textcoords = 'data', arrowprops = {'arrowstyle':'<->'})
plt.annotate('$t_f$', xy=(t4+tf/2.0,32), ha='center')

plt.savefig("powerProfile_noTau.png")
plt.savefig("powerProfile_noTau.pdf")

plt.show()
