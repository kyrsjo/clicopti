import numpy as np
import matplotlib.pyplot as plt

zMax = 0.0

#############################################
## FIELD GRADIENTS AT DIFFERENT CONDITIONS ##
#############################################

def loadPlot(fname,label,linestyle, col):
    (z,Ez,Es,Hs,Sc,P,PC) = np.loadtxt(fname,unpack=True)
    plt.plot (z*1e3,Ez,label=label,ls=linestyle, color=col)

    global zMax
    if max(z) > zMax:
        zMax = max(z)

plt.figure(1)
loadPlot("testfile_acsG_R05_loaded_G=10MVm.dat", "10 MV/m", "--",'b')
loadPlot("testfile_acsG_R05_unloaded_G=29.5718MVm.dat", "", "-",'b')
#loadPlot("testfile_acsG_R05_loaded_G=20MVm.dat", "20 MV/m","--")
#loadPlot("testfile_acsG_R05_unloaded_G=39.5718MVm.dat", "", "-")
loadPlot("testfile_acsG_R05_loaded_G=30MVm.dat", "30 MV/m","--",'g')
loadPlot("testfile_acsG_R05_unloaded_G=49.5718MVm.dat", "", "-",'g')
#loadPlot("testfile_acsG_R05_loaded_G=40MVm.dat", "40 MV/m","--")
#loadPlot("testfile_acsG_R05_unloaded_G=59.5718MVm.dat", "", "-")
loadPlot("testfile_acsG_R05_loaded_G=50MVm.dat", "50 MV/m","--",'r')
loadPlot("testfile_acsG_R05_unloaded_G=69.5718MVm.dat", "", "-",'r')
#loadPlot("testfile_acsG_R05_loaded_G=60MVm.dat", "60 MV/m","--")
#loadPlot("testfile_acsG_R05_unloaded_G=79.5718MVm.dat", "", "-")
loadPlot("testfile_acsG_R05_loaded_G=70MVm.dat", "70 MV/m","--",'k')
loadPlot("testfile_acsG_R05_unloaded_G=89.5718MVm.dat", "", "-",'k')
#loadPlot("testfile_acsG_R05_loaded_G=80MVm.dat", "80 MV/m","--")
#loadPlot("testfile_acsG_R05_unloaded_G=99.5718MVm.dat", "", "-")
loadPlot("testfile_acsG_R05_loaded_G=100MVm.dat", "100 MV/m","--",'c')
loadPlot("testfile_acsG_R05_unloaded_G=119.572MVm.dat", "", "-",'c')
loadPlot("testfile_acsG_R05_loaded_G=120MVm.dat", "120 MV/m","--",'m')
loadPlot("testfile_acsG_R05_unloaded_G=139.572MVm.dat", "", "-",'m')
plt.xlabel("z [mm]")
plt.ylabel("G(z) [MV/m]")

plt.xlim(0,zMax*1e3)
plt.ylim(-50,150)

plt.legend(frameon=False,ncol=3,loc=8)
#plt.title("Gradient profile, $I_b$=0 and $I_b$ = 1.19 A (dashed)")

#plt.savefig("gradientProfile_power.png")
plt.savefig("gradientProfile_power.pdf")

plt.figure(2)
zMax = 0.0
loadPlot("testfile_acsG_R05_loaded_G=100MVm_I=0.dat", "0.00 A", "-", 'b')
#loadPlot("testfile_acsG_R05_loaded_G=100MVm_I=0.25.dat", "0.25 A", "-")
loadPlot("testfile_acsG_R05_loaded_G=100MVm_I=0.5.dat", "0.50 A", "-",'g')
#loadPlot("testfile_acsG_R05_loaded_G=100MVm_I=0.75.dat", "0.75 A", "-",'r')
loadPlot("testfile_acsG_R05_loaded_G=100MVm_I=1.dat", "1.00 A", "-",'r')
#loadPlot("testfile_acsG_R05_loaded_G=100MVm_I=1.25.dat", "1.25 A", "-",'c')
loadPlot("testfile_acsG_R05_loaded_G=100MVm_I=1.5.dat", "1.50 A", "-",'c')
#loadPlot("testfile_acsG_R05_loaded_G=100MVm_I=1.75.dat", "1.75 A", "-")
loadPlot("testfile_acsG_R05_loaded_G=100MVm_I=2.dat", "2.00 A", "-",'m')

plt.xlabel("z [mm]")
plt.ylabel("G(z) [MV/m]")
plt.xlim(0,zMax*1e3)

plt.legend(frameon=False,ncol=1,loc=0)
#plt.title(r"Gradient profile, $\langle G \rangle$ = 100 MV/m")

#plt.savefig("gradientProfile_current.png")
plt.savefig("gradientProfile_current.pdf")

#################
## PEAK FIELDS ##
#################
 
LATEX_TEXTWIDTH = 452.9679 #pt
LATEX_TEXTWIDTH *= 1.0/72.27 #inches
FIGWIDTH = 0.45*LATEX_TEXTWIDTH
FIGHEIGTH = FIGWIDTH / ( (1+np.sqrt(5))/2.0)
FIGSIZE = (FIGWIDTH, FIGHEIGTH)
FIGSIZE = map(lambda x : x*2.0, FIGSIZE) #1/2 size font

FIGWIDTH2  = LATEX_TEXTWIDTH
FIGHEIGTH2 = FIGWIDTH2 / ( (1+np.sqrt(5))/2.0)
FIGSIZE2   = (FIGWIDTH2, FIGHEIGTH2)
FIGSIZE2   = map(lambda x : x*2.0, FIGSIZE2) #1/2 size font

(z_L,Ez_L,Es_L,Hs_L,Sc_L,P_L,PC_L) = np.loadtxt("testfile_acsG_R05_loaded_G=100MVm.dat",unpack=True)
(z,Ez,Es,Hs,Sc,P,PC) = np.loadtxt("testfile_acsG_R05_unloaded_G=119.572MVm.dat",unpack=True)

plt.figure(3,figsize=FIGSIZE)
plt.plot (z*1e3,Es)
plt.plot (z_L*1e3,Es_L,'r--')
plt.xlabel("z [mm]")
plt.ylabel(r"$\hat E(z)$ [MV/m]")
plt.subplots_adjust(bottom=0.12)
plt.xlim(0,zMax*1e3)
plt.savefig('peakfield_Es.pdf')

plt.figure(4,figsize=FIGSIZE)
plt.plot (z*1e3,Hs)
plt.plot (z_L*1e3,Hs_L,'r--')
plt.xlabel("z [mm]")
plt.ylabel(r"$\hat H(z)$ [kA/m]")
plt.subplots_adjust(bottom=0.12)
plt.xlim(0,zMax*1e3)
plt.savefig('peakfield_Hs.pdf')

plt.figure(5,figsize=FIGSIZE)
plt.plot (z*1e3,Sc)
plt.plot (z_L*1e3,Sc_L,'r--')
plt.xlabel("z [mm]")
plt.ylabel(r"$\hat S_c(z)$ [W/um$^2$]")
plt.subplots_adjust(bottom=0.12)
plt.xlim(0,zMax*1e3)
plt.savefig('peakfield_Sc.pdf')

plt.figure(6,figsize=FIGSIZE)
plt.plot (z*1e3,PC)
plt.plot (z_L*1e3,PC_L,'r--')
plt.xlabel("z [mm]")
plt.ylabel(r"$P/C$ [MW/mm]")
plt.subplots_adjust(bottom=0.12)
plt.xlim(0,zMax*1e3)
plt.savefig('peakfield_PC.pdf')

plt.figure(7,figsize=FIGSIZE2)
plt.plot (z*1e3,Ez)
plt.plot (z_L*1e3,Ez_L,'r--')
plt.xlabel("z [mm]")
plt.ylabel(r"$G(z)$ [MV/m]")
plt.subplots_adjust(bottom=0.12)
plt.xlim(0,zMax*1e3)
plt.savefig('peakfield_G.pdf')

plt.show()
