\chapter{Overview and installation}
\label{sec:intro}

The CLICopti RF structure parameter estimator is a C++ library, which makes it possible to quickly estimate the parameters of an RF structure from its length, apertures, tapering, and basic cell type. Typical estimated parameters are the input power required to reach a certain voltage with a given beam current, the maximum safe pulse length for a given input power and the minimum bunch spacing in RF cycles allowed by a given long-range wake limit.

This document describes the implemented physics, usage of the library through its Application Programming Interface (API) and the relation between the different parts of the library. Also discussed is how the library is checked for correctness, and the example programs included with the sources are described.

The document is divided into the following four chapters:
\begin{description}
\item[Chapter \ref{sec:intro}] describes how to download and install the library, as well as giving an overview of what it does.
\item[Chapter \ref{sec:cell}] describes the handling of the parameters describing a single accelerating cell.
\item[Chapter \ref{sec:database}] describes the cell database interface. This estimates the full parameter set of a single cell, given only it's iris aperture and thickness, within the range of the underlying database. The database interface works by interpolating between the data points in the database.
\item[Chapter \ref{sec:AccelStructure}] describes the code for estimating the parameters of an accelerating structure, based on the parameters of the constituent accelerating cells.
\item[Chapter \ref{sec:constants}] describes the contents of a header file which defines the values of a few useful numerical constants.
\end{description}
Each of the Chapters \ref{sec:cell} -- \ref{sec:constants} describes code implemented in one pair of \texttt{.h}/\texttt{.cpp}-files.
This document uses \texttt{typewriter font} when making direct references to names in the code and file names.

\section{Basic structure of the code}

\begin{figure}
  \input{intro/fig/codeStructure.tex}
  \caption{Overview of the program structure and general use pattern for CLICopti.}
  \label{fig:intro:code_overview}
\end{figure}

As illustrated in Figure \ref{fig:intro:code_overview}, CLICopti is built in two main layers -- the cell database interface described in Chapter \ref{sec:database} (blue), and the RF parameter estimator described in Chapter \ref{sec:AccelStructure} (green). The typical steps when using the library are shown in Figure \ref{fig:intro:operation}.

When using the code, the first step is almost\footnote{There exists a few specialized subclasses for the \texttt{AccelStructure} RF parameter estimation which does not require any database, as the full cell parameter interpolation points are hardcoded in the constructor instead of being returned from a database.} always to pick a database interface and initialize it from a database file, setting the required / wanted options in the constructor. This produces a \texttt{CellBase} object.

The second steps is then to pick an \texttt{AccelStructure} constructor, which is passed a pointer to the database interface, and a list of settings such as structure length, apertures, etc., depending on which subclass is used. This constructor returns an object which is a subclass of \texttt{AccelStructure}.

Once an \texttt{AccelStructure} object is obtained, this can then be interrogated to find what the power requirement, maximum pulse length at a given power level etc. is for a structure of the given design.

\section{Overview of the method}
\label{sec:intro:method}

\begin{figure}
  \input{intro/fig/usagePattern.tex}
  \caption{Typical usage pattern for CLICopti. Red stars are data points from database, blue stars are points created by the database interpolator (blue box). The green box represents the \texttt{AccelStructure} object. The ``extracted'' parameters (blue stars in database) corresponds to CLIC\_G \cite{proc.linac10.CLICmainLinacStructure}. The numbers in \textbf{bold} are the same as the step numbers in Figure \ref{fig:intro:code_overview}, where \textbf{2.1}--\textbf{2.4} are sub-steps of step \textbf{2}.}
  \label{fig:intro:operation}
\end{figure}

As described above and shown in Figure \ref{fig:intro:operation}, the method works in a three-step process, where the main part is the \texttt{AccelStructure} object. This implements an analytical method for estimating the gradient profile in the steady state gradient, as described in \cite{PhysRevSTAB.14.052001}. This can be estimated from the power flow, giving the expression
\begin{equation}
  G(z) = G_0 ~ g(z) - g_L(z) ~ I_\mathrm{beam} ~,
  \label{eq:Gprofile}
\end{equation}
where $G(z)$ is the gradient at beam-loading current $I_\mathrm{beam}$ at position $z$ along the structure, where $z=0$ is the beginning of the structure and $g=L$ is the end. Further, $G_0$ is given as
\begin{equation}
  G_0 = \sqrt{\frac{\omega ~ \frac{R'}{Q}(z=0) ~ P_0}{v_g(z=0)}}
  \label{eq:G0}
\end{equation}
and describes the input power to the structure. The symbols here are the (angular) operating frequency of the structure $\omega = 2 \pi f_0$, the normalized shunt impedance per unit length $R'/Q$ as function of position along the structure, the group velocity $v_g$ as a function of position along the structure, and the input power $P_0$.
Finally, the functions $g(z)$ and $g_L(z)$ are given as
\begin{equation}
  g(z) = \sqrt{\frac{v_g(0) ~ \frac{R'}{Q}(z)}{v_g(z) ~ \frac{R'}{Q}(0)}} 
  \exp{\left( -\frac{1}{2} \int_0^z \frac{\omega ~ \mathrm{d}z'}{v_g(z') ~ Q(z')} \right)}
\end{equation}
\begin{center}
  and
\end{center}
\begin{equation}
  g_L(z) = g(z) \int_0^z \frac{\omega ~\frac{R'}{Q}(z') \mathrm{d}z'}{2 ~ g(z') ~ v_g(z')} ~.
\end{equation}
These equations are the basis for predicting the gradient profile from the input power and beam current, once $R'/Q$, $v_g$, and $Q$ is known as a function of $z$ along the structure. Knowing the gradient profile is the basis for calculating the peak fields $\hat E$, $\hat S_c$, $\hat H$ and the peak power/circumference $P/C$, which together with the breakdown rate scaling laws \cite{PhysRevSTAB.12.102001,CLICNOTE_CellDesign} makes it possible to estimate the maximum pulse length for a given acceptable breakdown rate.

To find the necessary parameters $R'/Q$, $v_g$, and $Q$ as a function of $z$, a 2$^\mathrm{nd}$ order polynomial interpolation is used for each of them. This interpolation needs the value of the parameters at three points along $z$, which are taken to be $z=0$, $z=L/2$ and $z=L$. These interpolation points are usually provided by the database interface. In the typical scenario, the user thus passes the \texttt{AccelStructure} constructor the iris parameters for the first and last cell, along with other information such as the number of cells etc. The \texttt{AccelStructure} constructor then estimates the iris parameters of the middle cell as the average of the first and last cell parameters, which producing a linearly tapered structure. The iris parameters for these cells are then passed to the database interface, which estimates the full parameter set and passes it back as \texttt{Cell} objects.

The purpose of the database interface is to estimate the full parameter set of a cell, given it's iris parameters. This is done on the basis of a table of cells optimized for a representative set of iris parameters. The database interface loads this table, and for each of the parameters described in the table, a function which interpolates the parameter is created. This makes it possible to do lookups for the cell parameters at all iris parameters within the domain supported by the underlying database.

\section{Installation \& file structure}

The code for this library is available at CERN's SVN server, under the repository name \texttt{clicopti}. To download the newest version of the code, documentation etc. and create a SVN working directory, use the command\footnote{For these commands to work, you need a terminal of a Linux/Mac machine, and a working installation of Subversion (SVN). Use and knowledge of UNIX-like operating systems and C++ is assumed throughout this document.}:\\
\texttt{svn co svn+ssh://svn.cern.ch/reps/clicopti}\\
This produces a new folder \texttt{clicopti} in the current directory, which is populated with the subfolders \texttt{archive}, \texttt{doc}, \texttt{miniTests}, and \texttt{structureCalculator}. The \texttt{archive} folder contains old codes which implements some of the same techniques as CLICopti, \texttt{doc} contains the \LaTeX\, code for this manual, \texttt{miniTests} contains a few small proof-of-principle test programs, and \texttt{structureCalculator} is the library itself. By normal SVN convention, all of these folders contains the subfolders \texttt{trunk}, \texttt{tag} and \texttt{branches}. Here \texttt{trunk} contains the newest version of the program, \texttt{tag} contains ``finished'' versions and \texttt{branches} contains experimental branches of the code.

It is also possible to directly check out any subfolder. As an example, to check out only the subfolder \texttt{structure\-Calculator\-/trunk}, use the following command:\\
\texttt{svn co svn+ssh://svn.cern.ch/reps/clicopti/structureCalculator/trunk/}\\
This will create a new folder which is called \texttt{trunk}. To check out to a new folder called \texttt{struct\-ure\-Calc\-ul\-at\-or}, use this command:\\
\texttt{svn co svn+ssh://svn.cern.ch/reps/clicopti/structureCalculator/\-trunk/ struc\-ture\-Calculator}

To update the software to the newest version, enter the folder which should be updated (including subfolders), and issue this command:\\
\texttt{svn up}\\
It may also be useful to access the WebSVN interface which is located at \url{https://svnweb.cern.ch/cern/wsvn/clicopti/?}. This provides an easy way to browse through files of any versions, to compare different versions to see what has changed, and to see the comments which accompany all new versions uploaded to the repository.

The folder \texttt{structureCalculator/trunk} has the subfolders \texttt{src}, \texttt{h}, \texttt{cellBase} and \texttt{testOutput}. The \texttt{src} folder contains the \texttt{.cpp} implementation files and the \texttt{Makefile}, the \texttt{h} folder contains the \texttt{.h} header files, the \texttt{cellBase} folder contains the cell database files and the \texttt{testOutput} folder is used by the program \texttt{testCellLibrary\_compareDBs.cpp} to store it's output.

To compile the code, download it and enter the \texttt{src} subfolder. This folder contains a makefile, which instructs \texttt{make} how to compile the program. To run this makefile, simply run the following command in the \texttt{src} directory:\\
\texttt{make}\\
This compiles both the library and the test programs. The makefile also contains a target \texttt{clean}, which deletes all compiled objects. This target should be invoked if making changes to the header files, and if creating a source package for distribution. A named target (such as \texttt{clean} or \texttt{tests}) is invoked as follows:\\
\texttt{make \textit{targetname}} \\
In all cases, the compiled executable are created at the base folder of the source distribution, i.e. the \texttt{trunk} folder. No external libraries except the standard C/C++ libraries are required to compile the library.

The program is by default compiled with the option \texttt{-g}, which turns on debugging. It may be possible to gain some performance by removing this flag, and switch on optimization using \texttt{-O2} and maybe \texttt{-march=native}. Also note that the libraries contain many calls to \texttt{assert()}, which checks input and internal state for consistency, stopping the program and printing an error message if the check failed. Once an application using the library is running and properly debugged, one may consider switching these off by defining the preprocessor macro \texttt{NDEBUG}.

\section{Example programs}
\label{sec:intro:examples}

Four example/test programs are currently provided with the sourcecode of this library.
\begin{description}
\item[\texttt{testCellLibrary.cpp}] \hfill\\
  This program tests the basic functionality of the code for handling single cells (described in Chapter \ref{sec:cell}), and also the different cell database interface implementations (different interpolation methods) and frequency scaling (described in Chapter \ref{sec:database}).
\item[\texttt{testCellLibrary\_compareDBs.cpp}] \hfill\\
  This program scans through the parameter space for the iris aperture \textbf{a} and iris thickness \textbf{b} at 120$^\circ$ phase advance using three different databases (30 GHz database scaled to 12 GHz, 12 GHz database version 1 \cite{CLICNOTE_CellDesign} and 12 GHz database version 2 \cite{CLICNOTE_CellDesign}) with both linear and spline surface interpolation as described in Chapter \ref{sec:database}. Output files are written to subfolder \texttt{testOutput}, where \texttt{test\-Cell\-Library\-\_com\-pare\-DBs\--PL\-OT\-.py} and \texttt{plot\-3D\-.m} can plot them.
\item[\texttt{testStructureLibrary.cpp}] \hfill\\
  This program tests the RF parameter estimator described in Chapter \ref{sec:AccelStructure}.
\item[\texttt{testOptimize.cpp}] \hfill\\
  This program exemplifies how the library can be used for optimization, by creating data files (which may be plotted) describing how different parameters in structure powering and design interact.
\end{description}

These example programs also provide a template for the easiest way to integrate the library with a new code: Put the new source file in the \texttt{src} directory, and add it to the makefile. It is also possible to link your code with the library using the \texttt{-I\textit{path/to/headerfiles}} flag of GCC (where the headerfiles are located in the \texttt{h} directory), and linking with the \texttt{.o} object files of the library. It is not recommended to modify the directory structure, as keeping the standard layout greatly simplifies updating to new versions using SVN.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../structureCalculator"
%%% End: 
