import numpy as np
import matplotlib.pyplot as plt

LATEX_TEXTWIDTH = 452.9679 #pt
LATEX_TEXTWIDTH *= 1.0/72.27 #inches
FIGWIDTH = 0.45*LATEX_TEXTWIDTH
FIGHEIGTH = FIGWIDTH / ( (1+np.sqrt(5))/2.0)
FIGSIZE = (FIGWIDTH, FIGHEIGTH)

FIGSIZE = map(lambda x : x*2.0, FIGSIZE) #1/2 size font


#interpolation points
an_1 = 0.07
dn_1 = 0.1
rQ_1 = 20138.3 #[linacOhm/m]
Q_1  = 6389.33
vg_1 = 0.32    #[%c]

an_2 = 0.15
dn_2 = 0.25
rQ_2 = 12513.4 #[linacOhm/m]
Q_2  = 6046.22
vg_2 = 2.62    #[%c]

an_3 = 0.23
dn_3 = 0.4
rQ_3 = 7145.32 #[linacOhm/m]
Q_3  = 5644.62
vg_3 = 7.49    #[%c]

t_base  = (0.0,1.0,2.0)
rQ_base = (rQ_1, rQ_2, rQ_3)
Q_base  = (Q_1, Q_2, Q_3)
vg_base  = (vg_1, vg_2, vg_3)

(t_compat, an_compat,dn_compat,rQ_compat,Q_compat,vg_compat) = np.loadtxt("testCellLibrary_interpolCompatTest.dat", unpack=True)
(t_lin, an_lin,dn_lin,rQ_lin,Q_lin,vg_lin) = np.loadtxt("testCellLibrary_interpolLinear.dat", unpack=True)

plt.figure()
plt.plot(t_compat,rQ_compat)
plt.plot(t_lin,rQ_lin)
plt.plot(t_base,rQ_base, 'r*')

plt.figure()
plt.plot(t_compat,Q_compat)
plt.plot(t_lin,Q_lin)
plt.plot(t_base,Q_base, 'r*')

plt.figure(figsize=FIGSIZE)
plt.plot(t_compat,vg_compat,'k-',label="Quadratic")
plt.plot(t_lin,vg_lin, 'b--', label="Linear")
plt.plot(t_base,vg_base, 'r*', ms=15, label="Points")
plt.xlabel("t")
plt.ylabel(r"$v_g$ [%c]")
plt.legend(loc=2)
plt.subplots_adjust(bottom=0.12)
plt.savefig("verification1.pdf")


plt.show()
