\chapter{Handling of cell parameters}
\label{sec:cell}

This part of CLICopti is implemented in the files \texttt{cell.h} and \texttt{cell.cpp}. The header file contains the description of the data structure \texttt{Cell}, which is shown in Listing \ref{lst:cell:Cell}. The header file also defines the inline functions \texttt{get\-By\-Off\-set(\ldots)}, which provides a runtime-defined method of defining which variable should be extracted from a \texttt{Cell} object.

The data type \texttt{struct Cell} is fundamental to the inner workings of rest of the library. This is due to \texttt{Cell} objects being used to store the loaded points in the database classes described in Chapter \ref{sec:database}, and to do the arithmetic needed for creating and returning interpolated points from \texttt{Cell\-Ba\-se::\-get\-Cell\-Inter\-pol\-at\-ed (\ldots)}. Further, objects of the \texttt{Cell} type are used multiple places in the \texttt{AccelStructure} class described in Chapter \ref{sec:AccelStructure}, primarily to hold the interpolation points needed to describe the structure parameters as continuous functions along the length of the structure.

The code file \texttt{cell.cpp} implements a few more functions, which create \texttt{Cell} objects by parsing database files, outputs them to a C++ \texttt{ostream} such as \texttt{cout}, and handle algebraic operations on \texttt{Cell} objects. The parsing functions are called by \texttt{CellBase::CellBase()}, and which parser is used depends on the \texttt{FOR\-MAT\-CODE} setting in the database file (see Sections \ref{sec:database:CellBase:constructor} and \ref{sec:database:CellBase:CellType} for details).

\section{The data type \texttt{struct Cell}}

This \texttt{struct}, shown in Listing \ref{lst:cell:Cell}, holds the data describing one accelerator cell, or equivalently the properties of an accelerating structure at one $z$-location. The fields of the \texttt{struct} and their measurement units are listed in the code shown in the listing.

\begin{lstlisting}[caption=Definition of \texttt{struct Cell}.,label=lst:cell:Cell]
struct Cell {
  //Geometry
  double h;   //Cell length [m]
  double a;   //Iris aperture [m]

  double d_n; //Iris thickness / h (normalized)
  double a_n; //Normalized iris aperture: a/lambda

  //Main mode
  double f0;  //Frequency [GHz]
  double psi; //Phase advance [deg]

  double Q;  //Q-factor
  double vg; //Group velocity [%c]
  double rQ; //r/Q [linacOhm / m]

  double Es; //Esurf/Eacc
  double Hs; //Hsurf/Eacc [mA/V]
  double Sc; //Sc/Eacc^2 [mA/V]

  //Wakefield 1st transverse mode
  double f1mn; //Frequency [GHz]
  double Q1mn; //Q-factor
  double A1mn; //Amplitude [V/pC/mm/m]
};
\end{lstlisting}

Note that $\bar S_c$ is set to NaN by \texttt{Cell\_\-TD\_\-30GHz\_\-v1\_\-file\-Parse\-(std\-::\-string\& line)}, as the 30 GHz cell database file does not contain information about $\bar S_c$. This leads to \texttt{Accel\-Struct\-ure::\-get\-Max\-Fiel\-ds (double power, double beam\-Curr\-ent)} returning 0.0 as $\hat S_c$, and thus \texttt{Accel\-Struct\-ure\-::\-get\-Max\-Allow\-able\-Beam\-Time\_\-Sc\_\-has\-Peak(double max\-Sc, double was\-ted\-Ti\-me)} returning $\tau_{S_c} = \infty$ as the maximum pulse length allowed by $\hat S_c$, effectively ignoring the $\hat S_c$ constraint. See also Sections \ref{sec:intro:fileparse_12GHz_v1} and \ref{sec:AccelStructure:mainclass:getMaxScHasPeak} for more information.

Also note that all fields in the \texttt{Cell} structure should be doubles, as they are often accessed by the \texttt{get\-By\-Off\-set(\ldots)} functions -- the \texttt{Cell} structure is effectively used as an array of doubles with named elements.

\section{Functions}
This section defines the functions defined in \texttt{cell.h} and implemented in \texttt{cell.cpp} and \texttt{cell.h}.

\subsection{double getByOffset (const Cell\& cell, const size\_t off)}
\label{sec:cell:getByOffset1}
This function is implemented in \texttt{cell.h} as shown below, and allows getting the data in one field from a Cell structure.
\begin{lstlisting}
inline double getByOffset(const Cell& cell, const size_t off) {
  return *( (double*) ( (char*)(&cell) + off ) );
};
\end{lstlisting}
To use it, a reference to the \texttt{Cell} object \texttt{cell} and a offset value \texttt{off} (in bytes) must be passed. As an example, to access the field \texttt{Cell.Q} the following code would work:
\begin{lstlisting}
  Cell theCell = {}; //Initialize the value of all fields in the struct to 0
  double Q = getByOffset(theCellObject,offsetof(struct Cell, Q));
\end{lstlisting}
Note that the macro \texttt{size\_t offsetof(type, member)} is defined in \texttt{stddef.h}, which is also known as \texttt{cstddef} in C++.

\subsection{double getByOffset (const Cell* const cell, const size\_t off)}
\label{sec:cell:getByOffset2}
This function is implemented in \texttt{cell.h} as shown below, and works by the same principle as the function with the same name described above.
\begin{lstlisting}
inline double getByOffset(const Cell* const cell, const size_t off) {
  return *( (double*) ( (char*)(cell) + off ) );
};
\end{lstlisting}
The only difference is that this version expects a \texttt{Cell} pointer and not a reference.

\subsection{Cell Cell\_TD\_30GHz\_v1\_fileParse (std::string\& line)}
\label{sec:intro:fileparse_30GHz_v1}
This function parses a row describing one cell in the file format used by the old 30 GHz database file \texttt{TD\_\-30\-GHz\-.dat}. The expected format of a line is\\
\texttt{a\_n d\_n psi \quad Q vg rQ Es Hs \quad f1mn Q1mn A1mn} ~, \\
where the variable names and units match those in \texttt{Cell}, except \texttt{A1mn} which is in the file stored for a $\sigma_z = 0.6 ~\mathrm{mm}$ driving bunch, and is scaled to a $\delta$-function driving bunch by multiplying with the factor
\[
\exp\left( \frac{\left(2\pi(\mathtt{f1mn} \cdot 10^9) \cdot \sigma_z / c \right)^2}{2.0} \right) ~,
\]
where $c$ is the speed of light in vacuum.

The rest of the variables missing from the database but present in \texttt{Cell} are set as follows:
\begin{description}
\item[\texttt{h} =] $(\mathtt{psi}/360.0) \cdot \lambda$
\item[\texttt{a} =] $\mathtt{a\_n} \cdot \lambda$
\item[\texttt{f0} =] 29.985
\item[\texttt{Sc} =] \texttt{NaN}
\end{description}
Here $\lambda = c / (29.985 ~\mathrm{GHz})$ is the wavelength of the target frequency. Note that $S_c$ is set to \texttt{NaN}

\subsection{Cell Cell\_TD\_12GHz\_v1\_fileParse (std::string\& line)}
\label{sec:intro:fileparse_12GHz_v1}
This function parses a row describing one cell in the file format used by the new 12 GHz database files \texttt{TD\_\-12\-GHz\_\-v1\-.dat} and \texttt{TD\_\-12\-GHz\_\-v2\-.dat} \cite{CLICNOTE_CellDesign}. The expected format of a line is\\
\texttt{a\_n d\_n psi \quad f0 Q vg rQ Es Sc Hs \quad f1mn Q1mn A1mn} ~, \\
where the variable names and units match this in \texttt{Cell} except for \texttt{rQ}, which must be converted from $\mathrm{k\Omega/m}$ to $\mathrm{\Omega/m}$ by multiplying with 1000. The \texttt{f0} parameter is included as the cells designs typically do not hit exactly 11.442 GHz. The \texttt{f0} parameter thus allows the use of the scaling functionality in \texttt{CellBase} to rectify this (see Sections \ref{sec:database:scale} and \ref{sec:database:compat:scaleCell} for more information).

The rest of the variables missing from the database but present in \texttt{Cell} are set as follows:
\begin{description}
\item[\texttt{h} =] $(\mathtt{psi}/360.0) \cdot \lambda$
\item[\texttt{a} =] $\mathtt{a\_n} \cdot \lambda$
\end{description}
Here $\lambda = c / (11.9942 ~\mathrm{GHz})$ is the wavelength of the target frequency, where $c$ is the speed of light in vacuum.

\subsection{std::ostream\& operator<\hspace{0pt}< (std::ostream \&out, const Cell\& cell)}
This operator provides an easy way to print a \texttt{Cell} object to an \texttt{std::ostream} such as \texttt{cout}. A typical code using the operator is shown below, followed by the output.
\begin{lstlisting}
AccelStructure* CLIC_G = NULL;
//code initializing CLIC_G
cout << "FIRST : " CLIC_G->getCellFirst() << endl;
\end{lstlisting}
\begin{flushleft}
\texttt{FIRST : h=0.0083316[m], a=0.00315[m], d\_n=0.160233, a\_n=0.126026, f0=11.9942[GHz], psi=120[deg], Q=6308.74, vg=1.97214[\%c], rQ=15023.9[linacOhm/m], Es=2.13866, Hs=4.08638\-[mA/V], Sc=nan[mA/V], f1mn=16.709[GHz], Q1mn=10.7992, A1mn=151.992[V/pC/m/mm]}
\end{flushleft}

\subsection{Cell operator* (const Cell\& lhs, const double rhs)}
This multiplies every element in a \texttt{Cell} object with the provided \texttt{double}, returning a new \texttt{Cell} object. The input \texttt{Cell} is unchanged.
\subsection{Cell operator* (const double lhs, const Cell\& rhs)}
This function has the same effect as the one described above it, with the exception of commutation of the operands. It is implemented as a simple inline redirect.
\subsection{Cell operator/ (const Cell\& lhs, const double rhs)}
Similar to the functions above it, except that every element in the \texttt{Cell} is divided by the \texttt{double} instead of multiplied. Implemented as a multiplication by $1/\mathtt{rhs}$, redirecting to the top multiplication operator.

\subsection{Cell operator+ (const Cell\& lhs, const Cell\& rhs)}
Every element in \texttt{rhs} is added to its counterpart in \texttt{rhs}, producing a new \texttt{Cell}.
%\subsection{Cell operator+=(const Cell\& lhs, const Cell\& rhs)}
\subsection{Cell operator- (const Cell\& lhs, const Cell\& rhs)}
Similar to the elementwise \texttt{+} operator described above, but with subtraction. Implemented as $\mathtt{lhs}+( (-1)*\mathtt{rhs} )$.

%\subsection{Cell operator-=(const Cell\& lhs, const Cell\& rhs)}

\section{Verification and example programs}

The \texttt{Cell} arithmetic (operators \texttt{+}, \texttt{-}, \texttt{*}, \texttt{/}) is tested in \texttt{testCellLibrary.cpp}. These tests show that the implementation of the arithmetic yields the expected results.

The same program also tests \texttt{Cell\_TD\_30GHz\_v1\_fileParse (\ldots)} by parsing such a file to create a \texttt{CellBase} and then printing back the cells stored in the \texttt{CellBase}, both directly from the grid, and interpolated at the grid points.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../structureCalculator"
%%% End: 
